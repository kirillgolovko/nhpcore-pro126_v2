﻿using Common.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Utils.Logging
{
    public class NLogLogger : ILoggerUtility
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void Debug(string message, Exception ex = null)
        {
            if (ex != null) { logger.Debug(message, ex); return; }

            logger.Debug(message);
        }

        public void Error(string message, Exception ex = null)
        {
            if (ex != null) { logger.Error(message, ex); return; }

            logger.Error(message);
        }

        public void Fatal(string message, Exception ex = null)
        {
            if (ex != null) { logger.Fatal(message, ex); return; }

            logger.Fatal(message);
        }

        public void Info(string message, Exception ex = null)
        {
            if (ex != null) { logger.Info(message, ex); return; }

            logger.Info(message);
        }

        public void Trace(string message, Exception ex = null)
        {
            if (ex != null) { logger.Trace(message, ex); return; }

            logger.Trace(message);
        }

        public void Warn(string message, Exception ex = null)
        {
            if (ex != null) { logger.Warn(message, ex); return; }

            logger.Warn(message);
        }
    }
}
