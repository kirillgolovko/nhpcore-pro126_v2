﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateDepartmentCommand : BaseBusinessCommand<CreateDepartmentArgs, CreateDepartmentResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organinsationService;
        private readonly IDepartmentService _departmentService;

        private Organisation _organisation;
        private Department _parent;

        public CreateDepartmentCommand(IUnitOfWork<NhpCoreDbContext> uow, IOrganisationService organinsationService,IDepartmentService departmentService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organinsationService == null) { throw new ArgumentNullException(nameof(organinsationService)); }
            if (departmentService == null) { throw new ArgumentNullException(nameof(departmentService)); }
            _uow = uow;
            _organinsationService = organinsationService;
            _departmentService = departmentService;
        }

        protected override async Task<CreateDepartmentResult> PerformCommand(CreateDepartmentArgs arguments)
        {
            var result = new CreateDepartmentResult { IsSuccess = true };
            var _Department = arguments.Department;
            if (_parent == null) {
                _Department.Level = 1;
            } else {
                _Department.Level = _parent.Level + 1;
            }
            
            try
            {
                _uow.BeginTransaction();

                var repository = _uow.GetRepository<Department>().Add(_Department);

                await _uow.SaveChangesAsync();

                _uow.CommitTransaction();

                result.Id = _Department.Id;
                result.ParentId = _Department.ParentId ?? arguments.Department.OrganisationId;
                result.OrganisationId = _Department.OrganisationId;
            }
            catch (Exception ex)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_CREATING_ERROR, BusinessErrors.Organisation.DEPARTMENT_CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<CreateDepartmentResult> Validate(CreateDepartmentArgs arguments)
        {
            var result = new CreateDepartmentResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _organisation = await _organinsationService.GetOrganisation(arguments.Department.OrganisationId);

            if (_organisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            if (arguments.Department.ParentId !=null && arguments.Department.ParentId != 0)
            {
                _parent = await _departmentService.Get((int)arguments.Department.ParentId);
            }
            else {
                _parent = null;
            }

            return result;
        }
    }
}