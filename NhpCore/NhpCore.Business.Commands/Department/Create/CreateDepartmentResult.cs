﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateDepartmentResult : BaseBusinessCommandResult
    {
        /// <summary>
        /// Id созданного подразделения
        /// </summary>
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int OrganisationId { get; set; }
    }
}