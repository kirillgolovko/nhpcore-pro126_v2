﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

using NhpCore.Infrastructure.Utils.MessageSender;

namespace NhpCore.Business.Commands
{
    public class DeleteMemberCommand : BaseBusinessCommand<DeleteMemberArgs, EmptyBusinessCommandResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IUserService _userService;
        private readonly IDepartmentService _departmentService;

        private Department _department;
        private User _user;

        public DeleteMemberCommand(IUnitOfWork<NhpCoreDbContext> uow, IDepartmentService departmentService, IUserService userService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (departmentService == null) { throw new ArgumentNullException(nameof(departmentService)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            _uow = uow;
            _departmentService = departmentService;
            _userService = userService;
        }

        protected override async Task<EmptyBusinessCommandResult> PerformCommand(DeleteMemberArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };
            try
            {
                _uow.BeginTransaction();
                _user.MemberOfDepartments.Remove(_department);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_EDITING_ERROR, BusinessErrors.Organisation.DEPARTMENT_EDITING_ERROR_CODE);
            }
            return result;
        }

        protected override async Task<EmptyBusinessCommandResult> Validate(DeleteMemberArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _department = await _departmentService.Get(arguments.DepartmentId.Id);
            //_department = _uow.GetRepository<Department>()
            //    .Query()
            //    .Where(x => x.Id == arguments.DepartmentId.Id)
            //    .FirstOrDefault();

            if (_department == null)
            {
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_NOT_FOUND, BusinessErrors.Organisation.DEPARTMENT_NOT_FOUND_CODE);
                return result;
            }

            _user = await _userService.GetCoreUser(arguments.UserId.Id);
            //_user = _uow.GetRepository<User>()
            //    .Query()
            //    .Where(x => x.Id == arguments.UserId.Id)
            //    .FirstOrDefault();

            if (_user == null)
            {
                result.AddError(BusinessErrors.User.USER_NOT_FOUND, BusinessErrors.User.USER_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}