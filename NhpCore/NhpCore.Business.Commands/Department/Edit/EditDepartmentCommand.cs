﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditDepartmentCommand : BaseBusinessCommand<EditDepartmentArgs, EditDepartmentResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IDepartmentService _departmentService;

        private Department _department;

        public EditDepartmentCommand(IUnitOfWork<NhpCoreDbContext> uow, IDepartmentService departmentService, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (departmentService == null) { throw new ArgumentNullException(nameof(departmentService)); }
            _uow = uow;
            _departmentService = departmentService;
        }

        protected override async Task<EditDepartmentResult> PerformCommand(EditDepartmentArgs arguments)
        {
            var result = new EditDepartmentResult { IsSuccess = true };
            _department.ParentId = arguments.Department.ParentId;
            _department.Name = arguments.Department.Name;
            _department.Description = arguments.Department.Description;
            if (_department.Parent == null) {
                _department.Level = 1;
            } else {
                _department.Level = _department.Parent.Level + 1;
            }
            
            try
            {
                _uow.BeginTransaction();
                var repository = _uow.GetRepository<Department>().Update(_department);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_EDITING_ERROR, BusinessErrors.Organisation.DEPARTMENT_EDITING_ERROR_CODE);
            }
            return result;
        }

        protected override async Task<EditDepartmentResult> Validate(EditDepartmentArgs arguments)
        {
            var result = new EditDepartmentResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _department = await _departmentService.Get(arguments.Department.Id);

            if (_department == null)
            {
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_NOT_FOUND, BusinessErrors.Organisation.DEPARTMENT_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}
