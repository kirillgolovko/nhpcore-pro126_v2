﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class DeleteDepartmentCommand : BaseBusinessCommand<DeleteDepartmentArgs, EmptyBusinessCommandResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IDepartmentService _departmentService;

        private Department _department;

        public DeleteDepartmentCommand(
            IUnitOfWork<NhpCoreDbContext> uow,
            IDepartmentService departmentService,
            ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (departmentService == null) { throw new ArgumentNullException(nameof(departmentService)); }

            _uow = uow;
            _departmentService = departmentService;
        }

        protected override async Task<EmptyBusinessCommandResult> PerformCommand(DeleteDepartmentArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };
            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<Department>().Remove(_department);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_DELETING_ERROR, BusinessErrors.Organisation.DEPARTMENT_DELETING_ERROR_CODE);
            }
            return result;
        }

        protected override async Task<EmptyBusinessCommandResult> Validate(DeleteDepartmentArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };
            if (result == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _department = await _departmentService.Get(arguments.Id);
            if (_department == null)
            {
                result.AddError(BusinessErrors.Organisation.DEPARTMENT_NOT_FOUND, BusinessErrors.Organisation.DEPARTMENT_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}