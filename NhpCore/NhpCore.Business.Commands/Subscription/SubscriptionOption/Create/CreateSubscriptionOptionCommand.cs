﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionOptionCommand : BaseBusinessCommand<CreateSubscriptionOptionArgs, CreateSubscriptionOptionResult>
    {
        public CreateSubscriptionOptionCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<CreateSubscriptionOptionResult> PerformCommand(CreateSubscriptionOptionArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<CreateSubscriptionOptionResult> Validate(CreateSubscriptionOptionArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}