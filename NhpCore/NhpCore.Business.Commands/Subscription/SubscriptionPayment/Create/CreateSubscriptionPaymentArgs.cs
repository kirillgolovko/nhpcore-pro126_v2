﻿using Common.Commands;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentArgs : IBusinessCommandArguments
    {
        public SubscriptionPayment SubscriptionPayment { get; set; }
    }
}