﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentCommand : BaseBusinessCommand<CreateSubscriptionPaymentArgs, CreateSubscriptionPaymentResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organinsationService;
        private readonly IUserService _userService;

        private Organisation _organisation;
        private User _user;

        public CreateSubscriptionPaymentCommand(IUnitOfWork<NhpCoreDbContext> uow, IOrganisationService organinsationService, IUserService userService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organinsationService == null) { throw new ArgumentNullException(nameof(organinsationService)); }
            _uow = uow;
            _organinsationService = organinsationService;
            _userService = userService;
        }

        protected override async Task<CreateSubscriptionPaymentResult> PerformCommand(CreateSubscriptionPaymentArgs arguments)
        {
            var result = new CreateSubscriptionPaymentResult { IsSuccess = true };
            SubscriptionPayment _SubscriptionPayment = arguments.SubscriptionPayment;

            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<SubscriptionPayment>().Add(_SubscriptionPayment);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
                //result.ID = _SubscriptionPayment.Id;
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONPAYMENT_CREATING_ERROR, BusinessErrors.Subscription.SUBSCRIPTIONPAYMENT_CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<CreateSubscriptionPaymentResult> Validate(CreateSubscriptionPaymentArgs arguments)
        {
            var result = new CreateSubscriptionPaymentResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _organisation = await _organinsationService.GetOrganisation(arguments.SubscriptionPayment.BuyeerId);

            if (_organisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            _user = await _userService.GetCoreUser(arguments.SubscriptionPayment.BuyeerUserId);

            if (_user == null)
            {
                result.AddError(BusinessErrors.User.USER_NOT_FOUND, BusinessErrors.User.USER_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}