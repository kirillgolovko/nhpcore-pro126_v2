﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionPaymentCommand : BaseBusinessCommand<EditSubscriptionPaymentArgs, EditSubscriptionPaymentResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly ISubscriptionService _subscriptionService;

        private SubscriptionPayment _subscriptionPayment;

        public EditSubscriptionPaymentCommand(IUnitOfWork<NhpCoreDbContext> uow, ISubscriptionService subscriptionService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (subscriptionService == null) { throw new ArgumentNullException(nameof(subscriptionService)); }
            _uow = uow;
            _subscriptionService = subscriptionService;
        }

        protected override async Task<EditSubscriptionPaymentResult> PerformCommand(EditSubscriptionPaymentArgs arguments)
        {
            var result = new EditSubscriptionPaymentResult { IsSuccess = true };
            SubscriptionPayment _SubscriptionPayment = arguments.SubscriptionPayment;

            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<SubscriptionPayment>().Update(_SubscriptionPayment);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONPAYMENT_EDITING_ERROR, BusinessErrors.Subscription.SUBSCRIPTIONPAYMENT_EDITING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<EditSubscriptionPaymentResult> Validate(EditSubscriptionPaymentArgs arguments)
        {
            var result = new EditSubscriptionPaymentResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _subscriptionPayment = await _subscriptionService.GetSubscriptionPayment(arguments.SubscriptionPayment.Id);
            if (_subscriptionPayment == null)
            {
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONPAYMENT_NOT_FOUND, BusinessErrors.Subscription.SUBSCRIPTIONPAYMENT_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}