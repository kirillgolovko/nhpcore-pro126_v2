﻿using Common.Commands;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionPaymentArgs : IBusinessCommandArguments
    {
        public SubscriptionPayment SubscriptionPayment { get; set; }
    }
}