﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionPaymentBillCommand : BaseBusinessCommand<EditSubscriptionPaymentBillArgs, EditSubscriptionPaymentBillResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly ISubscriptionService _subscriptionService;

        private SubscriptionPaymentBill _Bill;

        public EditSubscriptionPaymentBillCommand(IUnitOfWork<NhpCoreDbContext> uow, ISubscriptionService subscriptionService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (subscriptionService == null) { throw new ArgumentNullException(nameof(subscriptionService)); }
            _uow = uow;
            _subscriptionService = subscriptionService;
        }

        protected override async Task<EditSubscriptionPaymentBillResult> PerformCommand(EditSubscriptionPaymentBillArgs arguments)
        {
            var result = new EditSubscriptionPaymentBillResult { IsSuccess = true };
            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<SubscriptionPaymentBill>().Update(arguments.Bill);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONBILL_CREATING_ERROR, BusinessErrors.Subscription.SUBSCRIPTIONBILL_CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<EditSubscriptionPaymentBillResult> Validate(EditSubscriptionPaymentBillArgs arguments)
        {
            var result = new EditSubscriptionPaymentBillResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _Bill = await _subscriptionService.GetSubscriptionPaymentBill(arguments.Bill.Id);

            if (_Bill == null)
            {
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONBILL_NOT_FOUND, BusinessErrors.Subscription.SUBSCRIPTIONBILL_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}