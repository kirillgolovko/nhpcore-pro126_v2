﻿using Common.Commands;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionPaymentBillArgs : IBusinessCommandArguments
    {
        public SubscriptionPaymentBill Bill { get; set; }
    }
}