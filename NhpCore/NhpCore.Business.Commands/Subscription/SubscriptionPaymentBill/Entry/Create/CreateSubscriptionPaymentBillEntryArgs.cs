﻿using Common.Commands;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentBillEntryArgs : IBusinessCommandArguments
    {
        public SubscriptionPaymentBillEntry BillEntry { get; set; }
    }
}