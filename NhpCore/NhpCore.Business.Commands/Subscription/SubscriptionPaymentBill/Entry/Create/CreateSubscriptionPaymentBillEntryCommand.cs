﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentBillEntryCommand : BaseBusinessCommand<CreateSubscriptionPaymentBillEntryArgs, CreateSubscriptionPaymentBillEntryResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly ISubscriptionService _subscriptionService;

        private SubscriptionPaymentBill _bill;

        public CreateSubscriptionPaymentBillEntryCommand(IUnitOfWork<NhpCoreDbContext> uow, ISubscriptionService subscriptionService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (subscriptionService == null) { throw new ArgumentNullException(nameof(subscriptionService)); }
            _uow = uow;
            _subscriptionService = subscriptionService;
        }

        protected override async Task<CreateSubscriptionPaymentBillEntryResult> PerformCommand(CreateSubscriptionPaymentBillEntryArgs arguments)
        {
            var result = new CreateSubscriptionPaymentBillEntryResult { IsSuccess = true };
            SubscriptionPaymentBillEntry _BillEntry = arguments.BillEntry;

            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<SubscriptionPaymentBillEntry>().Add(_BillEntry);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONBILLENTRY_CREATING_ERROR, BusinessErrors.Subscription.SUBSCRIPTIONBILLENTRY_CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<CreateSubscriptionPaymentBillEntryResult> Validate(CreateSubscriptionPaymentBillEntryArgs arguments)
        {
            var result = new CreateSubscriptionPaymentBillEntryResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _bill = await _subscriptionService.GetSubscriptionPaymentBill(arguments.BillEntry.BillId);
            if (_bill == null)
            {
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONBILL_NOT_FOUND, BusinessErrors.Subscription.SUBSCRIPTIONBILL_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}