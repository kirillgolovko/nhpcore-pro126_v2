﻿using Common.Commands;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentBillArgs : IBusinessCommandArguments
    {
        public SubscriptionPaymentBill Bill { get; set; }
    }
}