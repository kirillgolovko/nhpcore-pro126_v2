﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentBillCommand : BaseBusinessCommand<CreateSubscriptionPaymentBillArgs, CreateSubscriptionPaymentBillResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organinsationService;

        private Organisation _organisation;

        public CreateSubscriptionPaymentBillCommand(IUnitOfWork<NhpCoreDbContext> uow, IOrganisationService organinsationService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organinsationService == null) { throw new ArgumentNullException(nameof(organinsationService)); }
            _uow = uow;
            _organinsationService = organinsationService;
        }

        protected override async Task<CreateSubscriptionPaymentBillResult> PerformCommand(CreateSubscriptionPaymentBillArgs arguments)
        {
            var result = new CreateSubscriptionPaymentBillResult { IsSuccess = true };
            SubscriptionPaymentBill _Bill = arguments.Bill;

            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<SubscriptionPaymentBill>().Add(_Bill);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
                result.ID = _Bill.Id;
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Subscription.SUBSCRIPTIONBILL_CREATING_ERROR, BusinessErrors.Subscription.SUBSCRIPTIONBILL_CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<CreateSubscriptionPaymentBillResult> Validate(CreateSubscriptionPaymentBillArgs arguments)
        {
            var result = new CreateSubscriptionPaymentBillResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _organisation = await _organinsationService.GetOrganisation(arguments.Bill.BuyeerId);

            if (_organisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}