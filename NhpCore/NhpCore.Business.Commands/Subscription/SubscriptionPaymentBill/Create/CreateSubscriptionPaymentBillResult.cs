﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionPaymentBillResult : BaseBusinessCommandResult
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
    }
}