﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class DeleteSubscriptionCommand : BaseBusinessCommand<DeleteSubscriptionArgs, DeleteSubscriptionResult>
    {
        public DeleteSubscriptionCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<DeleteSubscriptionResult> PerformCommand(DeleteSubscriptionArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<DeleteSubscriptionResult> Validate(DeleteSubscriptionArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}