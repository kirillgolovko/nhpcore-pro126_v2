﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionCommand : BaseBusinessCommand<EditSubscriptionArgs, EditSubscriptionResult>
    {
        public EditSubscriptionCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<EditSubscriptionResult> PerformCommand(EditSubscriptionArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<EditSubscriptionResult> Validate(EditSubscriptionArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}