﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.Infrastructure.Utils.MessageSender;
using System.Text.RegularExpressions;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class InviteUsersCommand : BaseBusinessCommand<InviteUsersArgs, InviteUsersResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _coreUoW;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;
        private readonly IMessageSender _sender;

        private Organisation _inviteOrganisation;
        List<string> emailsForInvite=new List<string> { };

        public InviteUsersCommand(IUnitOfWork<NhpCoreDbContext> coreUow, IOrganisationService organisationService, IUserService userService, IMessageSender sender, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (coreUow == null) { throw new ArgumentNullException(nameof(coreUow)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (sender == null) { throw new ArgumentNullException(nameof(sender)); }


            this._coreUoW = coreUow;
            this._organisationService = organisationService;
            this._userService = userService;
            this._sender = sender;
        }

        protected override async Task<InviteUsersResult> PerformCommand(InviteUsersArgs arguments)
        {
            var taskList = new List<Task>();

            var result = new InviteUsersResult { IsSuccess = true };

            var InvitedUsers = await _organisationService.GetInvitedUsers(_inviteOrganisation.Id);
            var listRegisted = InvitedUsers.Where(x => arguments.Emails.Contains(x.Email)).Select(x=>x.Email).ToList();
            var listNotConfirmed = InvitedUsers.Where(x => arguments.Emails.Contains(x.Email) & !x.IsAccepted).ToList();
            var listConfirmed = InvitedUsers.Where(x => arguments.Emails.Contains(x.Email) & x.IsAccepted).Select(x=>x.Email).ToList();

            var inviteStructure = new List<Tuple<string, Guid>>();
            
            arguments.Emails.ForEach(mail =>
            {
                if (!listRegisted.Contains(mail)) {
                    emailsForInvite.Add(mail);
                }
            });
            // добавляем имейлы в базу
            emailsForInvite.ForEach(mail =>
            {
                var guid = Guid.NewGuid();
                var dtNow = DateTime.Now;

                inviteStructure.Add(new Tuple<string, Guid>(mail, guid));

                _coreUoW.GetRepository<OrganisationUserInvite>()
                    .Add(new OrganisationUserInvite
                    {
                        Email = mail,
                        InviteCode = guid,
                        InviteSentDate = dtNow.Date,
                        InviteSentTime = dtNow,
                        IsAccepted = false,
                        OrganisationId = _inviteOrganisation.Id,
                    });
            });

            taskList.Add(_coreUoW.SaveChangesAsync());

            //делаем рассылку новеньким
            inviteStructure.ForEach(tuple =>
            {
                var isExistedEmail = arguments.ExistedEmails.ContainsKey(tuple.Item1);
                var urlPath = isExistedEmail == true ? arguments.AcceptInviteUrl : arguments.RegistrationUrl;


                var messageUrl = $"{urlPath}?inviteCode={tuple.Item2}";

                if (isExistedEmail)
                {
                    var id = arguments.ExistedEmails[tuple.Item1];
                    messageUrl += $"&identityId={id}";
                }

                var messageBody = "Для принятия приглашения - щелкните <a href=" + messageUrl + ">здесь</a><br><br>---<br><a href='https://online.nhp-soft.ru'>НХП Проект в облаке</a>";


                var message = new Message
                {
                    Destination = tuple.Item1,
                    Subject = $"Приглашение присоединиться к компании {_inviteOrganisation.Name} в сервисе НХП Онлайн",
                    Body = messageBody,

                };
                taskList.Add(_sender.SendAsync(message));
            });

            //делаем рассылку не подтвержденным
            listNotConfirmed.ForEach(OrganisationUserInvite =>
            {
                var isExistedEmail = arguments.ExistedEmails.ContainsKey(OrganisationUserInvite.Email);
                var urlPath = arguments.AcceptInviteUrl;
                var messageUrl = $"{urlPath}?inviteCode={OrganisationUserInvite.InviteCode}";
                if (isExistedEmail)
                {
                    var id = arguments.ExistedEmails[OrganisationUserInvite.Email];
                    messageUrl += $"&identityId={id}";
                }
                var messageBody = "Для принятия приглашения - щелкните <a href=" + messageUrl + ">здесь</a><br><br>---<br><a href='https://online.nhp-soft.ru'>НХП Проект в облаке</a>";
                var message = new Message
                {
                    Destination = OrganisationUserInvite.Email,
                    Subject = $"Приглашение присоединиться к компании {_inviteOrganisation.Name} в сервисе НХП Онлайн",
                    Body = messageBody,
                };
                taskList.Add(_sender.SendAsync(message));
            });

            //делаем рассылку подтвержденным
            var profiles = await _userService.GetProfiles();
            listConfirmed.ForEach(email =>
            {
                var messageBody = $"Компания \"{_inviteOrganisation.Name}\" приглашает к участию в проектах, зайдите в <a href='http://online.nhp-soft.ru/dashboard'>личный кабинет</a>. Если вы забыли пароль, воспользуйтесь ссылкой \"<a href='http://online.nhp-soft.ru/account/forgotpassword'>ЗАБЫЛИ?</a>\".<br><br>---<br><a href='https://online.nhp-soft.ru'>НХП Проект в облаке</a>";
                var profile = profiles.Where(x => x.Email == email).FirstOrDefault();
                if (profile != null) {
                    messageBody += $"<br>-----------------------------------------------------------<br>Не хотите получать такие письма? Нажмите <a href=\"https://online.nhp-soft.ru/notmailsender?idprofile={profile.IdProfile}\">СЮДА</a>";
                }
                var message = new Message
                {
                    Destination = email,
                    Subject = $"Приглашение к участию в проектах компании {_inviteOrganisation.Name} в сервисе НХП Онлайн",
                    Body = messageBody,

                };
                taskList.Add(_sender.SendAsync(message));
            });

            // TODO: ждем окончания рассылок. Определиться нужно ли? их можно возвращать в списке обратно со статусом рассылки 
            Task.WhenAll(taskList).GetAwaiter();

            var failed = new List<string>();
            taskList.ForEach(x =>
            {
                if (x.Status == TaskStatus.Faulted)
                {
                    failed.Add(((Message)x.AsyncState).Destination);
                }
            });

            result.FailedEmails = failed;

            return result;
        }

        // TODO: Сделать нормальную валидацию мэйлов
        protected override async Task<InviteUsersResult> Validate(InviteUsersArgs arguments)
        {
            var result = new InviteUsersResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _inviteOrganisation = await _organisationService.GetOrganisation(arguments.OrganisationId);

            if (_inviteOrganisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
            }

            return result;
        }
    }
}
