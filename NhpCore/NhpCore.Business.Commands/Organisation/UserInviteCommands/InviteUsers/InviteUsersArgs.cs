﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
   public  class InviteUsersArgs: IBusinessCommandArguments
    {
        public int OrganisationId { get; set; }
        public List<string> Emails { get; set; }
        public string RegistrationUrl { get; set; }
        public Dictionary<string, int> ExistedEmails { get; set; }
        public string AcceptInviteUrl { get; set; }
    }
}
