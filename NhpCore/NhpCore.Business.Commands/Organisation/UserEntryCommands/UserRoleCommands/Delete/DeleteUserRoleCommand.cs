﻿using Common.Commands;
using System;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using Common.Logging;
using NhpCore.Business.Errors;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common.EntityFramework;

namespace NhpCore.Business.Commands
{
    public class DeleteUserRoleCommand : BaseBusinessCommand<DeleteUserRoleArgs, EmptyBusinessCommandResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _unitOfWork;
        private readonly IUserService _userService;
        private OrganisationUserEntry _userEntry;
        private OrganisationUserRole _userRole;

        public DeleteUserRoleCommand(IUnitOfWork<NhpCoreDbContext> unitOfWork, IUserService userService, ILoggerUtility logger) : base(logger)
        {
            if (unitOfWork == null) { throw new ArgumentNullException(nameof(unitOfWork)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        protected override async Task<EmptyBusinessCommandResult> PerformCommand(DeleteUserRoleArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };
            try
            {
                _unitOfWork.BeginTransaction();
                _userEntry.UserRoles.Remove(_userRole);
                _unitOfWork.GetRepository<OrganisationUserRole>().Remove(_userRole);
                await _unitOfWork.SaveChangesAsync();
                _unitOfWork.CommitTransaction();
            }
            catch (Exception)
            {
                result.IsSuccess = false;
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USERENTRY_ROLE_DELETING_ERROR, BusinessErrors.Organisation.ORGANISATION_USERENTRY_ROLE_DELETING_ERROR_CODE);
            }
            return result;
        }

        protected override async Task<EmptyBusinessCommandResult> Validate(DeleteUserRoleArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            
            _userEntry = await _userService.GetUserEntry(arguments.OrganisationUserEntryId);

            if (_userEntry == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND_CODE);
                return result;
            }

            _userRole = await _userService.GetUserRole(arguments.UserRoleId);

            if (_userRole == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ROLE_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ROLE_NOT_FOUND_CODE);
                return result;
            }
            return result;
        }
    }
}