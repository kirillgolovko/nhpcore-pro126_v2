﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditOrganisationUserEntryCommand : BaseBusinessCommand<EditOrganisationUserEntryArgs, EditOrganisationUserEntryResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;

        private OrganisationUserEntry _Entry;

        public EditOrganisationUserEntryCommand(
            IUnitOfWork<NhpCoreDbContext> uow,
            IUserService userService, IOrganisationService organisationService,
            ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _uow = uow;
            _userService = userService;
            _organisationService = organisationService;
        }

        protected override async Task<EditOrganisationUserEntryResult> PerformCommand(EditOrganisationUserEntryArgs arguments)
        {
            var result = new EditOrganisationUserEntryResult { IsSuccess = true };

            _Entry.ProfileId = arguments.OrganisationUserEntry.ProfileId;
            _Entry.IsEnabled = arguments.OrganisationUserEntry.IsEnabled;
            _Entry.IsDeleted = arguments.OrganisationUserEntry.IsDeleted;
            _Entry.DisablingTime = arguments.OrganisationUserEntry.DisablingTime;
            _Entry.DeleteTime = arguments.OrganisationUserEntry.DeleteTime;
            _Entry.Salary = arguments.OrganisationUserEntry.Salary;

            try
            {
                _uow.BeginTransaction();
                var repository = _uow.GetRepository<OrganisationUserEntry>().Update(_Entry);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND_CODE);
            }

            return result;
        }

        protected override async Task<EditOrganisationUserEntryResult> Validate(EditOrganisationUserEntryArgs arguments)
        {
            var result = new EditOrganisationUserEntryResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _Entry = arguments.OrganisationUserEntry;

            //_Entry = await _organisationService.GetUserEntry(arguments.OrganisationUserEntry.OrganisationId,arguments.OrganisationUserEntry.UserId);

            //if (_Entry == null)
            //{
            //    result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND_CODE);
            //    return result;
            //}


            return result;
        }
    }
}
