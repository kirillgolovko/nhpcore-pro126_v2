﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditOrganisationDetailsCommand : BaseBusinessCommand<EditOrganisationDetailsArgs, EditOrganisationDetailsResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organisationService;

        private OrganisationDetailCard _odc;
        private Organisation _org;

        public EditOrganisationDetailsCommand(IUnitOfWork<NhpCoreDbContext> uow, IOrganisationService organisationService, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            _uow = uow;
            _organisationService = organisationService;
        }

        protected override async Task<EditOrganisationDetailsResult> PerformCommand(EditOrganisationDetailsArgs arguments)
        {
            var result = new EditOrganisationDetailsResult { IsSuccess = true };
            _odc = arguments.DetailCard;
            try
            {
                _uow.BeginTransaction();
                var repository = _uow.GetRepository<OrganisationDetailCard>().Update(_odc);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
            }
            return result;
        }

        protected override async Task<EditOrganisationDetailsResult> Validate(EditOrganisationDetailsArgs arguments)
        {
            var result = new EditOrganisationDetailsResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _org = await _organisationService.GetOrganisation(arguments.DetailCard.Id);

            if (_org == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}
