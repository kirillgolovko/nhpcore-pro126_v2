﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class EditOrganisationArgs : IBusinessCommandArguments
    {
        public Organisation organisation { get; set; }
    }
}