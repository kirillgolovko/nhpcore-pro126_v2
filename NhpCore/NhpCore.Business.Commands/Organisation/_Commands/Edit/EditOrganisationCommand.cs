﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditOrganisationCommand : BaseBusinessCommand<EditOrganisationArgs, EditOrganisationResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organisationService;

        private Organisation _org;


        public EditOrganisationCommand(IUnitOfWork<NhpCoreDbContext> uow, IOrganisationService organisationService, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            _uow = uow;
            _organisationService = organisationService;
        }

        protected override async Task<EditOrganisationResult> PerformCommand(EditOrganisationArgs arguments)
        {
            var result = new EditOrganisationResult { IsSuccess = true };
            _org.Name = arguments.organisation.Name;
            try
            {
                _uow.BeginTransaction();
                var repository = _uow.GetRepository<Organisation>().Update(_org);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
            }
            return result;
        }

        protected override async Task<EditOrganisationResult> Validate(EditOrganisationArgs arguments)
        {
            var result = new EditOrganisationResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _org = await _organisationService.GetOrganisation(arguments.organisation.Id);

            if (_org == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}