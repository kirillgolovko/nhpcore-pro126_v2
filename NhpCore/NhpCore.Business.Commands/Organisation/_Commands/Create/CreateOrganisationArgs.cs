﻿using Common.Commands;
using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateOrganisationArgs : IBusinessCommandArguments
    {
        public int CreatorId { get; set; }
        public string ShortName { get; set; }
    }
}