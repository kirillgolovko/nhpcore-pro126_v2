﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class DeleteUserProfileCommand : BaseBusinessCommand<DeleteUserProfileArgs, EmptyBusinessCommandResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IUserService _userService;

        private UserProfile _profile;

        public DeleteUserProfileCommand(
            IUnitOfWork<NhpCoreDbContext> uow,
            IUserService userService,
            ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

            _uow = uow;
            _userService = userService;
        }

        protected override async Task<EmptyBusinessCommandResult> PerformCommand(DeleteUserProfileArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };
            try
            {
                _uow.BeginTransaction();
                _uow.GetRepository<UserProfile>().Remove(_profile);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.UserProfile.USERPROFILE_DELETING_ERROR, BusinessErrors.UserProfile.USERPROFILE_DELETING_ERROR_CODE);
            }
            return result;
        }

        protected override async Task<EmptyBusinessCommandResult> Validate(DeleteUserProfileArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };
            if (result == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _profile = await _userService.GetUserProfile(arguments.Id);
            if (_profile == null)
            {
                result.AddError(BusinessErrors.UserProfile.USERPROFILE_NOT_FOUND, BusinessErrors.UserProfile.USERPROFILE_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}