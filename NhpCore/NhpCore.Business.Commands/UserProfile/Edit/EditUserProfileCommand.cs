﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class EditUserProfileCommand : BaseBusinessCommand<EditUserProfileArgs, EditUserProfileResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IUserService _userService;

        private UserProfile _profile;

        public EditUserProfileCommand(IUnitOfWork<NhpCoreDbContext> uow,IUserService userService,ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            _uow = uow;
            _userService = userService;
        }

        protected override async Task<EditUserProfileResult> PerformCommand(EditUserProfileArgs arguments)
        {
            var result = new EditUserProfileResult { IsSuccess = true };
            //Вношу изменения в запись профиля
            _profile = arguments.Profile;
            //Сохраняю данные
            try
            {
                _uow.BeginTransaction();
                var repository = _uow.GetRepository<UserProfile>().Update(_profile);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.UserProfile.USERPROFILE_EDITING_ERROR, BusinessErrors.UserProfile.USERPROFILE_EDITING_ERROR_CODE);
            }
            return result;
        }

        protected override async Task<EditUserProfileResult> Validate(EditUserProfileArgs arguments)
        {
            var result = new EditUserProfileResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            // Получаю данные профиля по его ИД
            _profile = await _userService.GetUserProfile(arguments.Profile.Id);

            if (_profile == null)
            {
                result.AddError(BusinessErrors.UserProfile.USERPROFILE_NOT_FOUND, BusinessErrors.UserProfile.USERPROFILE_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}
