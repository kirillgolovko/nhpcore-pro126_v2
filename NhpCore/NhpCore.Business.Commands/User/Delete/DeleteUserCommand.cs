﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class DeleteUserCommand : BaseBusinessCommand<DeleteUserArgs, EmptyBusinessCommandResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IUserService _userService;

        private User _user;

        public DeleteUserCommand(
            IUnitOfWork<NhpCoreDbContext> uow,
            IUserService userService,
            ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

            _uow = uow;
            _userService = userService;
        }

        protected override async Task<EmptyBusinessCommandResult> PerformCommand(DeleteUserArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };

            _uow.GetRepository<User>().Remove(_user);
            await _uow.SaveChangesAsync();

            return result;
        }

        protected override async Task<EmptyBusinessCommandResult> Validate(DeleteUserArgs arguments)
        {
            var result = new EmptyBusinessCommandResult { IsSuccess = true };

            if (result == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _user = await _userService.GetCoreUser(arguments.UserId);

            if (_user == null)
            {
                result.AddError(BusinessErrors.User.USER_NOT_FOUND, BusinessErrors.User.USER_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}