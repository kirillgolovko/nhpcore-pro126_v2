﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class EditUserCommand : BaseBusinessCommand<EditUserArgs, EmptyBusinessCommandResult>
    {
        public EditUserCommand(ILoggerUtility logger) : base(logger)
        {
        }

        protected override Task<EmptyBusinessCommandResult> PerformCommand(EditUserArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<EmptyBusinessCommandResult> Validate(EditUserArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}