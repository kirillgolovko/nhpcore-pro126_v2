﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateUserArgs : IBusinessCommandArguments
    {
        //public int IdentityId { get; set; }
        public int Id { get; set; }
    }
}