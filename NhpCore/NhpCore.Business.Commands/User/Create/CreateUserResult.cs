﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateUserResult : BaseBusinessCommandResult
    {
        /// <summary>
        /// Id созданного пользователя
        /// </summary>
        public int CreatedUserId { get; set; }

        /// <summary>
        /// IdentityId созданного пользователя пользователя
        /// </summary>
        public int IdentityId { get; set; }

        /// <summary>
        /// Отображаемое имя пользователя
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        public string PatronymicName { get; set; }
    }
}