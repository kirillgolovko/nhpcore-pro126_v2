﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class AddStorageCredentialsArgs : IBusinessCommandArguments
    {
        public int OrganisationId { get; set; }
        public int UserId { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public string ExpiresIn { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public AccessType AccessType { get; set; }
        public StorageType StorageType { get; set; }
    }
}
