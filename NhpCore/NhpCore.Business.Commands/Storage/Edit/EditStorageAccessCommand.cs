﻿using Common.Commands;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class EditStorageAccessCommand : BaseBusinessCommand<EditStorageAccessArgs, EditStorageAccessResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;

        private OrganisationDocumentsStorageAccess _odsa;

        public EditStorageAccessCommand(IUnitOfWork<NhpCoreDbContext> uow,
            IOrganisationService organisationService,
            IUserService userService,
            ILoggerUtility logger)
            : base(logger)
        {
            if (uow == null) throw new ArgumentNullException(nameof(uow));
            if (organisationService == null) throw new ArgumentNullException(nameof(organisationService));
            if (userService == null) throw new ArgumentNullException(nameof(userService));

            _uow = uow;
            _organisationService = organisationService;
            _userService = userService;
        }

        protected override async Task<EditStorageAccessResult> PerformCommand(EditStorageAccessArgs arguments)
        {
            var result = new EditStorageAccessResult { IsSuccess = true };

            _odsa.AccessType = arguments.dto.AccessType;
            _odsa.StorageType = arguments.dto.StorageType;
            _odsa.AccessName = arguments.dto.AccessName;
            _odsa.ServerUri = arguments.dto.ServerUri;
            if (arguments.dto.AccessType == AccessType.Token)
            {
                _odsa.Token = arguments.dto.Token;
                _odsa.RefreshToken = arguments.dto.RefreshToken;
                _odsa.ExpiresIn = arguments.dto.ExpiresIn;

                _odsa.Email = null;
                _odsa.Password = null;
            }
            else if (arguments.dto.AccessType == AccessType.LoginPassword)
            {
                _odsa.Token = null;
                _odsa.RefreshToken = null;
                _odsa.ExpiresIn = null;

                _odsa.Email = arguments.dto.Email;
                _odsa.Password = arguments.dto.Password;
            }

            try
            {
                _uow.BeginTransaction();
                var repo = _uow.GetRepository<OrganisationDocumentsStorageAccess>().Update(_odsa);
                await _uow.SaveChangesAsync();
                _uow.CommitTransaction();
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.Organisation.STORAGEACCESS_EDITING_ERROR, BusinessErrors.Organisation.STORAGEACCESS_EDITING_ERROR_CODE);
            }
            result.Id = _odsa.Id;

            return result;
        }

        protected override async Task<EditStorageAccessResult> Validate(EditStorageAccessArgs arguments)
        {
            var result = new EditStorageAccessResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _odsa = await _organisationService.GetOrganisationStorageAccess(arguments.dto.Id);

            if (_odsa == null)
            {
                result.AddError(BusinessErrors.Organisation.STORAGEACCESS_NOT_FOUND, BusinessErrors.Organisation.STORAGEACCESS_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}
