﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using NhpCore.DTOs;

namespace NhpCore.Business.Commands
{
    public class EditStorageAccessArgs : IBusinessCommandArguments
    {
        public dtoOrganisationStorageAccess dto { get; set; }
    }
}
