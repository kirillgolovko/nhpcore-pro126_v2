﻿using Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace NhpCore.Infrastructure.Utils.EntityAnnotationValidation
{
    /// <summary>
    /// Валидация сущности на основании DataAnnotations
    /// </summary>
    public class EntityAnnotationValidator : IAnnotationValidator
    {
        /// <summary>
        /// Валидация сущности, DataAnnotation атрибуты должны находиться непосредственно в классе
        /// </summary>
        /// <typeparam name="T">тип валидируемого сущности</typeparam>
        /// <param name="obj">экземпляр валидируемого сущности</param>
        /// <returns cref="AnnotationValidationResult">Результат валидации</returns>
        public AnnotationValidationResult IsValid<T>(T obj)
        {
            return IsValid<T, T>(obj);
        }

        /// <summary>
        /// Валидация сущности, DataAnnotation атрибуты находятся в файле метаданных
        /// </summary>
        /// <typeparam name="T">тип валидируемой сущности</typeparam>
        /// <typeparam name="U">метаданные содержащие атрибуты DataAnnotation валидируемой сущности</typeparam>
        /// <param name="obj">эземпляр валидируемой сущности</param>
        /// <returns cref="AnnotationValidationResult">Результат валидации</returns>
        public AnnotationValidationResult IsValid<T, U>(T obj)
        {
            var errors = new Dictionary<string, string>();
            AnnotationValidationResult result = new AnnotationValidationResult(errors);

            //привязка файла класса с метаданными к сущности
            if (typeof(T) != typeof(U))
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(T), typeof(U)), typeof(T));
            }

            var validationContext = new ValidationContext(obj, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(obj, validationContext, validationResults, true);


            foreach (var validationResult in validationResults)
            {
                errors.Add(validationResult.MemberNames.First(), validationResult.ErrorMessage);
            }

            return result;
        }
    }

}
