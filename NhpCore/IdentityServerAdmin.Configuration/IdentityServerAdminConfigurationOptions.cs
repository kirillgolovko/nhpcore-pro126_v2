﻿namespace IdentityServerAdmin.Configuration
{
    public class IdentityServerAdminConfigurationOptions
    {
        public bool IsSslRequired { get; set; }
        public string IdSrvConnStr { get; set; }
    }
}