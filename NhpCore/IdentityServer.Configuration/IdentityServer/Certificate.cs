﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace IdentityServer.Configuration
{
    public class Certificate
    {
        /// https://github.com/IdentityServer/IdentityServer3/issues/1714
        public static X509Certificate2 Load(string path, string name, string password)
        {
#if DEBUG
            name = "idsrv3test";
            password = "idsrv3test";
#endif
            var certPath = string.Format(path, AppDomain.CurrentDomain.BaseDirectory, name);
#if DEBUG
            var cert = new X509Certificate2(certPath, password);
            return cert;
#endif
            var asembly = typeof(Certificate).Assembly;
            // pwd for idsrv3test.pfx: idsrv3test / onlinenhpsoft:helgard240490
            using (var stream = new FileStream(certPath, FileMode.Open))
            {
                return new X509Certificate2(ReadStream(stream), password, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            }
        }

        private static byte[] ReadStream(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}