﻿using IdentityServer3.Core;
using IdentityServer3.Core.Services.InMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace IdentityServer.Configuration
{
    public class Users
    {
        public static List<InMemoryUser> Get()
        {
            var result = new List<InMemoryUser>();

            result.Add(new InMemoryUser
            {
                Subject = "1",
                Enabled = true,
                Username = "admin",
                Password = "admin",
                Claims = new List<Claim>
                {
                    new Claim(Constants.ClaimTypes.Role, "Admin")
                }
            });
            result.Add(new InMemoryUser
            {
                Subject = "818727",
                Username = "alice",
                Password = "alice",
                Claims = new Claim[]
                    {
                        new Claim(Constants.ClaimTypes.Name, "Alice Smith"),
                        new Claim(Constants.ClaimTypes.GivenName, "Alice"),
                        new Claim(Constants.ClaimTypes.FamilyName, "Smith"),
                        new Claim(Constants.ClaimTypes.Email, "AliceSmith@email.com"),
                        new Claim(Constants.ClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                        new Claim(Constants.ClaimTypes.Role, "Admin"),
                        new Claim(MyIdSrvConstants.ClaimTypes.Subscription, "NhpProjectPROGRAM"),
                        new Claim(Constants.ClaimTypes.WebSite, "http://alice.com"),
                        new Claim(Constants.ClaimTypes.Address, @"{ ""street_address"": ""One Hacker Way"", ""locality"": ""Heidelberg"", ""postal_code"": 69118, ""country"": ""Germany"" }", Constants.ClaimValueTypes.Json)
                    }
            });

            return result;
        }
    }
}