﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Configuration
{
    public static class MyIdSrvConstants
    {
        public const string Path = "https://localhost:44333";
        public const string IdentityRoute = "identity";
        public const string BaseAddress = Path + IdentityRoute;

        public const string AuthorizeEndpoint = BaseAddress + "/connect/authorize";
        public const string LogoutEndpoint = BaseAddress + "/connect/endsession";
        public const string TokenEndpoint = BaseAddress + "/connect/token";
        public const string UserInfoEndpoint = BaseAddress + "/connect/userinfo";
        public const string IdentityTokenValidationEndpoint = BaseAddress + "/connect/identitytokenvalidation";
        public const string TokenRevocationEndpoint = BaseAddress + "/connect/revocation";

        public static class Scopes
        {
            public const string Prefix = "core.";
            public const string Subscription = Prefix+"subscriptions";
        }

        public static class ClaimTypes
        {
            public const string Module = "module";
            public const string Subscription = "subscription";
        }
    }
}
