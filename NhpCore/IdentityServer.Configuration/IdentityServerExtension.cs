﻿using CoreApp.IdentityServer;
using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Services;
using IdentityServer3.EntityFramework;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityServer3.Core.Services.Default;
using mngInMem = IdentityManager.Configuration.InMemoryService;
using admInMem = IdentityServerAdmin.Configuration.InMemoryService;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services.Contrib;

namespace IdentityServer.Configuration
{
    public static class IdentityServerExtension
    {
        public static void ConfigureIdentityServer(this IAppBuilder app, IdentityServerConfurationOptions options)
        {
            var idSrvFactory = new IdentityServerServiceFactory();

            //#if DEBUG
            var efConfig = new EntityFrameworkServiceOptions { ConnectionString = options.IdSrvDbConnStr };

            // these two calls just pre-populate the test DB from the in-memory config
            ConfigureClients(Clients.Get(), efConfig);
            ConfigureScopes(Scopes.Get(), efConfig);

            //idsrv ef for scopes and clients
            idSrvFactory.RegisterOperationalServices(efConfig);
            idSrvFactory.RegisterConfigurationServices(efConfig);

            //idsrv aspnet.identity ef for interop with user identities
            idSrvFactory.Register(new Registration<NhpIdentityDbContext>(resolver => new NhpIdentityDbContext(options.IdentityDbConnStr)));
            idSrvFactory.Register(new Registration<CustomUserStore>());
            idSrvFactory.Register(new Registration<CustomUserManager>());
            idSrvFactory.UserService = new Registration<IUserService, CustomUserService>();
            idSrvFactory.CorsPolicyService = new Registration<ICorsPolicyService>(resolver => new DefaultCorsPolicyService() { AllowAll = true });

            options.SetCustomViews(idSrvFactory);

            // localization
            var localeOptions = new LocaleOptions { LocaleProvider = env => "ru-Ru" };
            idSrvFactory.Register(new Registration<LocaleOptions>(localeOptions));
            idSrvFactory.LocalizationService = new Registration<ILocalizationService, GlobalizedLocalizationService>();


            //seq events
            idSrvFactory.EventService = new Registration<IEventService, SeqEventService>();

            var loggingOptions = new LoggingOptions
            {
                EnableHttpLogging = true
            };


            var idsrvOption = new IdentityServerOptions
            {
                Factory = idSrvFactory,
                SigningCertificate = Certificate.Load(options.CertificatePath, options.CertificateName, options.CertificatePassword),
                LoggingOptions = loggingOptions,
                SiteName = options.SiteName,
                EnableWelcomePage = false,
#if DEBUG
                RequireSsl = options.RequireSsl,
#endif
            };

            app.Map("/identity", appIdSrv => appIdSrv.UseIdentityServer(idsrvOption));
        }

        public static void ConfigureClients(IEnumerable<Client> clients, EntityFrameworkServiceOptions options)
        {
            using (var db = new ClientConfigurationDbContext(options.ConnectionString, options.Schema))
            {
                if (!db.Clients.Any())
                {
                    foreach (var c in clients)
                    {
                        var e = c.ToEntity();
                        db.Clients.Add(e);
                    }
                    db.SaveChanges();
                }
            }
        }

        public static void ConfigureScopes(IEnumerable<Scope> scopes, EntityFrameworkServiceOptions options)
        {
            using (var db = new ScopeConfigurationDbContext(options.ConnectionString, options.Schema))
            {
                if (!db.Scopes.Any())
                {
                    foreach (var s in scopes)
                    {
                        var e = s.ToEntity();
                        db.Scopes.Add(e);
                    }
                    db.SaveChanges();
                }
            }
        }
    }
}