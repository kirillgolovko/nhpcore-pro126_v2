﻿using System.Threading.Tasks;
using IdentityServer3.AspNetIdentity;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;

namespace IdentityServer.Configuration
{
    public class CustomUserService : AspNetIdentityUserService<CustomUser, int>
    {
        public CustomUserService(CustomUserManager userManager) : base(userManager)
        {
        }

        protected override async Task<CustomUser> FindUserAsync(string username)
        {
            // var user = await userManager.FindByNameAsync(username);
            var user = await userManager.FindByEmailAsync(username);
            return user;
        }
    }
}