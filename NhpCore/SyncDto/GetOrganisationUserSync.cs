﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncDto
{
    public class GetOrganisationUserSync
    {
        public int Organisation_Id { get; set; }
        public int User_Id { get; set; }
    }
}
