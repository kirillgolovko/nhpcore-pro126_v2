﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncDto
{
    public class AddUserToOrganisationSyncReq
    {
        public int UserId { get; set; }
        public int OrganisationId { get; set; }
    }
}
