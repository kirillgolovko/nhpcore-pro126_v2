﻿using IdentityManager.AspNetIdentity;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;

namespace IdentityManager.Configuration
{
    public class CustomIdentityManagerService : AspNetIdentityManagerService<CustomUser, int, CustomRole, int>
    {
        public CustomIdentityManagerService(CustomUserManager userManager, CustomRoleManager roleManager)
            : base(userManager, roleManager)
        {
        }
    }
}
