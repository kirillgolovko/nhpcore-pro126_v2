﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.CoreLayer.Services
{
    public interface IDepartmentService
    {
        Task<IEnumerable<Department>> GetList();
        Task<IEnumerable<Department>> GetAvailable(int organisationId);
        Task<Department> Get(int Id);
    }
}
