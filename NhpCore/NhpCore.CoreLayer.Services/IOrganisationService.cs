﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Services
{
    public interface IOrganisationService
    {
        Task<Organisation> GetOrganisation(int id);
        Task<OwnerDetails> GetOwnerDetails();
        Task<IEnumerable<Organisation>> GetOrganisations();
        Task<IEnumerable<Organisation>> GetOrganisationList(IEnumerable<int> ids);
        Task<OrganisationUserEntry> GetUserEntry(int organisationId, int userId);
        Task<IEnumerable<OrganisationUserEntry>> GetUserEntries(int organisationId);
        Task<IEnumerable<OrganisationUserInvite>> GetInvitedUsers(int organisationId);
        Task<OrganisationUserInvite> GetInvitedUser(Guid inviteId);
        Task<OrganisationUserRoleType> GetOrganisationUserRoleTypeByTitle(string title);
        Task<IEnumerable<CustomUser>> GetIdentityUsers(IEnumerable<string> emails);
        Task<OrganisationDocumentsStorageAccess> GetOrganisationStorageAccess(int storageId);
        Task<IEnumerable<OrganisationDocumentsStorageAccess>> GetStorageAccess(int organisationId);
        Task<IEnumerable<OrganisationUserRoleType>> GetOrganisationUserRoleTypes();
    }
}