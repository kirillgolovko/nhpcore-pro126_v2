﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.CoreLayer.Services
{
    public interface ISubscriptionService
    {
        Task<IEnumerable<ITProduct>> GetAvailable();
        Task<IEnumerable<Subscription>> GetSubscriptions(int[] subscriptionIds);
        Task<IEnumerable<UserSubscriptionEntry>> GetUserEntries(int subscriptionId, IEnumerable<int> userIds);
        Task<SubscriptionPayment> GetSubscriptionPayment(Guid Id);
        Task<SubscriptionPayment> GetSubscriptionPayment(string code);
        Task<IEnumerable<SubscriptionPayment>> GetSubscriptionPayments(int orgId);
        Task<IEnumerable<SubscriptionPaymentBill>> GetSubscriptionPaymentBillList(int orgId);
        Task<SubscriptionPaymentBill> GetSubscriptionPaymentBill(int BillId);
    }
}
