﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities.Enums
{
   public enum TimePeriodType
    {
        year,
        month,
        week,
        day,
        hour
    }
}
