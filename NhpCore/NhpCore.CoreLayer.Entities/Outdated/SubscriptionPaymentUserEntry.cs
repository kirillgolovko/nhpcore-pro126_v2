﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Позиция пользователя которому приобретается подписка
    /// </summary>
    public class SubscriptionPaymentUserEntry : IEntity
    {
        public Guid Id { get; set; }
        public int PayeeId { get; set; }
        public User Payee { get; set; }
        public Guid SubscriptionPaymentId { get; set; }
        public virtual SubscriptionPayment SubscriptionPayment { get; set; }
        // статус - оплачено/не оплачено
        public bool IsPayed { get; set; }

        public DateTime? InvalidationDate { get; set; }
    }
}
