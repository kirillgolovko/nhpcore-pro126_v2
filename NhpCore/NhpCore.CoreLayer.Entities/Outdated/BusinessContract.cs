﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Договор
    /// </summary>
    public class BusinessContract : IEntity
    {
        public int Id { get; set; }

        

        // Реквизиты для договора и счета
        public int ContractDetailsId { get; set; }
        public virtual BusinessContractDetails ContractDetails { get; set; }

        public virtual BusinessContractBill BusinessContractBill { get; set; }
    }
}
