﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Счёт
    /// </summary>
    public class BusinessContractBill : IEntity
    {
        [Key, ForeignKey("BusinessContract")]
        public int Id { get; set; }

        public virtual BusinessContract BusinessContract { get; set; }

        public DateTime BillingDate { get; set; }
        public DateTime BillingTime { get; set; }

        // сумма счета
        public double Sum { get; set; }

        // включен ли НДС - true/false
        public bool IsNdsIncluded { get; set; }

        // позиции по выставленному счету
        public virtual ICollection<BusinessContractBillEntry> BillEntries { get; set; }

        public BusinessContractBill()
        {
            BillEntries = new HashSet<BusinessContractBillEntry>();
        }
    }
}
