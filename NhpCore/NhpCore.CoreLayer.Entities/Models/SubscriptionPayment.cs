﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Информация о платеже на подписку. 
    /// </summary>
    public class SubscriptionPayment : IEntity
    {
        public Guid Id { get; set; }

        public bool IsPayed { get; set; }

        /// <summary>
        /// Код счета по которому куплена подписка
        /// </summary>
        public int BillId { get; set; }
        /// <summary>
        /// Купленная подписка
        /// </summary>
        public int BoughtSubscriptionId { get; set; }
        public virtual Subscription BoughtSubscription { get; set; }

        /// <summary>
        /// Дата успешной оплаты
        /// </summary>
        public DateTime? DateOfSuccesfullyPaid { get; set; }

        /// <summary>
        /// Дата истечения подписки
        /// </summary>
        public DateTime? InvalidationTime { get; set; }

        /// <summary>
        /// Компания инициировавшая покупку
        /// </summary>
        public int BuyeerId { get; set; }
        public virtual Organisation Buyeer { get; set; }

        /// <summary>
        /// Дата успешной активации
        /// </summary>
        public DateTime? DateOfSuccesfullyActivation { get; set; }

        /// <summary>
        /// Пользователь инициировавший покупку
        /// </summary>
        public int BuyeerUserId { get; set; }
        public virtual User BuyeerUser { get; set; }

        /// <summary>
        /// Лицензионный код по которому будет вестись активация лицензии
        /// </summary>
        public Guid LicenseCode { get; set; }
        /// <summary>
        /// Количество купленных лицензий
        /// </summary>
        public int Quantity { get; set; }
        public bool IsActivatedLicense { get; set; }

        // 
        /// <summary>
        /// Компания-активатор лицензии
        /// </summary>
        public int EndCustomerOrganisationId { get; set; }
        public virtual Organisation EndCustomerOrganisation { get; set; }

        /// <summary>
        /// Пользователь активировавший лицензию
        /// </summary>
        public int EndCustomerUserId { get; set; }
        public virtual User EndCustomerUser { get; set; }

        public virtual ICollection<UserSubscriptionEntry> UserEntries { get; set; }

        public SubscriptionPayment()
        {
            UserEntries = new HashSet<UserSubscriptionEntry>();
        }
    }
}
