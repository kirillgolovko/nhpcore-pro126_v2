﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationDetailCard : IEntity
    {
        [Key, ForeignKey("Organisation")]
        public int Id { get; set; }
        public virtual Organisation Organisation { get; set; }
        public string FullName { get; set; }
        public string Logo { get; set; }

        [Required]
        public string ShortName { get; set; }
        public string OGRN { get; set; } // ОГРН - основной государственный регистрационный номер
        [Required]
        public string INN { get; set; } //ИНН - Идентификационный номер налогоплательщика
        public string KPP { get; set; } // КПП - Код причины постановки на учет
        public string OKPO { get; set; } // ОКПО - Общероссийский классификатор предприятий и организаций
        public string OKATO { get; set; } // ОКАТО - Общероссийский классификатор объектов административно-территориального деления
        [Required]
        public string LegalAddress { get; set; } // Юр.адрес лица
        public string PhysicalAddress { get; set; } // Фактический адрес
        [Required]
        public string PhoneNumber { get; set; } // Номер телефона
        public string PhoneNumberAdditional { get; set; } // добавочный номер к номеру телефона
        [Required]
        public string Email { get; set; }
        [Required]
        public string DirectorString { get; set; } // ФИО Директора, используется если пользователь - "Директор"  еще не зарегистрирован
        public int? DirectorId { get; set; } // Id директора
        public virtual User Director { get; set; }
        [Required]
        public string ChiefAccountantString { get; set; } // ФИО ГлавногоБухгалтера, используется если пользователь - "ГлавБух"  еще не зарегистрирован
        public int? ChiefAccountantId { get; set; } // Id ГлавБуха
        public virtual User ChiefAccountant { get; set; }

        #region Банковские реквизиты
        [Required]
        public string BankCorporateAccount { get; set; } // Р/с Расчетный счёт
        [Required]
        public string BankName { get; set; } // Название банка
        public string BankAddress { get; set; } //Адрес банка <- а вот это еще зачем? никому и никогда Адрес банка не нужен
        public string BankOGRN { get; set; } // ОГРН <- а вот это еще зачем? никому и никогда ОГРН банка не нужен
        public string BankOKPO { get; set; } // ОКПО <- а вот это еще зачем? никому и никогда ОКПО банка не нужен
        [Required]
        public string BankBIK { get; set; } // БИК
        [Required]
        public string BankCorrespondentAccount { get; set; } // К/с -  Корреспондентский счет
        #endregion
    }
}