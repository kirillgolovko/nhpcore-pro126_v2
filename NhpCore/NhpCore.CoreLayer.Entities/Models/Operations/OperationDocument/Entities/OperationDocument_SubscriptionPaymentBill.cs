﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    // счет
    [MetadataType(typeof(OperationDocument_SubscriptionPaymentBill_Annotation))]
    public class OperationDocument_SubscriptionPaymentBill : OperationDocumentBase
    {
        [Index()]
        public int? BillId { get; set; }
        public int? BillDetailsId { get; set; }
        public virtual BusinessContractDetails BillDetails { get; set; }
    }
    [MetadataType(typeof(OperationDocument_SubscriptionPaymentBill))]
    public class OperationDocument_SubscriptionPaymentBill_Annotation : OperationDocumentBase_Annotation
    {
        [Required]
        public object BillId { get; set; }
        [Required]
        public object BillDetailsId { get; set; }
    }
}
