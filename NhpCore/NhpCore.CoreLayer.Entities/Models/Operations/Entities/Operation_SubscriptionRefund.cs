﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class Operation_SubscriptionRefund : OperationBase
    {
        public int? SubscriptionRefundId { get; set; }
        public virtual SubscriptionRefund SubscriptionRefund { get; set; }
    }
}
