﻿using Common.EntityFramework;
using NhpCore.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [Table(NhpConstants.EntityTableNames.OperationBase)]
    [MetadataType(typeof(OperationBase_Annotation))]
    public abstract class OperationBase : IEntity
    {
        public Guid Id { get; set; }


        public DateTime CreationDate { get; set; }
        public DateTime CreationTime { get; set; }
        public int OperationTypeId { get; set; }
        public virtual OperationType OperationType { get; set; }
        public int OperationStatusId { get; set; }
        public virtual OperationStatus OperationStatus { get; set; }

        public virtual ICollection<OperationUserEntryBase> OperationUserEntries { get; set; }
        public virtual ICollection<OperationDocumentBase> AttachedDocuments { get; set; }
        //    public virtual ICollection<OperationInformationBase> AdditionalOperationInformation { get; set; }


        public OperationBase()
        {
            OperationUserEntries = new HashSet<OperationUserEntryBase>();
            AttachedDocuments = new HashSet<OperationDocumentBase>();
            // AdditionalOperationInformation = new HashSet<OperationInformationBase>();
        }
    }

    [MetadataType(typeof(OperationBase))]
    public class OperationBase_Annotation
    {
        [Required]
        public object OperationTypeId { get; set; }
        [Required]
        public object CreationDate { get; set; }
        [Required]
        public object CreationTime { get; set; }
    }

}
