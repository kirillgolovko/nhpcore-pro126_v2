﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class SubscriptionPaymentBill : IEntity
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public int BuyeerId { get; set; }
        public virtual Organisation Buyeer { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public double Total { get; set; }
        public bool IsPayed { get; set; }
        public virtual ICollection<SubscriptionPaymentBillEntry> BillEntry { get; set; }

        public SubscriptionPaymentBill()
        {
            BillEntry = new HashSet<SubscriptionPaymentBillEntry>();
        }
    }
}
