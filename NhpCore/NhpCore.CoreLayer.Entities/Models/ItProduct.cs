﻿using Common.EntityFramework;
using System.Collections.Generic;

namespace NhpCore.CoreLayer.Entities
{
    public class ITProduct : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }

        public virtual ICollection<Subscription> Subscriptions { get; set; }


        public ITProduct()
        {
            Subscriptions = new HashSet<Subscription>();
        }
    }
}