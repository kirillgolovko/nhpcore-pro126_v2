﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationUserRole : IEntity
    {
        public int Id { get; set; }
        public Guid OrganisationUserEntryId { get; set; }
        public virtual OrganisationUserEntry OrganisationUserEntry { get; set; }
        public int OrganisationUserRoleTypeId { get; set; }
        public virtual OrganisationUserRoleType OrganisationUserRoleType { get; set; }
    }
}