﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    // тип Платежа: Безнал, Яндекс, что-то еще
    public class PaymentType:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SubscriptionPayment> Payments { get; set; }

        public PaymentType()
        {
            Payments = new HashSet<SubscriptionPayment>();
        }
    }
}
