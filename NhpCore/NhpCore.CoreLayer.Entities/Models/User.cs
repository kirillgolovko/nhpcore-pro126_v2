﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class User : IEntity, IDeletableState, IDisableableState
    {
        public int Id { get; set; }

        ////  [Index(IsUnique = true)]
        //  public int IdentityId { get; set; }

        public string PhoneNumber { get; set; }
        public string AvatarPath { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsManager { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime? DisablingTime { get; set; }
        public int? LastVisitedOrganisationId { get; set; }
        public Organisation LastVisitedOrganisation { get; set; }


        public virtual ICollection<UserProfile> Profiles { get; set; }
        public virtual ICollection<OrganisationUserInvite> Invites { get; set; }
        public virtual ICollection<OrganisationUserEntry> Organisations { get; set; }
        public virtual ICollection<Department> MemberOfDepartments { get; set; }
        public virtual ICollection<Department> ChiefOfDepartments { get; set; }
        public virtual ICollection<SubscriptionPayment> PaidSubscriptions { get; set; }
        public virtual ICollection<SubscriptionPayment> ActivatedSubscriptions { get; set; }
        public virtual ICollection<OrganisationDetailCard> DirectorAt { get; set; }
        public virtual ICollection<OrganisationDetailCard> AccountantAt { get; set; }


        public User()
        {
            Invites = new HashSet<OrganisationUserInvite>();
            Organisations = new HashSet<OrganisationUserEntry>();
            MemberOfDepartments = new HashSet<Department>();
            ChiefOfDepartments = new HashSet<Department>();
            PaidSubscriptions = new HashSet<SubscriptionPayment>();
            ActivatedSubscriptions = new HashSet<SubscriptionPayment>();
            DirectorAt = new HashSet<OrganisationDetailCard>();
            AccountantAt = new HashSet<OrganisationDetailCard>();
        }
    }
}