﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class Balance
    {
        [Key, ForeignKey("Organisation")]
        public int Id { get; set; }

        public virtual Organisation Organisation { get; set; }

        public double Value { get; set; }
        public virtual ICollection<BalanceEntry> BalanceLog { get; set; }

        public Balance()
        {
            BalanceLog = new HashSet<BalanceEntry>();
        }
    }
}