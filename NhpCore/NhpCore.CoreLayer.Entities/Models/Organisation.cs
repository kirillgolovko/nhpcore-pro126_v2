﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class Organisation : IEntity, IDeletableState
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime RegisterDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public virtual Balance Balance { get; set; }
        public virtual OrganisationDetailCard Details { get; set; }
        public virtual ICollection<OrganisationDocumentsStorageAccess> StorageAccess { get; set; }
        public virtual ICollection<OrganisationUserInvite> Invites { get; set; }
        public virtual ICollection<OrganisationUserEntry> Users { get; set; }
        public virtual ICollection<SubscriptionPayment> PaidSubscriptions { get; set; }
        public virtual ICollection<SubscriptionPayment> ActivatedSubscriptions { get; set; }
        public virtual ICollection<Department> Departments { get; set; }

        public Organisation()
        {
            StorageAccess = new HashSet<OrganisationDocumentsStorageAccess>();
            Invites = new HashSet<OrganisationUserInvite>();
            Users = new HashSet<OrganisationUserEntry>();
            PaidSubscriptions = new HashSet<SubscriptionPayment>();
            ActivatedSubscriptions = new HashSet<SubscriptionPayment>();
            Departments = new HashSet<Department>();
        }
    }
}