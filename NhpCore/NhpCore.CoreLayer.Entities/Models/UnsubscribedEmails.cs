﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class UnsubscribedEmails : IEntity
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
    }
}
