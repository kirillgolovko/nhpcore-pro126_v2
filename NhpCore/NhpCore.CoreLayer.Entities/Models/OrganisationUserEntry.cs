﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationUserEntry : IEntity, IDeletableState, IDisableableState
    {
        public Guid Id { get; set; }
        public int OrganisationId { get; set; }
        public virtual Organisation Organisation { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public bool? IsOwner { get; set; }
        public bool? IsPersonal { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public DateTime? DisablingTime { get; set; }
        public int? ProfileId { get; set; }
        public double? Salary { get; set; } //Оклад
        public virtual UserProfile Profile { get; set; }

        public virtual OrganisationUserProfile UserProfile { get; set; }
        public virtual ICollection<OrganisationUserRole> UserRoles { get; set; }

        public OrganisationUserEntry()
        {
            UserRoles = new HashSet<OrganisationUserRole>();
        }
    }
}