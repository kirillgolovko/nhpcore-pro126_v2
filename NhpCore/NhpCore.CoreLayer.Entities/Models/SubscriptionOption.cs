﻿using System;
using System.Collections.Generic;

namespace NhpCore.CoreLayer.Entities
{
    public class SubscriptionOption
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public virtual SubscriptionOptionType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }

        public virtual ICollection<Subscription> Subscriptions { get; set; }

        public SubscriptionOption()
        {
            Subscriptions = new HashSet<Subscription>();
        }
    }
}