﻿using System;

namespace NhpCore.CoreLayer.Entities
{
    public class SubscriptionOptionType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}