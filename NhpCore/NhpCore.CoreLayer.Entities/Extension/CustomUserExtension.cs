﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace NhpCore.CoreLayer.Entities.Extension
{
    public static class CustomUserExtension
    {
        public static void SetPassword(this CustomUser user, string password)
        {
            user.PasswordHash = new PasswordHasher().HashPassword(password);
        }
    }
}
