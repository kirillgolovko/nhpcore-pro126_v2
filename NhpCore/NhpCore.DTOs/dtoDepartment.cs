﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.DTOs
{
    public class dtoDepartment
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public IEnumerable<dtoDepartment> availableParents { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public IEnumerable<dtoMember> Members { get; set; }
        public IEnumerable<dtoDepartment> Nodes { get; set; }
    }

    public class dtoMember
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime? DisablingTime { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public IEnumerable<dtoOrganisationUserRoleType> UserRoles { get; set; }
    }

    public class dtoInvitedUsers
    {
        public Guid id { get; set; }
        public int? userId { get; set; }
        public string inviteDate { get; set; }
        public string displayName { get; set; }
        public string email { get; set; }
        public bool status { get; set; }
        public IEnumerable<dtoOrganisationUserRoleType> userroles { get; set; }
    }

    public class dtoOrganisationUserRoleType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsSet { get; set; }
    }



}
