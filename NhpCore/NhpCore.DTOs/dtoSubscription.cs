﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities.Enums;

namespace NhpCore.DTOs
{
    public class dtoSubscription
    {
        public int Id { get; set; }
        public int ItProductId { get; set; }
        public bool IsActive { get; set; }
        public TimePeriodType TimePeriodType { get; set; }
        public string Duration { get; set; }
        public double Price { get; set; }
    }

    public class dtoITProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public List<dtoSubscription> Subscriptions { get; set; }
    }

    public class dtoSubscriptionPayment
    {
        public Guid Id { get; set; }
        public string SubscriptionName { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string BuyeerName { get; set; }
        public string EndCustomerOrganisationName { get; set; }
        public string InvalidationTime { get; set; }
        public Guid LicenseCode { get; set; }
        public bool IsActivatedLicense { get; set; }
        public string DownloadLink { get; set; }
    }

    public class dtoSubscriptionPaymentBill
    {
        public int Id { get; set; }
        public bool IsPayed { get; set; }
        public string DateTime { get; set; }
        public string Name { get; set; }
        public string BuyeerName { get; set; }
        public double Total { get; set; }
        public string DownloadLink { get; set; }
    }

    public class dtoInvoiceItems
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public string Duration { get; set; }
    }
}
