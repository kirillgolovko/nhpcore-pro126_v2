﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.DTOs
{
    public class dtoOrganisation
    {
    }

    public class dtoOrganisationDetailCard
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Logo { get; set; }
        public string ShortName { get; set; }
        public string OGRN { get; set; } // ОГРН - основной государственный регистрационный номер
        public string INN { get; set; } //ИНН - Идентификационный номер налогоплательщика
        public string KPP { get; set; } // КПП - Код причины постановки на учет
        public string OKPO { get; set; } // ОКПО - Общероссийский классификатор предприятий и организаций
        public string OKATO { get; set; } // ОКАТО - Общероссийский классификатор объектов административно-территориального деления
        public string LegalAddress { get; set; } // Юр.адрес лица
        public string PhysicalAddress { get; set; } // Фактический адрес
        public string PhoneNumber { get; set; } // Номер телефона
        public string PhoneNumberAdditional { get; set; } // добавочный номер к номеру телефона
        public string Email { get; set; }
        public string DirectorString { get; set; } // ФИО Директора, используется если пользователь - "Директор"  еще не зарегистрирован
        public int? DirectorId { get; set; } // Id директора
        public string ChiefAccountantString { get; set; } // ФИО ГлавногоБухгалтера, используется если пользователь - "ГлавБух"  еще не зарегистрирован
        public int? ChiefAccountantId { get; set; } // Id ГлавБуха

        public string BankCorporateAccount { get; set; } // Р/с Расчетный счёт
        public string BankName { get; set; } // Название базнка
        public string BankAddress { get; set; } //Адрес банка
        public string BankOGRN { get; set; } // ОГРН
        public string BankOKPO { get; set; } // ОКПО
        public string BankBIK { get; set; } // БИК
        public string BankCorrespondentAccount { get; set; } // К/с -  Корреспондентский счет
    }

    public class dtoOrganisationStorageAccess
    {
        public int Id { get; set; }
        public string AccessName { get; set; }
        public StorageType StorageType { get; set; }
        public AccessType AccessType { get; set; }
        public int OrganisationId { get; set; }
        public int AssignerId { get; set; }
        public string ServerUri { get; set; }
        public DateTime? CreationDateTime { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RefreshToken { get; set; }
        public string ExpiresIn { get; set; }
    }
}
