﻿using Microsoft.AspNet.Identity.EntityFramework;
using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace NhpCore.Infrastructure.Data.Ef
{
    public class NhpIdentityDbContext : IdentityDbContext<CustomUser, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        #region ctor

        static NhpIdentityDbContext()
        {
#if DEBUG
            // Database.SetInitializer(new NhpIdentityDbContextDropCreateAlwaysInitializer());
            Database.SetInitializer(new NhpIdentityDbContextCreateIfNotExistInitializer());
#endif
        }

        public NhpIdentityDbContext()
            : base("NhpIdentityDbContext")
        {
        }

        public NhpIdentityDbContext(string connString)
            : base(connString)
        {
        }

        #endregion ctor

        #region create

        public static NhpIdentityDbContext Create()
        {
            return new NhpIdentityDbContext();
        }

        public static NhpIdentityDbContext Create(string connectionString)
        {
            return new NhpIdentityDbContext(connectionString);
        }

        #endregion create
    }
}