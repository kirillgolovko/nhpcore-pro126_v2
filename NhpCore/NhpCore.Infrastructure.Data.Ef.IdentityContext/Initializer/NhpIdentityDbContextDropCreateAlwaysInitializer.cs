﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Initializer
{
    public class NhpIdentityDbContextDropCreateAlwaysInitializer : DropCreateDatabaseAlways<NhpIdentityDbContext>
    {
    }
}