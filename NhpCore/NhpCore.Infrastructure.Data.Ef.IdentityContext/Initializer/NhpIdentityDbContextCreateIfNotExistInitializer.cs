﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef
{
    class NhpIdentityDbContextCreateIfNotExistInitializer : CreateDatabaseIfNotExists<NhpIdentityDbContext>
    {
        protected override void Seed(NhpIdentityDbContext context)
        {
            base.Seed(context);

            //var releaseConnStr = GetConnectionString("NhpIdentityDbContext", "Release");
            //var debugConnStr = GetConnectionString("NhpIdentityDbContext", "Debug");

            //if (releaseConnStr != string.Empty && debugConnStr != string.Empty)
            //{
            //    List<CustomUser> users = null; // берем пользователей из продакшен базы
            //    using (NhpIdentityDbContext db = new NhpIdentityDbContext(releaseConnStr))
            //    {
            //        users = db.Users.ToList();
            //    }
            //    if (users != null)
            //    {   // закидывам на локальную дебаг базу
            //        using (NhpIdentityDbContext db = new NhpIdentityDbContext(debugConnStr))
            //        {
            //            var maxId = users.Max(x => x.Id);

            //            for (int i = 1; i <= maxId; i++)
            //            {
            //                var user = users.FirstOrDefault(x => x.Id == i);
            //                if (user != null)
            //                {
            //                    db.Users.Add(user);
            //                    db.SaveChanges();
            //                }
            //                else
            //                {
            //                    var newUser = new CustomUser {
            //                        Email = "test@test",
            //                        EmailConfirmed = true,
            //                        PhoneNumberConfirmed = false,
            //                        TwoFactorEnabled = false,
            //                        LockoutEnabled = false,
            //                        AccessFailedCount = 0,
            //                        UserName = $"test"
            //                    };
            //                    if (newUser.UserName == "test")
            //                    {
            //                        var str = Guid.NewGuid().ToString().Split('-').First().Substring(0, 3);
            //                        newUser.UserName += str;
            //                    }
            //                    db.Users.Add(newUser);
            //                    db.SaveChanges();
            //                }
            //            }

            //            var list = db.Users.Where(x => x.Email == "test@test").ToList();

            //            list.ForEach(x => db.Users.Remove(x));

            //            db.SaveChanges();
            //        }
            //    }
            //    else
            //    {
            //        throw new Exception("something went wrong");
            //    }
            //}
            //else
            //{
            //    throw new Exception("one of connection strings are empty");
            //}
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="mode"></param>
        /// <returns>строка подключения, в случае ошибки - пустая строка (string.Empty)</returns>
        private string GetConnectionString(string name, string mode)
        {
            string result = string.Empty;

            string path = AppDomain.CurrentDomain.BaseDirectory + $@"EnvironmentConfiguration\ConnectionStrings-{mode}.config";

            var isExist = File.Exists(path);
            if (isExist)
            {
                StreamReader sr = new StreamReader(path);

                while (result == string.Empty)
                {
                    var line = sr.ReadLine();
                    var splitedBySpace = line.Split(' ');
                    if (splitedBySpace.Length > 0 && splitedBySpace[0] != "<connectionStrings>")
                    {
                        if (splitedBySpace[3] == $@"name=""{name}""")
                        {

                            result = line.Split('"')[3];
                        }
                    }
                }
            }

            return result;
        }
    }
}
