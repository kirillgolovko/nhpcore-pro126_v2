﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NhpCore.Business.Errors
{
    public static class BusinessErrors
    {
        /// <summary>
        /// Системные ошибки
        /// </summary>
        public static class System
        {
            public const string INVALID_REQUEST = "Неверный запрос";
            public const int INVALID_REQUEST_CODE = 1000;

            public const string ARGUMENT_IS_NULL = "Argument is null";
            public const int ARGUMENT_IS_NULL_CODE = 1001;

            public const string INTERNAL_SERVER_ERROR = "Внутренняя ошибка сервера :( Повторите попытку позже.";
            public const int INTERNAL_SERVER_ERROR_CODE = 1002;
        }

        public static class User
        {
            public const string USER_NOT_FOUND = "Пользователь не найден";
            public const int USER_NOT_FOUND_CODE = 1003;

            public const string USER_ALREADY_EXIST = "Такой пользователь уже существует";
            public const int USER_ALREADY_EXIST_CODE = 1006;

            public const string CREATING_ERROR = "Возникла проблема во время создания пользователя";
            public const int CREATING_ERROR_CODE = 1008;

            public const string USERINVITE_NOT_FOUND = "Приглашение пользователя не найдено";
            public const int USERINVITE_NOT_FOUND_CODE = 1012;

            public const string CLAIM_DISPLAYNAME_NOT_FOUND = "DisplayName не найден";
            public const int CLAIM_DISPLAYNAME_NOT_FOUND_CODE = 1013;
        }

        public static class Organisation
        {
            public const string ORGANISATION_NOT_FOUND = "Организация не найдена";
            public const int ORGANISATION_NOT_FOUND_CODE = 1004;

            public const string ORGANISATION_USER_ENTRY_NOT_FOUND = "Запись о пользователе организации не найдена";
            public const int ORGANISATION_USER_ENTRY_NOT_FOUND_CODE = 1005;

            public const string ORGANISATION_USERINVITE_ENTRY_NOT_FOUND = "Запись о приглашенном организацией пользователе не найдена";
            public const int ORGANISATION_USERINVITE_ENTRY_NOT_FOUND_CODE = 1007;

            public const string CREATING_ERROR = "Возникла проблема при создании организации";
            public const int CREATING_ERROR_CODE = 1009;

            public const string ORGANISATION_USER_ENTRY_CREATING_ERROR = "Возникла проблема при создании записи пользователя в организации";
            public const int ORGANISATION_USER_ENTRY_CREATING_ERROR_CODE = 1010;

            public const string ORGANISATION_USER_ROLE_NOT_FOUND = "Запись о роли пользователя организации не найдена";
            public const int ORGANISATION_USER_ROLE_NOT_FOUND_CODE = 1011;

            public const string ORGANISATION_USERENTRY_ROLE_CREATING_ERROR = "Возникла проблема при создании роли пользователя в организации";
            public const int ORGANISATION_USERENTRY_ROLE_CREATING_ERROR_CODE = 1012;

            public const string ORGANISATION_USERENTRY_ROLE_DELETING_ERROR = "Возникла проблема при удалении роли пользователя в организации";
            public const int ORGANISATION_USERENTRY_ROLE_DELETING_ERROR_CODE = 1013;

            public const string DEPARTMENT_NOT_FOUND = "Подразделение не найдено";
            public const int DEPARTMENT_NOT_FOUND_CODE = 1021;

            public const string DEPARTMENT_CREATING_ERROR = "Ошибка создания подразделения";
            public const int DEPARTMENT_CREATING_ERROR_CODE = 1022;

            public const string DEPARTMENT_EDITING_ERROR = "Ошибка редактирования подразделения";
            public const int DEPARTMENT_EDITING_ERROR_CODE = 1023;

            public const string DEPARTMENT_DELETING_ERROR = "Ошибка удаления подразделения";
            public const int DEPARTMENT_DELETING_ERROR_CODE = 1024;

            public const string STORAGEACCESS_NOT_FOUND = "Сервер WEBDav не найден";
            public const int STORAGEACCESS_NOT_FOUND_CODE = 1031;

            public const string STORAGEACCESS_CREATING_ERROR = "Ошибка создания сервера WEBDav";
            public const int STORAGEACCESS_CREATING_ERROR_CODE = 1032;

            public const string STORAGEACCESS_EDITING_ERROR = "Ошибка редактирования сервера WEBDav";
            public const int STORAGEACCESS_EDITING_ERROR_CODE = 1033;
        }

        public static class UserProfile
        {
            public const string USER_NOT_FOUND = "Пользователь для профиля не найден";
            public const int USER_NOT_FOUND_CODE = 4001;

            public const string USERPROFILE_NOT_FOUND = "Профиль не существует";
            public const int USERPROFILE_NOT_FOUND_CODE = 4002;

            public const string USERPROFILE_CREATING_ERROR = "Возникла проблема при создании профиля";
            public const int USERPROFILE_CREATING_ERROR_CODE = 4003;

            public const string USERPROFILE_EDITING_ERROR = "Возникла проблема при редактировании профиля";
            public const int USERPROFILE_EDITING_ERROR_CODE = 4004;

            public const string USERPROFILE_DELETING_ERROR = "Возникла проблема при удалении профиля";
            public const int USERPROFILE_DELETING_ERROR_CODE = 4005;
        }

        public static class Subscription
        {
            public const string SUBSCRIPTION_NOT_FOUND = "Подписка не найдена";
            public const int SUBSCRIPTION_NOT_FOUND_CODE = 5001;

            public const string SUBSCRIPTION_CREATING_ERROR = "Возникла проблема при создании подписки";
            public const int SUBSCRIPTION_CREATING_ERROR_CODE = 5002;

            public const string SUBSCRIPTION_EDITING_ERROR = "Возникла проблема при редактировании подписки";
            public const int SUBSCRIPTION_EDITING_ERROR_CODE = 5003;

            public const string SUBSCRIPTION_DELETING_ERROR = "Возникла проблема при удалении подписки";
            public const int SUBSCRIPTION_DELETING_ERROR_CODE = 5004;

            public const string SUBSCRIPTIONBILL_NOT_FOUND = "Счет на подписку не найден";
            public const int SUBSCRIPTIONBILL_NOT_FOUND_CODE = 5011;

            public const string SUBSCRIPTIONBILL_CREATING_ERROR = "Возникла проблема при создании счета на подписку";
            public const int SUBSCRIPTIONBILL_CREATING_ERROR_CODE = 5012;

            public const string SUBSCRIPTIONBILL_EDITING_ERROR = "Возникла проблема при редактировании счета на подписку";
            public const int SUBSCRIPTIONBILL_EDITING_ERROR_CODE = 5013;

            public const string SUBSCRIPTIONBILL_DELETING_ERROR = "Возникла проблема при удалении счета на подписку";
            public const int SUBSCRIPTIONBILL_DELETING_ERROR_CODE = 5014;

            public const string SUBSCRIPTIONBILLENTRY_NOT_FOUND = "Данное содержимое счета не найдено";
            public const int SUBSCRIPTIONBILLENTRY_NOT_FOUND_CODE = 5021;

            public const string SUBSCRIPTIONBILLENTRY_CREATING_ERROR = "Возникла проблема при создании содержимого счета на подписку";
            public const int SUBSCRIPTIONBILLENTRY_CREATING_ERROR_CODE = 5022;

            public const string SUBSCRIPTIONBILLENTRY_EDITING_ERROR = "Возникла проблема при редактировании содержимого счета на подписку";
            public const int SUBSCRIPTIONBILLENTRY_EDITING_ERROR_CODE = 5023;

            public const string SUBSCRIPTIONBILLENTRY_DELETING_ERROR = "Возникла проблема при удалении содержимого счета на подписку";
            public const int SUBSCRIPTIONBILLENTRY_DELETING_ERROR_CODE = 5024;

            public const string SUBSCRIPTIONPAYMENT_NOT_FOUND = "Оплата подписки не найдена";
            public const int SUBSCRIPTIONPAYMENT_NOT_FOUND_CODE = 5031;

            public const string SUBSCRIPTIONPAYMENT_CREATING_ERROR = "Возникла проблема при создании оплаты на подписку";
            public const int SUBSCRIPTIONPAYMENT_CREATING_ERROR_CODE = 5032;

            public const string SUBSCRIPTIONPAYMENT_EDITING_ERROR = "Возникла проблема при редактировании оплаты на подписку";
            public const int SUBSCRIPTIONPAYMENT_EDITING_ERROR_CODE = 5033;
        }
    }
}