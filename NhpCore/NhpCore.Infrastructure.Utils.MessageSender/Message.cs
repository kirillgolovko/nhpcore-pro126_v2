﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Utils.MessageSender
{
    public class Message : IMessage
    {
        // адрес получателя сообщения
        public string Destination { get; set; }
        
        // тема сообщения
        public string Subject { get; set; }
        
        // тело сообщения
        public string Body { get; set; }

        public Message()
        {

        }

        public Message(string destination, string subject, string body)
        {
            Destination = destination;
            Subject = subject;
            Body = body;
        }
    }
}
