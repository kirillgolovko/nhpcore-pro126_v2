﻿using Autofac;
using Common.Logging;
using Common.Mapping;
using Common.Validation;
using NhpCore.Infrastructure.Utils.EntityAnnotationValidation;
using NhpCore.Infrastructure.Utils.Logging;
using NhpCore.Infrastructure.Utils.Mapping;
using NhpCore.Infrastructure.Utils.MessageSender;
using NhpCore.Infrastructure.Utils.ServiceSynchronization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.DependencyResolution
{
    public class InfrastructureUtilsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<EmptyLoggerUtility>().As<ILoggerUtility>();
            builder.RegisterType<NLogLogger>().As<ILoggerUtility>();
            builder.RegisterType<AutoMapperUtilityImpl>().As<IMapperUtility>();
            builder.RegisterType<EntityAnnotationValidator>().As<IAnnotationValidator>();
            builder.RegisterType<EmailMessageSender>().As<IMessageSender>();
            builder.RegisterType<ServiceSynchronization>().As<IServiceSynchronization>();
        }
    }
}