﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using NhpCore.CoreLayer.Entities;
using Autofac;
using NhpCore.Infrastructure.Data.Ef;
using Microsoft.AspNet.Identity;
using NhpCore.Infrastructure.AspNetIdentityServices;

namespace NhpCore.Infrastructure.DependencyResolution.AutofacModules
{
    public class IdentityManagerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CustomUserStore>().AsSelf().As<IUserStore<CustomUser, int>>();

            builder.RegisterType<CustomUserManager>();
            builder.RegisterType<ApplicationSignInManager>();
        }
    }
}