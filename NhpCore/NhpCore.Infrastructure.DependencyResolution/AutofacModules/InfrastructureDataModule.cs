﻿using Autofac;
using Common.DataAccess.UnitOfWork;
using Common.EntityFramework;
using NhpCore.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.DependencyResolution
{
    public class InfrastructureDataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NhpIdentityDbContext>().AsSelf().InstancePerRequest();

            builder.RegisterType<NhpCoreDbContext>().AsSelf().As<DbContext>().InstancePerRequest();

            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();

            builder.RegisterGeneric(typeof(EfUnitOfWorkGeneric<>)).As(typeof(IUnitOfWork<>)).InstancePerRequest();
        }
    }
}