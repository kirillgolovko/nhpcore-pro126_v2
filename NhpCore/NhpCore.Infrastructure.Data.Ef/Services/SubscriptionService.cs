﻿using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using System.Data.Entity;

namespace NhpCore.Infrastructure.Data.Ef.Services
{
    public class SubscriptionService : BaseService, ISubscriptionService
    {
        public SubscriptionService(IUnitOfWork<NhpCoreDbContext> coreUoW) : base(coreUoW)
        {

        }

        public async Task<IEnumerable<ITProduct>> GetAvailable()
        {
            return await UnitOfWork.GetRepository<ITProduct>()
                .Query()
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IEnumerable<Subscription>> GetSubscriptions(int[] subscriptionIds)
        {
            return await UnitOfWork.GetRepository<Subscription>()
                .Query()
                .Where(x => subscriptionIds.Contains(x.Id))
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IEnumerable<UserSubscriptionEntry>> GetUserEntries(int subscriptionId, IEnumerable<int> userIds)
        {
            return await UnitOfWork.GetRepository<UserSubscriptionEntry>()
                .Query()
                .AsNoTracking()
                .Where(x => x.SubscriptionId == subscriptionId && userIds.Contains(x.UserId))
                .ToListAsync();
        }

        public async Task<SubscriptionPayment> GetSubscriptionPayment(Guid Id)
        {
            return await UnitOfWork.GetRepository<SubscriptionPayment>()
                .Query()
                .Where(x => x.Id == Id)
                .FirstOrDefaultAsync();
        }

        public async Task<SubscriptionPayment> GetSubscriptionPayment(string code)
        {
            try
            {
                Guid guid = Guid.Parse(code);
                return await UnitOfWork.GetRepository<SubscriptionPayment>()
                    .Query()
                    .Where(x => x.LicenseCode == guid)
                    .FirstOrDefaultAsync();

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<IEnumerable<SubscriptionPayment>> GetSubscriptionPayments(int orgId)
        {
            return await UnitOfWork.GetRepository<SubscriptionPayment>()
                .Query()
                .AsNoTracking()
                .Where(x => x.IsPayed == true && (x.BuyeerId == orgId || x.EndCustomerOrganisationId == orgId))
                .ToListAsync();
        }

        public async Task<IEnumerable<SubscriptionPaymentBill>> GetSubscriptionPaymentBillList(int orgId)
        {
            if (orgId == 0)
            {
                return await UnitOfWork.GetRepository<SubscriptionPaymentBill>()
                .Query()
                .AsNoTracking()
                .ToListAsync();
            }
            else
            {
                return await UnitOfWork.GetRepository<SubscriptionPaymentBill>()
                    .Query()
                    .AsNoTracking()
                    .Where(x => x.BuyeerId == orgId)
                    .ToListAsync();
            }
        }

        public async Task<SubscriptionPaymentBill> GetSubscriptionPaymentBill(int BillId)
        {
            return await UnitOfWork.GetRepository<SubscriptionPaymentBill>()
                .Query()
                .Where(x => x.Id == BillId)
                .FirstOrDefaultAsync();
        }

    }
}