﻿using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using System.Data.Entity;

namespace NhpCore.Infrastructure.Data.Ef.Services
{
    public class DepartmentService : BaseService, IDepartmentService
    {
        public DepartmentService(IUnitOfWork<NhpCoreDbContext> coreUoW) : base(coreUoW)
        {

        }
        
        public async Task<IEnumerable<Department>> GetList()
        {
            return await UnitOfWork.GetRepository<Department>()
                .Query()
                .ToListAsync();
        }

        public async Task<IEnumerable<Department>> GetAvailable(int organisationId)
        {
            return await UnitOfWork.GetRepository<Department>()
                .Query()
                .Where(x => x.OrganisationId == organisationId)
                .ToListAsync();
        }

        public async Task<Department> Get(int Id)
        {
            return await UnitOfWork.GetRepository<Department>()
                .Query()
                .Where(x => x.Id == Id)
                .FirstOrDefaultAsync();
        }
    }
}