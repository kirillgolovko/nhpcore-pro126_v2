﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NhpCore.Infrastructure.Data.Ef.Seed;

namespace NhpCore.Infrastructure.Data.Ef.Initializer
{
    public class NhpCoreDbContextDropCreateAlwaysInitializer : DropCreateDatabaseAlways<NhpCoreDbContext>
    {
        protected override void Seed(NhpCoreDbContext context)
        {
            base.Seed(context);

            DbSeeder.Seed(context);
        }
    }
}