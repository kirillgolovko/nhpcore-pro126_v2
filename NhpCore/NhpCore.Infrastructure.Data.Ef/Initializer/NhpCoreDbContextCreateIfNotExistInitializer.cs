﻿using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef.Seed;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef
{
    internal class NhpCoreDbContextCreateIfNotExistInitializer : CreateDatabaseIfNotExists<NhpCoreDbContext>
    {
        protected override void Seed(NhpCoreDbContext db)
        {
            base.Seed(db);

          //  DbSeeder.Seed(db);
        }
    }
}