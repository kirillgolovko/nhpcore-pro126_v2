namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubscriptionBill : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ITProductClients", "ITProduct_Id", "dbo.ITProducts");
            DropForeignKey("dbo.SubscriptionPayments", "BuyeerId", "dbo.Organisations");
            DropIndex("dbo.ITProductClients", new[] { "ITProduct_Id" });
            CreateTable(
                "dbo.SubscriptionPaymentBills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        BuyeerId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Total = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.BuyeerId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.BuyeerId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.SubscriptionPayments", "DateOfSuccesfullyActivation", c => c.DateTime());
            AddColumn("dbo.SubscriptionPayments", "BuyeerUserId", c => c.Int(nullable: false));
            AddColumn("dbo.SubscriptionPayments", "LicenseCode", c => c.Guid(nullable: false));
            AddColumn("dbo.SubscriptionPayments", "Quantity", c => c.Int(nullable: false));
            AddColumn("dbo.SubscriptionPayments", "IsActivatedLicense", c => c.Boolean(nullable: false));
            AddColumn("dbo.SubscriptionPayments", "EndCustomerOrganisationId", c => c.Int(nullable: false));
            AddColumn("dbo.SubscriptionPayments", "EndCustomerUserId", c => c.Int(nullable: false));
            AddColumn("dbo.ITProducts", "FullName", c => c.String());
            AddColumn("dbo.SubscriptionPaymentBillEntries", "SubscriptionId", c => c.Int(nullable: false));
            AddColumn("dbo.SubscriptionPaymentBillEntries", "Name", c => c.String());
            AlterColumn("dbo.BusinessContractDetails", "IssuerKPP", c => c.String());
            AlterColumn("dbo.BusinessContractDetails", "IssuerBankOGRN", c => c.String());
            AlterColumn("dbo.BusinessContractDetails", "IssuerBankOKPO", c => c.String());
            CreateIndex("dbo.SubscriptionPayments", "BuyeerUserId");
            CreateIndex("dbo.SubscriptionPayments", "EndCustomerOrganisationId");
            CreateIndex("dbo.SubscriptionPayments", "EndCustomerUserId");
            CreateIndex("dbo.SubscriptionPaymentBillEntries", "SubscriptionId");
            AddForeignKey("dbo.SubscriptionPayments", "EndCustomerOrganisationId", "dbo.Organisations", "Id");
            AddForeignKey("dbo.SubscriptionPayments", "EndCustomerUserId", "dbo.Users", "Id");
            AddForeignKey("dbo.SubscriptionPayments", "BuyeerUserId", "dbo.Users", "Id");
            AddForeignKey("dbo.SubscriptionPaymentBillEntries", "SubscriptionId", "dbo.Subscriptions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SubscriptionPayments", "BuyeerId", "dbo.Organisations", "Id");
            DropColumn("dbo.Subscriptions", "Name");
            DropColumn("dbo.Subscriptions", "MaxAvailableUsers");
            DropColumn("dbo.ITProductClients", "ITProduct_Id");

            Sql("DELETE FROM dbo.SubscriptionPaymentBillEntries");
            Sql("UPDATE dbo.ITProducts SET FullName=Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ITProductClients", "ITProduct_Id", c => c.Int());
            AddColumn("dbo.Subscriptions", "MaxAvailableUsers", c => c.Int(nullable: false));
            AddColumn("dbo.Subscriptions", "Name", c => c.String());
            DropForeignKey("dbo.SubscriptionPayments", "BuyeerId", "dbo.Organisations");
            DropForeignKey("dbo.SubscriptionPaymentBillEntries", "SubscriptionId", "dbo.Subscriptions");
            DropForeignKey("dbo.SubscriptionPaymentBills", "UserId", "dbo.Users");
            DropForeignKey("dbo.SubscriptionPaymentBills", "BuyeerId", "dbo.Organisations");
            DropForeignKey("dbo.SubscriptionPayments", "BuyeerUserId", "dbo.Users");
            DropForeignKey("dbo.SubscriptionPayments", "EndCustomerUserId", "dbo.Users");
            DropForeignKey("dbo.SubscriptionPayments", "EndCustomerOrganisationId", "dbo.Organisations");
            DropIndex("dbo.SubscriptionPaymentBills", new[] { "UserId" });
            DropIndex("dbo.SubscriptionPaymentBills", new[] { "BuyeerId" });
            DropIndex("dbo.SubscriptionPaymentBillEntries", new[] { "SubscriptionId" });
            DropIndex("dbo.SubscriptionPayments", new[] { "EndCustomerUserId" });
            DropIndex("dbo.SubscriptionPayments", new[] { "EndCustomerOrganisationId" });
            DropIndex("dbo.SubscriptionPayments", new[] { "BuyeerUserId" });
            AlterColumn("dbo.BusinessContractDetails", "IssuerBankOKPO", c => c.String(nullable: false));
            AlterColumn("dbo.BusinessContractDetails", "IssuerBankOGRN", c => c.String(nullable: false));
            AlterColumn("dbo.BusinessContractDetails", "IssuerKPP", c => c.String(nullable: false));
            DropColumn("dbo.SubscriptionPaymentBillEntries", "Name");
            DropColumn("dbo.SubscriptionPaymentBillEntries", "SubscriptionId");
            DropColumn("dbo.ITProducts", "FullName");
            DropColumn("dbo.SubscriptionPayments", "EndCustomerUserId");
            DropColumn("dbo.SubscriptionPayments", "EndCustomerOrganisationId");
            DropColumn("dbo.SubscriptionPayments", "IsActivatedLicense");
            DropColumn("dbo.SubscriptionPayments", "Quantity");
            DropColumn("dbo.SubscriptionPayments", "LicenseCode");
            DropColumn("dbo.SubscriptionPayments", "BuyeerUserId");
            DropColumn("dbo.SubscriptionPayments", "DateOfSuccesfullyActivation");
            DropTable("dbo.SubscriptionPaymentBills");
            CreateIndex("dbo.ITProductClients", "ITProduct_Id");
            AddForeignKey("dbo.SubscriptionPayments", "BuyeerId", "dbo.Organisations", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ITProductClients", "ITProduct_Id", "dbo.ITProducts", "Id");
        }
    }
}
