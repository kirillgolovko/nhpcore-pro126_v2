namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserProfileAvatar_StorageAccess_OrgUserEntrySalary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "AvatarPath", c => c.String());
            AddColumn("dbo.OrganisationUserEntries", "Salary", c => c.Double());
            AddColumn("dbo.OrganisationDocumentsStorageAccesses", "ServerUri", c => c.String());
            AddColumn("dbo.OrganisationDocumentsStorageAccesses", "CreationDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrganisationDocumentsStorageAccesses", "CreationDateTime");
            DropColumn("dbo.OrganisationDocumentsStorageAccesses", "ServerUri");
            DropColumn("dbo.OrganisationUserEntries", "Salary");
            DropColumn("dbo.Users", "AvatarPath");
        }
    }
}
