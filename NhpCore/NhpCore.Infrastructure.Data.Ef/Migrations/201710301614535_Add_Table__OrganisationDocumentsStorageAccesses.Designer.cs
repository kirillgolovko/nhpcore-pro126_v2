// <auto-generated />
namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_Table__OrganisationDocumentsStorageAccesses : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_Table__OrganisationDocumentsStorageAccesses));
        
        string IMigrationMetadata.Id
        {
            get { return "201710301614535_Add_Table__OrganisationDocumentsStorageAccesses"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
