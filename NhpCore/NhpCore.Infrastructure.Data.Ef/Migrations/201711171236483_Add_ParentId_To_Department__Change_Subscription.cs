namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ParentId_To_Department__Change_Subscription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "ParentId", c => c.Int());
            AddColumn("dbo.Subscriptions", "TimePeriodType", c => c.Int(nullable: false));
            AddColumn("dbo.Subscriptions", "Duration", c => c.Int(nullable: false));
            AddColumn("dbo.Subscriptions", "MaxAvailableUsers", c => c.Int(nullable: false));
            CreateIndex("dbo.Departments", "ParentId");
            AddForeignKey("dbo.Departments", "ParentId", "dbo.Departments", "Id");
            DropColumn("dbo.SubscriptionPayments", "Duration");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubscriptionPayments", "Duration", c => c.Int(nullable: false));
            DropForeignKey("dbo.Departments", "ParentId", "dbo.Departments");
            DropIndex("dbo.Departments", new[] { "ParentId" });
            DropColumn("dbo.Subscriptions", "MaxAvailableUsers");
            DropColumn("dbo.Subscriptions", "Duration");
            DropColumn("dbo.Subscriptions", "TimePeriodType");
            DropColumn("dbo.Departments", "ParentId");
        }
    }
}
