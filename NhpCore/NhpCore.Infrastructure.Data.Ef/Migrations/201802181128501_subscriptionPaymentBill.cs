namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subscriptionPaymentBill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubscriptionPayments", "BillId", c => c.Int(nullable: false));
            AddColumn("dbo.SubscriptionPaymentBills", "IsPayed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubscriptionPaymentBills", "IsPayed");
            DropColumn("dbo.SubscriptionPayments", "BillId");
        }
    }
}
