namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class User_Department : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsAdmin", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.Users", "IsManager", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.Departments", "Level", c => c.Int(nullable: false, defaultValue: 0));
        }

        public override void Down()
        {
            DropColumn("dbo.Departments", "Level");
            DropColumn("dbo.Users", "IsManager");
            DropColumn("dbo.Users", "IsAdmin");
        }
    }
}
