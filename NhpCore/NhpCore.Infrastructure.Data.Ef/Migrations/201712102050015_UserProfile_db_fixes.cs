namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserProfile_db_fixes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrganisationDetailCards", "Logo", c => c.String());
            AddColumn("dbo.UserProfiles", "IsEmailExpired", c => c.Boolean());
            AddColumn("dbo.UserProfiles", "IsEmailTask", c => c.Boolean());
            AlterColumn("dbo.OrganisationDetailCards", "KPP", c => c.String());
            AlterColumn("dbo.OrganisationDetailCards", "BankOGRN", c => c.String());
            AlterColumn("dbo.OrganisationDetailCards", "BankOKPO", c => c.String());
            AlterColumn("dbo.UserProfiles", "IsTelegram", c => c.Boolean());
            DropColumn("dbo.UserProfiles", "IsEmail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "IsEmail", c => c.String());
            AlterColumn("dbo.UserProfiles", "IsTelegram", c => c.String());
            AlterColumn("dbo.OrganisationDetailCards", "BankOKPO", c => c.String(nullable: false));
            AlterColumn("dbo.OrganisationDetailCards", "BankOGRN", c => c.String(nullable: false));
            AlterColumn("dbo.OrganisationDetailCards", "KPP", c => c.String(nullable: false));
            DropColumn("dbo.UserProfiles", "IsEmailTask");
            DropColumn("dbo.UserProfiles", "IsEmailExpired");
            DropColumn("dbo.OrganisationDetailCards", "Logo");
        }
    }
}
