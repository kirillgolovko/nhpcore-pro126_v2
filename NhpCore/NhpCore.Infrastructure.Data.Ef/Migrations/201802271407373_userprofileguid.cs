namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userprofileguid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "IdProfile", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfiles", "IdProfile");
        }
    }
}
