// <auto-generated />
namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class userprofileguid : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(userprofileguid));
        
        string IMigrationMetadata.Id
        {
            get { return "201802271407373_userprofileguid"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
