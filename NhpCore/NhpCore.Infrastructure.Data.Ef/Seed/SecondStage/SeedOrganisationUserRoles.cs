﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedOrganisationUserRoles
    {
        public static void SeedUserRoles(NhpCoreDbContext db)
        {
            var result = new List<OrganisationUserRole>();

            Random rnd = new Random();
            var userEntries = db.OrganisationUserEntries.ToList();
            var max = db.OrganisationUserRoleTypes.ToList().Count;
            userEntries.ForEach(x =>
            {
                if (x.UserId == 10 || x.UserId == 2)
                {
                    var superAdminRole = new OrganisationUserRole { OrganisationUserEntryId = x.Id, OrganisationUserRoleTypeId = 1 };
                    result.Add(superAdminRole);

                    if (x.UserId == 2)
                    {
                        var adminRole = new OrganisationUserRole { OrganisationUserEntryId = x.Id, OrganisationUserRoleTypeId = 2 };
                        result.Add(adminRole);
                    }
                }
                else
                {
                    var role = new OrganisationUserRole
                    {
                        OrganisationUserEntryId = x.Id,
                        OrganisationUserRoleTypeId = 3,
                    };
                    result.Add(role);
                }
            });

            db.OrganisationUserRoles.AddRange(result);
        }
    }
}