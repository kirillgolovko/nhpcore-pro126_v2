﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedOrganisationBalanceEntries
    {
        public static void SeedEntries(NhpCoreDbContext db)
        {
            var balances = db.OrganisationBalances.ToList();

            for (int i = 0; i < balances.Count; i++)
            {
                db.OrganisationBalanceEntries.AddRange(
                    new List<BalanceEntry>
                    {
                        new BalanceEntry{
                            Balance = balances[i],
                            Date= DateTime.Now.Date,
                            Time = DateTime.Now,
                            Title = $"balance title {i}",
                            Description = $"description of balance entry {i}",
                            Value=(i+0.756)*50,
                        },
                        new BalanceEntry{
                            Balance = balances[i],
                            Date= DateTime.Now.Date,
                            Time = DateTime.Now,
                            Title = $"balance title {i+1}",
                            Description = $"description of balance entry {i+1}",
                            Value=(i+0.756)*50,
                        },
                        new BalanceEntry{
                            Balance = balances[i],
                            Date= DateTime.Now.Date,
                            Time = DateTime.Now,
                            Title = $"balance title {i+2}",
                            Description = $"description of balance entry {i+2}",
                            Value=(i+0.756)*50,
                        },
                    });
            }
        }
    }
}