﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedOperationStatuses
    {
        public static ICollection<OperationStatus> GetOperationStatuses()
        {
            return new List<OperationStatus>
            {
                new OperationStatus{ Name="Операция начата"},
                new OperationStatus{ Name="Идет обработка платежа"},
                new OperationStatus{ Name="Успешно обработана"},
                new OperationStatus{ Name="Отменена"}
            };
        }
    }
}
