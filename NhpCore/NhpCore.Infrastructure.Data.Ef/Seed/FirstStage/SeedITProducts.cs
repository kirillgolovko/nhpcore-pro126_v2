﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedITProducts
    {
        public static ICollection<ITProduct> GetServices()
        {
            return new List<ITProduct>
            {
                new ITProduct{ Name="НХП Проект"},
                new ITProduct{ Name="НХП Теплообменник"},
                new ITProduct{ Name="НХП Пожаротушение"},
                new ITProduct{ Name="НХП Изоляция"},
            };
        }
    }
}