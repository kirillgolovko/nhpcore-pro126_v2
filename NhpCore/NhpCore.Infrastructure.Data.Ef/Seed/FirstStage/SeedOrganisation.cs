﻿using NhpCore.CoreLayer.Entities;
//using NhpProject.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedOrganisation
    {
        public static void GetOrganisations(NhpCoreDbContext db)
        {
            Random rnd = new Random();
           // var prodb = new NhpProjectDbContext();
           // var orgs = prodb.Organisations.ToList();

            //var maxOrgId = orgs.Max(x => x.Id);

            //for (int i = 1; i <= maxOrgId; i++)
            //{
            //    var org = orgs.FirstOrDefault(x => x.Id == i);
            //    var newOrg = new Organisation
            //    {
            //        Balance = new Balance { Value = 0 },
            //        RegisterDate =DateTime.Now,
            //        DeleteTime=null,
            //        Details = new OrganisationDetailCard
            //        {
            //            INN = "empty",
            //            KPP = "empty",
            //            LegalAddress = "empty",
            //            PhoneNumber = "empty",
            //            PhoneNumberAdditional = "empty",
            //            Email = "empty",
            //            DirectorString = "empty",
            //            ChiefAccountantString = "empty",
            //            BankName = "empty",
            //            BankBIK = "empty",
            //            BankCorporateAccount = "empty",
            //            BankCorrespondentAccount = "empty",
            //            BankOGRN = "empty",
            //            BankOKPO = "empty"
            //        }
            //    };

            //    if (org!=null)
            //    {
            //        newOrg.Name = org.Name;
            //        newOrg.RegisterDate = org.RegisterDate;
            //        newOrg.Details.ShortName = org.Name;
            //    }
            //    else
            //    {
            //        newOrg.Name = "test@test";
            //        newOrg.Details.ShortName = "test@test";
            //    }

            //    db.Organisations.Add(newOrg);
            //    try
            //    {
            //        db.SaveChanges();
            //    }
            //    catch (Exception ex)
            //    {

            //        throw;
            //    }
                
            //}
            //var testOrgs =  db.Organisations.Where(x => x.Name == "test@test");
            //db.Organisations.RemoveRange(testOrgs);

            //db.SaveChanges();
            //prodb.Dispose();















            //ICollection<Organisation> result = new List<Organisation>
            //{
            //    new Organisation{ Name="НИПИ",
            //        Balance = new Balance {
            //            Value = 50000,
            //        },
            //        Details = new OrganisationDetailCard
            //        {
            //            ShortName=@"ООО ""НИПИ""",
            //            INN="2635210195",
            //            KPP = "263501001",
            //            LegalAddress=@"355000, Россия, Ставропольский край, г. Ставрополь, ул. Доваторцев 38 ""А"", офис 302",
            //            PhoneNumber = @"(8652)31-41-31",
            //            PhoneNumberAdditional = "105",
            //            Email ="info@nhp-soft.ru",
            //            DirectorString ="Кирдяшев Владимир Александрович",
            //            ChiefAccountantString = "Кирдяшев Владимир Александрович",
            //            BankName=@"ФИЛИАЛ НАЛЬЧИКСКИЙ ПАО БАНКА ""ФК ОТКРЫТИЕ"" г. Нальчик",
            //            BankBIK="04832",
            //            BankCorporateAccount="407242342234234",
            //            BankCorrespondentAccount="234424234234",
            //            BankOGRN="324234234",
            //            BankOKPO="2342342342"
            //        }
            //    },
            //    new Organisation{ Name="НХП",
            //        Balance = new Balance {
            //            Value = 20000,
            //        },
            //        Details = new OrganisationDetailCard
            //        {
            //            ShortName=@"ООО ""НХП""",
            //            INN="2635210195",
            //            KPP = "263501001",
            //            LegalAddress=@"355000, Россия, Ставропольский край, г. Ставрополь, ул. Доваторцев 38 ""А"", офис 302",
            //            PhoneNumber = @"(8652)31-41-31",
            //            PhoneNumberAdditional = "105",
            //            Email ="info@nhp-soft.ru",
            //            DirectorString ="Кирдяшев Владимир Александрович",
            //            ChiefAccountantString = "Кирдяшев Владимир Александрович",
            //            BankName=@"ФИЛИАЛ НАЛЬЧИКСКИЙ ПАО БАНКА ""ФК ОТКРЫТИЕ"" г. Нальчик",
            //            BankBIK="04832",
            //            BankCorporateAccount="407242342234234",
            //            BankCorrespondentAccount="234424234234",
            //            BankOGRN="324234234",
            //            BankOKPO="2342342342"
            //        }
            //    },
            //    new Organisation{
            //        Name ="Careprost",
            //        Balance = new Balance {
            //            Value = 10000,
            //        },
            //        Details = new OrganisationDetailCard
            //        {
            //            ShortName=@"Careprost",
            //            INN="2635210195",
            //            KPP = "263501001",
            //            LegalAddress=@"355000, Россия, Ставропольский край, г. Ставрополь, ул. Доваторцев 38 ""А"", офис 302",
            //            PhoneNumber = @"(8652)31-41-31",
            //            PhoneNumberAdditional = "105",
            //            Email ="info@nhp-soft.ru",
            //            DirectorString ="Кирдяшев Владимир Александрович",
            //            ChiefAccountantString = "Кирдяшев Владимир Александрович",
            //            BankName=@"ФИЛИАЛ НАЛЬЧИКСКИЙ ПАО БАНКА ""ФК ОТКРЫТИЕ"" г. Нальчик",
            //            BankBIK="04832",
            //            BankCorporateAccount="407242342234234",
            //            BankCorrespondentAccount="234424234234",
            //            BankOGRN="324234234",
            //            BankOKPO="2342342342"
            //        }
            //    },
            //};

            // return result;
        }

        internal static void SeedPersonalOrganisations(NhpCoreDbContext db)
        {
            //var users = db.Users.ToList();
            //var superAdminRole = db.OrganisationUserRoleTypes.FirstOrDefault(x => x.Title == "SuperAdmin");

            //var prodb = new NhpProjectDbContext();

            //users.ForEach(x =>
            //{
            //    var dtNow = DateTime.Now;

            //    var org = new Organisation
            //    {
            //        Name = $"Личная-{x.Id}",
            //        Balance = new Balance { Value = 0 },
            //        RegisterDate = dtNow
            //    };

            //    db.Organisations.Add(org);
            //    db.SaveChanges();

            //    var projOrg = new NhpProject.Core.Entities.Organisation
            //    {
            //        Id = org.Id,
            //        CoreId = org.Id,
            //        Name = org.Name,
            //        IsDeleted = false,
            //        RegisterDate = dtNow,
            //    };

            //    prodb.Organisations.Add(projOrg);
            //    prodb.SaveChanges();
            //    var user = prodb.Users.FirstOrDefault(u => u.Id == x.Id);
            //    if (user != null)
            //    {
            //        projOrg.ManyUsers.Add(user);
            //        db.SaveChanges();
            //    }

            //    var entry = new OrganisationUserEntry
            //    {
            //        Organisation = org,
            //        User = x,
            //        IsEnabled = true,
            //        IsPersonal = true,
            //        UserProfile = new OrganisationUserProfile { DisplayName = "Вы" },
            //        ProfileId = x.Id

            //    };
            //    var entryRole = new OrganisationUserRole
            //    {
            //        OrganisationUserEntry = entry,
            //        OrganisationUserRoleType = superAdminRole
            //    };
            //    entry.UserRoles.Add(entryRole);
            //    db.OrganisationUserEntries.Add(entry);
            //    db.OrganisationUserRoles.Add(entryRole);
            //    db.SaveChanges();
            //});

            //prodb.Dispose();
        }
    }
}