﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedOperationType
    {
        public static IEnumerable<OperationType> SeedOperationTypes()
        {
            return new List<OperationType>{
                new OperationType{ Name="Оплата подписки"},
                new OperationType{  Name="Возврат подписки"}
            };
        }
    }
}
