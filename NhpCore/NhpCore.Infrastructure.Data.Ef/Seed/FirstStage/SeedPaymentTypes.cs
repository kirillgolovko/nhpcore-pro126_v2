﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
   public  class SeedPaymentTypes
    {
        public static ICollection<PaymentType> GetPaymentTypes()
        {
            return new List<PaymentType>()
            {
                new PaymentType{ Name = "Yandex"},
                new PaymentType{ Name="Безналичный платеж"}
            };
        }
    }
}
