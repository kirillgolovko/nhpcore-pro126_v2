﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedUser
    {
        public static ICollection<User> GetUsers()
        {
            ICollection<User> result = new List<User>();

            string path = AppDomain.CurrentDomain.BaseDirectory + $@"EnvironmentConfiguration\ConnectionStrings-Debug.config";

            var isExist = File.Exists(path);

            if (isExist)
            {
                StreamReader sr = new StreamReader(path);
                string connStr = string.Empty;

                while (connStr == string.Empty)
                {
                    var line = sr.ReadLine();
                    var splitedBySpace = line.Split(' ');
                    if (splitedBySpace.Length > 0 && splitedBySpace[0] != "<connectionStrings>")
                    {
                        if (splitedBySpace[3] == @"name=""NhpIdentityDbContext""")
                        {

                            connStr = line.Split('"')[3];
                        }
                    }
                }
                var users = 0;
                using (var db = new NhpIdentityDbContext(connStr))
                {
                    users = db.Users.Count();
                    if (users > 0)
                    {
                        var identityUsers = db.Users.ToList();
                        //identityUsers.ForEach(x => result.Add(new User { IdentityId = x.Id }));

                        // updated version below
                        var max = identityUsers.Max(x => x.Id);
                        for (int i = 1; i <= max; i++)
                        {
                            var user = identityUsers.FirstOrDefault(u => u.Id == i);

                            result.Add(new User
                            {
                                Id = i,
                                IsDeleted = false,
                                IsEnabled = true
                            });
                        }
                    }
                    else
                    {
                        GetSimpleUsers(ref result);
                    }
                }
            }
            else
            {
                GetSimpleUsers(ref result);
            }


            return result;
        }

        internal static void GenerateUserProfile(NhpCoreDbContext db)
        {
            var users = db.Users.ToList();
            var dtnow = DateTime.Now;
            var releaseConnStr = GetConnectionString("NhpIdentityDbContext", "Debug");
            var iddb = new NhpIdentityDbContext(releaseConnStr);
            var identityUsers = iddb.Users.ToList();


            users.ForEach(u =>
            {
                var idUser = identityUsers.FirstOrDefault(x => x.Id == u.Id);
                var profile = new UserProfile
                {
                    UserId = u.Id,
                    BirthDate = dtnow.Date,
                    BirthYear = dtnow
                };
                if (idUser != null)
                {
                    var displayName = idUser.Claims.FirstOrDefault(c => c.ClaimType == "displayname");
                    if (displayName != null)
                    {
                        profile.DisplayName = displayName.ClaimValue;
                    }
                }
                db.UserProfiles.Add(profile);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="mode"></param>
        /// <returns>строка подключения, в случае ошибки - пустая строка (string.Empty)</returns>
        private static string GetConnectionString(string name, string mode)
        {
            string result = string.Empty;

            string path = AppDomain.CurrentDomain.BaseDirectory + $@"EnvironmentConfiguration\ConnectionStrings-{mode}.config";

            var isExist = File.Exists(path);
            if (isExist)
            {
                StreamReader sr = new StreamReader(path);

                while (result == string.Empty)
                {
                    var line = sr.ReadLine();
                    var splitedBySpace = line.Split(' ');
                    if (splitedBySpace.Length > 0 && splitedBySpace[0] != "<connectionStrings>")
                    {
                        if (splitedBySpace[3] == $@"name=""{name}""")
                        {

                            result = line.Split('"')[3];
                        }
                    }
                }
            }

            return result;
        }

        internal static void ExcludeUsers(NhpCoreDbContext db)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + $@"EnvironmentConfiguration\ConnectionStrings-Debug.config";

            var isExist = File.Exists(path);

            if (isExist)
            {
                StreamReader sr = new StreamReader(path);
                string connStr = string.Empty;

                while (connStr == string.Empty)
                {
                    var line = sr.ReadLine();
                    var splitedBySpace = line.Split(' ');
                    if (splitedBySpace.Length > 0 && splitedBySpace[0] != "<connectionStrings>")
                    {
                        if (splitedBySpace[3] == @"name=""NhpIdentityDbContext""")
                        {
                            connStr = line.Split('"')[3];
                        }
                    }
                }
                using (var idb = new NhpIdentityDbContext(connStr))
                {
                    var users = db.Users.ToList().Select(x => x.Id).ToList();

                    var finded = idb.Users.Where(x => users.Contains(x.Id)).Select(x => x.Id).ToList();

                    var findedCoreUsers = db.Users.Where(x => !finded.Contains(x.Id)).ToList();
                    var rolesToRemove = db.UserProfiles.Where(x => !finded.Contains(x.Id)).ToList();
                    db.UserProfiles.RemoveRange(rolesToRemove);
                    findedCoreUsers.ForEach(x => db.Users.Remove(x));
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {

                        throw;
                    }

                }
            }
            else
            {
                throw new FileNotFoundException();
            }
        }

        private static void GetSimpleUsers(ref ICollection<User> result)
        {
            //result.Add(new User { IdentityId = 1 });
            //result.Add(new User { IdentityId = 2 });
            //result.Add(new User { IdentityId = 3 });
            result.Add(new User { Id = 1 });
            result.Add(new User { Id = 2 });
            result.Add(new User { Id = 3 });
        }
    }
}