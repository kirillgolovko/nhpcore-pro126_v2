﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedSubscriptionOptionType
    {
        public static ICollection<SubscriptionOptionType> GetOptionTypes()
        {
            IList<SubscriptionOptionType> result = new List<SubscriptionOptionType>
            {
                new SubscriptionOptionType { Title = "Module", Description = "Program module" },
                new SubscriptionOptionType { Title = "Multiplier", Description = "subscription price multiplier for discrount, etc..." }
            };

            return result;
        }
    }
}