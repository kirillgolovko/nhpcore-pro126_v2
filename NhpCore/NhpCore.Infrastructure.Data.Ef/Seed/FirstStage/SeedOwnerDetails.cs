﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedOwnerDetails
    {
        public static ICollection<OwnerDetails> GetOwnerDetails()
        {
            return new List<OwnerDetails>
            {
                new OwnerDetails{
                    ShortName =@"ООО ""НХП""",
                    INN="2635210195",
                    KPP = "263501001",
                    LegalAddress=@"355000, Россия, Ставропольский край, г. Ставрополь, ул. Доваторцев 38 ""А"", офис 302",
                    PhoneNumber = @"(8652)31-41-31",
                    PhoneNumberAdditional = "105",
                    Email ="info@nhp-soft.ru",
                    DirectorString ="Кирдяшев Владимир Александрович",
                    ChiefAccountantString = "Кирдяшев Владимир Александрович",
                    BankName=@"ФИЛИАЛ НАЛЬЧИКСКИЙ ПАО БАНКА ""ФК ОТКРЫТИЕ"" г. Нальчик",
                    BankBIK="04832",
                    BankCorporateAccount="407242342234234",
                    BankCorrespondentAccount="234424234234",
                    BankOGRN="324234234",
                    BankOKPO="2342342342"
                }
            };
        }
    }
}
