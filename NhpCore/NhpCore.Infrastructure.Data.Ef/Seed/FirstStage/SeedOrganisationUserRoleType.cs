﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedOrganisationUserRoleType
    {
        public static ICollection<OrganisationUserRoleType> GetRoleTypes()
        {
            ICollection<OrganisationUserRoleType> result = new List<OrganisationUserRoleType>
            {
                new OrganisationUserRoleType { Title = "SuperAdmin", Description = "Full Access. Even self removing." },
                new OrganisationUserRoleType { Title = "Admin", Description = "Second admin. Can almost everything." },
                new OrganisationUserRoleType { Title = "Member", Description = "Сотрудник компании" },
                new OrganisationUserRoleType { Title = "Contractor", Description = "Подрядчик" },
                new OrganisationUserRoleType { Title = "EquipmentSupplier", Description = "Поставщик оборудования" },
                new OrganisationUserRoleType { Title = "ProductionFactory", Description = "Завод изготовитель" },
                new OrganisationUserRoleType { Title = "Consultant", Description = "Консалтинговые услуги" },
            };

            return result;
        }
    }
}