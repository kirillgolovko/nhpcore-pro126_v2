﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
   public  class SeedOperationDocumentType
    {
        public static IEnumerable<OperationDocumentType> SeedTypes()
        {
            return new List<OperationDocumentType> {
                new OperationDocumentType{ Name="Договор"},
                new OperationDocumentType{ Name="Счет"},
                new OperationDocumentType{ Name="Заявление о возврате"},
                /*smthng else*/
            };
        }
    }
}
