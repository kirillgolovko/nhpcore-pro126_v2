﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.AspNet.Identity.EntityFramework.CustomEntities
{
    public class CustomDbContext : IdentityDbContext<CustomUser, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public CustomDbContext()
            : base("DefaultConnection")
        {
        }

        public CustomDbContext(string connString)
            : base(connString)
        {
        }

        public static CustomDbContext Create()
        {
            return new CustomDbContext();
        }
    }
}
