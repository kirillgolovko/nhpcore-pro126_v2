﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Logging
{
    public abstract class BaseLoggerUtility : ILoggerUtility
    {
        public virtual void Debug(string message, Exception ex = null)
        {
        }

        public virtual void Error(string message, Exception ex = null)
        {
        }

        public virtual void Fatal(string message, Exception ex = null)
        {
        }

        public virtual void Info(string message, Exception ex = null)
        {
        }

        public virtual void Trace(string message, Exception ex = null)
        {
        }

        public virtual void Warn(string message, Exception ex = null)
        {
        }
    }
}