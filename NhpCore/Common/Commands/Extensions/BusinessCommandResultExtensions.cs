﻿namespace Common.Commands
{
    /// <summary>Расширения для результатов команд</summary>
    public static class BusinessCommandResultExtensions
    {
        public static TResult AddError<TResult>(this TResult result, string error, int errorCode)
            where TResult : IBusinessCommandResult
        {
            if (result == null)
                return result;

            result.ErrorMessage = error;
            result.IsSuccess = false;
            result.ErrorCode = errorCode;
            return result;
        }
    }
}