﻿using System.Threading.Tasks;

namespace Common.Commands
{
    /// <summary>Команда, выполняющая какие-либо действия</summary>
    /// <typeparam name="TArguments">Тип входных аргументов для команды</typeparam>
    public interface IBusinessCommand<TArguments, TResult>
        where TArguments : IBusinessCommandArguments
        where TResult : IBusinessCommandResult
    {
        Task<TResult> Execute(TArguments arguments);
    }
}