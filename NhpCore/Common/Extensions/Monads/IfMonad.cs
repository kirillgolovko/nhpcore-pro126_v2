﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class IfMonad
    {
        public static TInput If<TInput>(this TInput source, Func<TInput, bool> evaluator)
            where TInput : class
        {
            if (source == null)
            {
                return null;
            }

            return evaluator(source) ? source : null;
        }
    }
}
