﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.EntityFramework
{
    public interface IDisableableState
    {
        bool IsEnabled { get; set; }
        DateTime? DisablingTime { get; set; }
    }
}