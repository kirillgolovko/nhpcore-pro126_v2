﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Validation
{
    public interface IAnnotationValidator
    {
        AnnotationValidationResult IsValid<T>(T obj);
        AnnotationValidationResult IsValid<T, U>(T obj);

    }
}
