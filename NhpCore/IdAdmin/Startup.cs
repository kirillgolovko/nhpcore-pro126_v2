﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IdentityServerAdmin.Configuration;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdAdmin
{
    public static class ClaimsExtensions
    {
        public static string GetValue(this IEnumerable<Claim> claims, string type)
        {
            if (claims == null) throw new ArgumentNullException("claims");
            if (String.IsNullOrWhiteSpace(type)) throw new ArgumentNullException("type");

            var claim = claims.FirstOrDefault(x => x.Type == type);
            if (claim != null)
            {
                return claim.Value;
            }

            return null;
        }
    }

    public class Startup
    {
        bool _isSslRequired;
        string _connstrIdSrv = "connstr.idsrv";

        public void Configuration(IAppBuilder app)
        {
            app.UseOpenIdConnectAuthentication(new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc",
                Authority = "http://localhost:57556/identity",
                ClientId = "idAdmin_client",
                RedirectUri = "http://localhost:62055/identity-admin",
                ResponseType = "id_token",
                UseTokenLifetime = false,
                Scope = "openid idAdmin",
                SignInAsAuthenticationType = "Cookies",
                Notifications = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        n.AuthenticationTicket.Identity.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = async n =>
                    {
                        if (n.ProtocolMessage.RequestType == Microsoft.IdentityModel.Protocols.OpenIdConnectRequestType.LogoutRequest)
                        {
                            var res = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");
                            if (res != null)
                            {
                                var id_token = res.Identity.Claims.GetValue("id_token");

                                if (id_token != null)
                                {
                                    n.ProtocolMessage.IdTokenHint = id_token;
                                    n.ProtocolMessage.PostLogoutRedirectUri = "http://localhost:62055/identity-admin";
                                }
                            }
                        }
                    }
                }
            });



            app.ConfigureIdentityServerAdmin(GetOptions());
        }

        private IdentityServerAdminConfigurationOptions GetOptions()
        {
            IdentityServerAdminConfigurationOptions result = new IdentityServerAdminConfigurationOptions();
            result.IdSrvConnStr = _connstrIdSrv;
            result.IsSslRequired = _isSslRequired;

            return result;

        }
    }
}