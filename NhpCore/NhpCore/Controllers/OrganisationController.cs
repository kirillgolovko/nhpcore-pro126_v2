﻿using Common.Commands;
using Common.Logging;
using Common.Mapping;
using MoreLinq;
using Newtonsoft.Json;
using NhpCore.Attributes;
using NhpCore.Business.Commands;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Utils.ServiceSynchronization;
using NhpCore.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    public class OrganisationController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IMapperUtility _mapperUtility;
        private readonly IServiceSynchronization _syncUtil;

        public OrganisationController(IUserService userService,
            IOrganisationService organisationService,
            IMapperUtility mapper,
            IServiceSynchronization syncUtil)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (mapper == null) { throw new ArgumentNullException(nameof(mapper)); }
            if (syncUtil == null) { throw new ArgumentNullException(nameof(syncUtil)); }

            _userService = userService;
            _organisationService = organisationService;
            _mapperUtility = mapper;
            _syncUtil = syncUtil;
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> CreateOrganisation(string name)
        {
            var result = string.Empty;

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            User user = await _userService.GetCoreUser(userId);


            // 1. создание организации
            var createCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateOrganisationArgs, CreateOrganisationResult>>();
            var createResult = await createCommand.Execute(new CreateOrganisationArgs { CreatorId = userId, ShortName = name });

            if (!createResult.IsSuccess)
            {
            }
#if !DEBUG
            // 2. синхронизация по базе
            await _syncUtil.AddOrganisation(createResult.OrganisationId, createResult.Name, createResult.RegistrationDate);
#endif

            // 3. создание записи о пользователе в этой организации
            var createUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();
            var userEntryResult = await createUserEntryCommand.Execute(new CreateUserEntryArgs { IdentityId = userId, IsPersonal = true, OrganisationId = createResult.OrganisationId, ProfileId= user.Profiles.First().Id });

            if (!userEntryResult.IsSuccess)
            {
            }

#if !DEBUG
            // 4. синхронизация добавленных пользователей
            await _syncUtil.AddUserToOrganisation(userId, createResult.OrganisationId);
#endif

            // 5. присвоение роли пользователю в организации
            var organisationService = DependencyResolver.Current.GetService<IOrganisationService>();
            var role = await organisationService.GetOrganisationUserRoleTypeByTitle("SuperAdmin");

            var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
            var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = userEntryResult.CreatedUserEntryId, UserRoleTypeId = role.Id });

            if (!createUserRoleCommandResult.IsSuccess)
            {
            }
            result = JsonConvert.SerializeObject(createResult);
            return Json(result);
        }


        [HttpGet, AjaxOnly] // получить пользователей организации
        public async Task<ActionResult> GetOrganisationUserEntries(int organisationId)
        {
            var userEntries = await _organisationService.GetUserEntries(organisationId);
            // TODO : сделать маппинг нужных полей в DTO
            var result = JsonConvert.SerializeObject(userEntries);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        class OrgDto
        {
            public int id { get; set; }
            public string name { get; set; }
            public double balanceValue { get; set; }
        }

        [HttpGet, AjaxOnly] //получить доступные организации
        //public async Task<ActionResult> GetAvailable(int userId)
        public async Task<ActionResult> GetAvailable()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            var availableOrgs = await _userService.GetAvailableOrganisations(userId);

            var Ids = availableOrgs.Select(x => x.Id).ToList();

            var orgs = await _organisationService.GetOrganisationList(Ids);

            var availableOrgsDto = orgs.Select(x =>
            {
                var isSuperAdmin = x.Users.Where(r => r.UserId == userId).First().UserRoles.Where(ur => ur.OrganisationUserRoleTypeId == 1).FirstOrDefault();
                OrganisationDocumentsStorageAccess odsa = isSuperAdmin == null ? null : x.StorageAccess.LastOrDefault();
                if (isSuperAdmin != null && odsa == null)
                {
                    odsa = new OrganisationDocumentsStorageAccess { OrganisationId = x.Id, AssignerId = userId };
                }
                OrganisationDetailCard odc = x.Details ?? new OrganisationDetailCard { };
                return new
                {
                    id = x.Id,
                    name = x.Name,
                    StorageAccess = odsa == null ? null : new dtoOrganisationStorageAccess
                    {
                        Id = odsa.Id,
                        OrganisationId = odsa.OrganisationId,
                        CreationDateTime = odsa.CreationDateTime,
                        AccessName = odsa.AccessName,
                        StorageType = odsa.StorageType,
                        AccessType = odsa.AccessType,
                        AssignerId = odsa.AssignerId,
                        Token = odsa.Token,
                        RefreshToken = odsa.RefreshToken,
                        ExpiresIn = odsa.ExpiresIn,
                        Email = odsa.Email,
                        Password = odsa.Password,
                        ServerUri = odsa.ServerUri
                    },
                    Details = odc == null ? null : new dtoOrganisationDetailCard
                    {
                        Id = odc.Id,
                        FullName = odc.FullName,
                        Logo = odc.Logo,
                        ShortName = odc.ShortName,
                        OGRN = odc.OGRN,
                        INN = odc.INN,
                        KPP = odc.KPP,
                        OKPO = odc.OKPO,
                        OKATO = odc.OKATO,
                        LegalAddress = odc.LegalAddress,
                        PhysicalAddress = odc.PhysicalAddress,
                        PhoneNumber = odc.PhoneNumber,
                        PhoneNumberAdditional = odc.PhoneNumberAdditional,
                        Email = odc.Email,
                        DirectorString = odc.DirectorString,
                        DirectorId = odc.DirectorId,
                        ChiefAccountantString = odc.ChiefAccountantString,
                        ChiefAccountantId = odc.ChiefAccountantId,
                        BankCorporateAccount = odc.BankCorporateAccount,
                        BankName = odc.BankName,
                        BankAddress = odc.BankAddress,
                        BankOGRN = odc.BankOGRN,
                        BankOKPO = odc.BankOKPO,
                        BankBIK = odc.BankBIK,
                        BankCorrespondentAccount = odc.BankCorrespondentAccount
                    },
                    balanceValue = x.Balance.Value,
                    userRoles = x.Users.Where(r => r.UserId == userId).First().UserRoles.Select(ur => new
                    {
                        Id = ur.OrganisationUserRoleType.Id,
                        Name = ur.OrganisationUserRoleType.Title
                    }).ToList(),
                    Profile = x.Users.Where(u => u.UserId == userId).Select(u => new
                    {
                        Id = u.ProfileId,
                        DisplayName = u.Profile.DisplayName,
                        MobilePhone = u.Profile.MobilePhone1
                    }).FirstOrDefault()
                };
            }).ToList();

            // TODO : сделать маппинг нужных полей в DTO
            var result = JsonConvert.SerializeObject(availableOrgsDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Invites
        [HttpGet, AjaxOnly] // получить список приглашенных юзеров
        public async Task<ActionResult> GetInvitedUsers(int organisationId)
        {
            var invitedUsers = await _organisationService.GetInvitedUsers(organisationId);
            IEnumerable<dtoInvitedUsers> invitedUsersDto = invitedUsers.Select(x =>
            {
                try
                {
                    var dispName = x.IsAccepted == true ? $"{x.OrganisationUserEntry.Profile.SecondName} {x.OrganisationUserEntry.Profile.FirstName} {x.OrganisationUserEntry.Profile.PatronymicName} ({x.OrganisationUserEntry.Profile.JobTitle})" : $"{x.Email} (профиль не заполнен)";
                    var statusText = x.IsAccepted == true ? "Зарегистрирован" : "Приглашен";
                    return new dtoInvitedUsers
                    {
                        id = x.Id,
                        userId = x.UserId,
                        inviteDate = x.InviteSentDate.ToString("dd.MM.yyyy"),
                        displayName = dispName,
                        status = x.IsAccepted,
                        email = x.Email,
                        userroles = x.OrganisationUserEntry.UserRoles.Select(ur =>
                        {
                            try
                            {
                                OrganisationUserRoleType ourt = ur.OrganisationUserRoleType;
                                return new dtoOrganisationUserRoleType
                                {
                                    Id = ur.OrganisationUserRoleType.Id,
                                    Title = ur.OrganisationUserRoleType.Title,
                                    Description = ur.OrganisationUserRoleType.Description,
                                    IsSet = true
                                };

                            }
                            catch (Exception)
                            {
                                return new dtoOrganisationUserRoleType
                                {
                                    Id = -1,
                                    Title = "unknown",
                                    Description = "Неизвестный",
                                    IsSet = true
                                };
                            }
                        }).ToList()
                    };
                }
                catch (Exception)
                {
                    return new dtoInvitedUsers
                    {
                        id = x.Id,
                        userId = x.UserId,
                        inviteDate = x.InviteSentDate.ToString("dd.MM.yyyy"),
                        displayName = $"{x.Email} (профиль не заполнен)",
                        status = x.IsAccepted,
                        email = x.Email
                    };
                }
            });
            invitedUsersDto = invitedUsersDto.OrderBy(x => x.displayName).ToList();
            var result = JsonConvert.SerializeObject(invitedUsersDto);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly] // удалить юзера. IsDeleted = true
        public async Task<ActionResult> DeleteInvitedUser(Guid id)
        {
            // команда
            var deleteUserInviteCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteUserInviteArgs, DeleteUserInviteResult>>();
            var commandResult = await deleteUserInviteCommand.Execute(new DeleteUserInviteArgs { Id = id });

            var result = JsonConvert.SerializeObject(commandResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly] // пригласить юзеров
        public async Task<ActionResult> InviteUsers(int organisationId, string[] emails)
        {
            // валидация имейлов
            List<string> invalidEmails = new List<string>();
            List<string> validEmails = new List<string>();

            string emailPattern = @"^\w+([-.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
            Regex regex = new Regex(emailPattern, RegexOptions.IgnoreCase);

            for (int i = 0; i < emails.Length; i++)
            {
                var isValid = regex.IsMatch(emails[i]);

                if (isValid) { validEmails.Add(emails[i]); }
                else { invalidEmails.Add(emails[i]); }
            }

            var emailsInDb = await _organisationService.GetIdentityUsers(validEmails);
            Dictionary<string, int> dict = new Dictionary<string, int>();
            emailsInDb.ForEach(x => dict.Add(x.Email, x.Id));

            // добавление команд
            var inviteCommand = DependencyResolver.Current.GetService<IBusinessCommand<InviteUsersArgs, InviteUsersResult>>();
            var inviteCommandResult = await inviteCommand.Execute(new InviteUsersArgs
            {
                OrganisationId = organisationId,
                Emails = validEmails,
                ExistedEmails = dict,
                RegistrationUrl = Url.Action("Register", "Account", null, protocol: Request.Url.Scheme),
                AcceptInviteUrl = Url.Action("AcceptInvite", "Organisation", null, protocol: Request.Url.Scheme)
            });

            var resultDto = new
            {
                inviteResult = inviteCommandResult,
                invalidEmails = invalidEmails
            };

            // TODO: сделать на клиенте в ajax success response
            // добавление валидных в список инвайченых, а о инвалидных вывести уведомление
            var result = JsonConvert.SerializeObject(resultDto);
            return Json(result);

        }

        // названия входных параметров используются в текстовом виде в InviteUsersCommand - не менять
        public async Task<ActionResult> AcceptInvite(string inviteCode, int identityId)
        {
            var logger = DependencyResolver.Current.GetService<ILoggerUtility>();

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);


            var invite = await _userService.GetUserInviteByInviteCode(inviteCode);

            //Этот инвайт уже принят или его не существует
            if (invite == null || invite.IsAccepted)
            {
                logger.Debug("invite wasn't found or already accepted");

                return View("Error");
            }
            try
            {


                // 1. подтверждение инвайта
                var acceptCommand = DependencyResolver.Current.GetService<IBusinessCommand<AcceptUserInviteArgs, AcceptUserInviteResult>>();
                var acceptInviteResult = await acceptCommand.Execute(new AcceptUserInviteArgs { InviteCode = inviteCode, IdentityId = identityId });

                if (!acceptInviteResult.IsSuccess)
                {
                    logger.Debug($"AcceptUserInvite Command failed - {acceptInviteResult.ErrorCode} - {acceptInviteResult.ErrorMessage}");
                    return View("Error");
                }

                // 2. создание записи о пользователе в организации
                User user = await _userService.GetCoreUser(userId);

                var createUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();
                var createUserEntryResult = await createUserEntryCommand.Execute(new CreateUserEntryArgs { IdentityId = identityId, OrganisationId = acceptInviteResult.UserInviteEntity.OrganisationId, ProfileId = user.Profiles.First().Id });

                if (!createUserEntryResult.IsSuccess)
                {
                    logger.Debug($"createUserEntryFailed - {createUserEntryResult.ErrorCode} - {createUserEntryResult.ErrorMessage}");
                    return View("Error");
                }

                // 3: добавление пользователя в организацию на сторонних сервисах
                await _syncUtil.AddUserToOrganisation(userId, invite.OrganisationId);

                // TODO 4: назначение пользователя и записи о пользователе к инвайту
                var setUserAndUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<SetUserAndUserEntryToUserInviteArgs, SetUserAndUserEntryToUserInviteResult>>();
                var setUserAndUserEntryResult = await setUserAndUserEntryCommand.Execute(new SetUserAndUserEntryToUserInviteArgs { InviteId = acceptInviteResult.UserInviteEntity.Id, UserEntryId = createUserEntryResult.CreatedUserEntryId });

                if (!setUserAndUserEntryResult.IsSuccess)
                {
                    logger.Debug($"Set User And UserEntry to Invite - failed - {setUserAndUserEntryResult.ErrorCode} - {setUserAndUserEntryResult.ErrorMessage}");
                    return View("Error");
                }

                var organisationService = DependencyResolver.Current.GetService<IOrganisationService>();
                var role = await organisationService.GetOrganisationUserRoleTypeByTitle("Member");

                var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
                var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = setUserAndUserEntryResult.OrganisationUserEntryId, UserRoleTypeId = role.Id });

                if (!createUserRoleCommandResult.IsSuccess)
                {
                    logger.Debug($"create user role - failed - {createUserRoleCommandResult.ErrorCode} - {createUserRoleCommandResult.ErrorMessage}");

                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                logger.Debug("AcceptInvite Action was failed", ex);
                return View("Error");

            }
            return View();
        }

        #endregion

        #region UserProfile
        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetOrganisationUserEntry(int organisationId)
        {
            var result = "";
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);
            var organisationUserEntry = await _userService.GetUserEntryByIdentityId(organisationId, userId);

            if (organisationUserEntry == null || organisationUserEntry.UserProfile == null) { result = "error"; }

            result = JsonConvert.SerializeObject(organisationUserEntry.UserProfile);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region OrganisationProfile
        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetOrganisationDetails(int organisationId)
        {
            var result = "";
            var organisation = await _organisationService.GetOrganisation(organisationId);
            if (organisation == null || organisation.Details == null) { result = "error"; }

            var details = organisation.Details;

            result = JsonConvert.SerializeObject(organisation.Details);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> EditOrganisationDetails(OrganisationDetailCard model)
        {
            var result = "";
            if (model.KPP == null || model.KPP.Length == 0)
            {
                if (model.INN.Length == 10)
                {
                    model.KPP = $"{model.INN.Substring(0, 4)}01001";
                }
            }
            // команда для редактирования карточки организации
            Organisation organisation = new Organisation { Id = model.Id, Name = model.ShortName };

            var editOrganisationCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditOrganisationArgs, EditOrganisationResult>>();
            var editOrganisationResult = await editOrganisationCommand.Execute(new EditOrganisationArgs { organisation = organisation });

            var editDetailsCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditOrganisationDetailsArgs, EditOrganisationDetailsResult>>();
            var editResult = await editDetailsCommand.Execute(new EditOrganisationDetailsArgs { DetailCard = model });

            result = JsonConvert.SerializeObject(editResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> EditOrganisationStorageAccess(dtoOrganisationStorageAccess dto)
        {
            var result = "";
            var editresult = new EditStorageAccessResult { IsSuccess = true };
            dto.CreationDateTime = DateTime.Now.AddMilliseconds(-DateTime.Now.Millisecond);
            if (dto.Id == 0)
            {
                var addStorageAccessCommand = DependencyResolver.Current.GetService<IBusinessCommand<AddStorageCredentialsArgs, AddStorageCredentialsResult>>();
                var addresult = await addStorageAccessCommand.Execute(new AddStorageCredentialsArgs { AccessType = dto.AccessType, Email = dto.Email, ExpiresIn = dto.ExpiresIn, OrganisationId = dto.OrganisationId, Password = dto.Password, RefreshToken = dto.RefreshToken, StorageType = dto.StorageType, Token = dto.Token, UserId = dto.AssignerId });
                editresult.IsSuccess = addresult.IsSuccess;
                editresult.Id = addresult.Id;
            }
            else
            {
                var editStorageAccessCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditStorageAccessArgs, EditStorageAccessResult>>();
                editresult = await editStorageAccessCommand.Execute(new EditStorageAccessArgs {  dto=dto});
            }

            result = JsonConvert.SerializeObject(editresult);

            return Json(result);
        }

        #endregion

        // Владимир, сказал, что баланс пока ненужен...
        //[HttpGet, AjaxOnly] // переход на яндекс кассу с редиректом
        //public async Task<ActionResult> AddBalance()
        //{

        //    return Redirect("");
        //}

        //[HttpPost] // запрос от яндекса при успешной оплате
        //public async Task<ActionResult> AddBalanceSuccess()
        //{
        //    return null;
        //}
    }
}