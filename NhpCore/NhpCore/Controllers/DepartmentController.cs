﻿using Common.Commands;
using Common.Logging;
using Common.Mapping;
using MoreLinq;
using Newtonsoft.Json;
using NhpCore.Attributes;
using NhpCore.Business.Commands;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NhpCore.DTOs;
using Microsoft.AspNet.Identity.Owin;
using NhpCore.Infrastructure.Data.Ef;

namespace NhpCore.Controllers
{
    public class DepartmentController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IDepartmentService _departmentService;
        private IEnumerable<OrganisationUserRoleType> ourtDictionary;

        public DepartmentController(IUserService userService, IOrganisationService organisationService, IDepartmentService departmentService)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (departmentService == null) { throw new ArgumentNullException(nameof(departmentService)); }
            _userService = userService;
            _organisationService = organisationService;
            _departmentService = departmentService;

        }

        [HttpGet, AjaxOnly] //получить доступные подразделения
        public async Task<ActionResult> GetAvailable(int organisationId)
        {
            char pad = '.';
            var org_id = organisationId;
            var deps = await _departmentService.GetAvailable(org_id);
            ourtDictionary = await _organisationService.GetOrganisationUserRoleTypes();
            Organisation organisation = await _organisationService.GetOrganisation(org_id);
            IEnumerable<dtoDepartment> depsDto = deps.Where(x => x.ParentId == null).OrderBy(x => x.ParentId).OrderBy(x => x.Name).Select(x =>
            {
                List<int> child = getChild(deps, x.Id);
                IEnumerable<Department> availableParents = deps.Where(p => child.Contains(p.Id) == false && p.Id != x.Id);
                availableParents = reorderAvailableParents(availableParents);
                dtoDepartment dto = new dtoDepartment
                {
                    Id = x.Id,
                    ParentId = 0,
                    Level = x.Level,
                    availableParents = availableParents.Select(a => new dtoDepartment
                    {
                        Id = a.Id,
                        Name = a.Name.PadLeft(a.Name.Length + a.Level, pad)
                    }).ToList(),
                    Name = x.Name,
                    Description = x.Description,
                    Members = x.Members.Select(y =>
                    {
                        OrganisationUserEntry oue = y.Organisations.Where(o => o.OrganisationId == org_id).FirstOrDefault();
                        try
                        {
                            UserProfile up = y.Profiles.Where(p => p.Id == oue.ProfileId).FirstOrDefault();
                            return new dtoMember
                            {
                                Id = y.Id,
                                Name = $"{up.SecondName} {up.FirstName} {up.PatronymicName} ({up.JobTitle})",
                                Salary = oue.Salary ?? 0,
                                IsEnabled = !oue.IsEnabled,
                                DisablingTime = oue.DisablingTime,
                                IsDeleted = oue.IsDeleted,
                                DeleteTime = oue.DeleteTime,
                                UserRoles = getUserRoles(oue.UserRoles)
                            };
                        }
                        catch (Exception)
                        {
                            return new dtoMember
                            {
                                Id = y.Id,
                                Name = y.Invites.Count > 0 ? $"{y.Invites.First().Email} (профиль не заполнен)" : "Нет закрепленного профиля",
                                Salary = oue.Salary ?? 0,
                                IsEnabled = !oue.IsEnabled,
                                DisablingTime = oue.DisablingTime,
                                IsDeleted = oue.IsDeleted,
                                DeleteTime = oue.DeleteTime,
                                UserRoles = getUserRoles(oue.UserRoles)
                            };
                        }
                    }).ToList(),
                    Nodes = GetNodes(deps, x.Id, org_id).ToList()
                };
                dto.Members = dto.Members.OrderBy(m => m.Name);
                return dto;
            }).ToList();

            IEnumerable<OrganisationUserInvite> users = await _organisationService.GetInvitedUsers(org_id);
            var ids = users.Where(x => x.IsAccepted == true).Select(x => x.UserId).ToList();
            List<dtoMember> members = new List<dtoMember> { };
            deps.ForEach(x => x.Members.ForEach(y =>
            {
                if (ids.Contains(y.Id))
                {
                    ids.Remove(y.Id);
                }
            }));

            if (ids.Count > 0)
            {
                members = users.Where(x => ids.Contains(x.UserId) == true).Select(y =>
                {
                    OrganisationUserEntry oue = y.OrganisationUserEntry;
                    try
                    {
                        UserProfile up = y.User.Profiles.Where(p => p.Id == oue.ProfileId).FirstOrDefault();
                        return new dtoMember
                        {
                            Id = (int)y.UserId,
                            Name = $"{up.SecondName} {up.FirstName} {up.PatronymicName} ({up.JobTitle})",
                            Salary = oue.Salary ?? 0,
                            //Salary = oue.Salary == null ? 0 : (double)oue.Salary,
                            IsEnabled = !oue.IsEnabled,
                            DisablingTime = oue.DisablingTime,
                            IsDeleted = oue.IsDeleted,
                            DeleteTime = oue.DeleteTime,
                            UserRoles = getUserRoles(oue.UserRoles)
                        };
                    }
                    catch (Exception)
                    {
                        return new dtoMember
                        {
                            Id = (int)y.UserId,
                            Name = $"{y.Email} (профиль не заполнен)",
                            Salary = oue.Salary ?? 0,
                            //Salary = oue.Salary == null ? 0 : (double)oue.Salary,
                            IsEnabled = !oue.IsEnabled,
                            DisablingTime = oue.DisablingTime,
                            IsDeleted = oue.IsDeleted,
                            DeleteTime = oue.DeleteTime,
                            UserRoles = getUserRoles(oue.UserRoles)
                        };
                    }
                }).ToList();
                members = members.OrderBy(x => x.Name).ToList();
            }

            dtoDepartment mainOrg = new dtoDepartment
            {
                Id = organisation.Id,
                ParentId = null,
                availableParents = { },
                Name = members.Count == 0 ? organisation.Name : $"{organisation.Name} ({members.Count} - не в структуре)",
                Members = members,
                Nodes = depsDto
            };

            var resultDto = JsonConvert.SerializeObject(mainOrg);

            return Json(resultDto, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<dtoOrganisationUserRoleType> getUserRoles(IEnumerable<OrganisationUserRole> userRole)
        {
            var ids = userRole.Select(ur => ur.OrganisationUserRoleType.Id).ToList();
            return ourtDictionary.Select(d =>
            {
                if (ids.Contains(d.Id))
                {
                    return new dtoOrganisationUserRoleType
                    {
                        Id = d.Id,
                        Title = d.Title,
                        Description = d.Description,
                        IsSet = true
                    };
                }
                else
                {
                    return new dtoOrganisationUserRoleType
                    {
                        Id = d.Id,
                        Title = d.Title,
                        Description = d.Description,
                        IsSet = false
                    };
                }

            }).ToList();
        }

        public List<int> getChild(IEnumerable<Department> parentNodes, int ParentId)
        {
            List<int> child = parentNodes.Where(c => c.ParentId == ParentId).Select(x => x.Id).ToList();
            List<int> newchild = new List<int> { };
            child.ForEach(x => newchild.AddRange(getChild(parentNodes, x)));
            child.AddRange(newchild);
            return child;
        }

        public IEnumerable<Department> reorderAvailableParents(IEnumerable<Department> parentNodes)
        {
            IEnumerable<Department> main = parentNodes.Where(x => x.ParentId == null).OrderBy(x => x.Name);
            List<Department> result=new List<Department> { };
            main.ForEach(x => {
                result.Add(x);
                result.AddRange(getAvailableParentsChild(parentNodes, x.Id));
            });
            return result;
        }

        public IEnumerable<Department> getAvailableParentsChild(IEnumerable<Department> parentNodes,int ParentId)
        {
            List<Department> result = new List<Department> { };
            IEnumerable<Department> data = parentNodes.Where(x => x.ParentId == ParentId).OrderBy(x => x.Name);
            data.ForEach(x => {
                result.Add(x);
                result.AddRange(getAvailableParentsChild(parentNodes, x.Id));
            });
            return result;
        }

        public IEnumerable<dtoDepartment> GetNodes(IEnumerable<Department> parentNodes, int ParentId, int org_id)
        {
            IEnumerable<dtoDepartment> depsDto = parentNodes.Where(x => x.ParentId == ParentId).OrderBy(x => x.Name).Select(x =>
              {
                  char pad = '.';
                  List<int> child = getChild(parentNodes, x.Id);
                  IEnumerable<Department> availableParents = parentNodes.Where(p => child.Contains(p.Id) == false && p.Id != x.Id);
                  availableParents = reorderAvailableParents(availableParents);
                  dtoDepartment dto = new dtoDepartment
                  {
                      Id = x.Id,
                      ParentId = x.ParentId,
                      Level = x.Level,
                      availableParents = availableParents.Select(a =>
                      {
                          return new dtoDepartment
                          {
                              Id = a.Id,
                              Name = a.Name.PadLeft(a.Name.Length + a.Level, pad)
                          };
                      }).ToList(),
                      Name = x.Name,
                      Description = x.Description,
                      Members = x.Members.Select(y =>
                      {
                          OrganisationUserEntry oue = y.Organisations.Where(o => o.OrganisationId == org_id).FirstOrDefault();
                          try
                          {
                              UserProfile up = y.Profiles.Where(p => p.Id == oue.ProfileId).FirstOrDefault();
                              return new dtoMember
                              {
                                  Id = y.Id,
                                  Name = $"{up.SecondName} {up.FirstName} {up.PatronymicName} ({up.JobTitle})",
                                  Salary = oue.Salary ?? 0,
                                  //Salary = oue.Salary == null ? 0 : (double)oue.Salary,
                                  IsEnabled = !oue.IsEnabled,
                                  DisablingTime = oue.DisablingTime,
                                  IsDeleted = oue.IsDeleted,
                                  DeleteTime = oue.DeleteTime,
                                  UserRoles = getUserRoles(oue.UserRoles)
                              };
                          }
                          catch (Exception)
                          {
                              return new dtoMember
                              {
                                  Id = y.Id,
                                  Name = y.Invites.Count > 0 ? $"{y.Invites.First().Email} (профиль не заполнен)" : "Нет закрепленного профиля",
                                  Salary = oue.Salary ?? 0,
                                  //Salary = oue.Salary == null ? 0 : (double)oue.Salary,
                                  IsEnabled = !oue.IsEnabled,
                                  DisablingTime = oue.DisablingTime,
                                  IsDeleted = oue.IsDeleted,
                                  DeleteTime = oue.DeleteTime,
                                  UserRoles = getUserRoles(oue.UserRoles)
                              };
                          }
                      }).ToList(),
                      Nodes = GetNodes(parentNodes, x.Id, org_id).ToList()
                  };
                  dto.Members = dto.Members.OrderBy(m => m.Name);
                  return dto;
              });
            return depsDto;
        }

        public int getLevel(IEnumerable<Department> Deparments, int DepartmentId)
        {
            Department department = Deparments.Where(x => x.Id == DepartmentId).FirstOrDefault();
            if (department == null)
            {
                return 0;
            }
            Department parent = department.Parent;
            if (parent == null)
            {
                return 1;
            }
            int? parentID = parent.Id;
            int level = 1;
            while (parentID != null)
            {
                level += 1;
                Department cParent = Deparments.Where(x => x.Id == parentID).FirstOrDefault();
                if (cParent == null)
                {
                    return level;
                }
                parentID = cParent.ParentId;
            }
            return level;
        }

        [HttpPost, AjaxOnly] //Создать подразделение
        public async Task<ActionResult> Create(Department _department)
        {
            var result = string.Empty;

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            _department.ChiefId = userId;
            if (_department.ParentId == _department.OrganisationId)
            {
                _department.ParentId = null;
                _department.Level = 1;
            }
            var createDepartmentCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateDepartmentArgs, CreateDepartmentResult>>();
            var createDepartmentResult = await createDepartmentCommand.Execute(new CreateDepartmentArgs { Department = _department });
            result = JsonConvert.SerializeObject(createDepartmentResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly] //Изменить подразделение
        public async Task<ActionResult> Edit(Department _department)
        {
            var result = string.Empty;
            if (_department.ParentId == 0)
            {
                _department.ParentId = null;
                _department.Level = 1;
            }

            var editDepartmentCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditDepartmentArgs, EditDepartmentResult>>();
            var editDepartmentCommandResult = await editDepartmentCommand.Execute(new EditDepartmentArgs { Department = _department });

            result = JsonConvert.SerializeObject(editDepartmentCommandResult);

            return Json(result);
        }

        [HttpGet]
        public async Task<ActionResult> CalculateLevel()
        {
            var deps = await _departmentService.GetList();
            foreach (Department dep in deps)
            {
                if (dep.Level == 0)
                {
                    dep.Level = getLevel(deps, dep.Id);
                    var editDepartmentCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditDepartmentArgs, EditDepartmentResult>>();
                    var editDepartmentCommandResult = await editDepartmentCommand.Execute(new EditDepartmentArgs { Department = dep });
                }
            }

            return RedirectToAction("Index","Home");
        }

        [HttpPost, AjaxOnly] //Удалить подразделение
        public async Task<ActionResult> Delete(Department _department)
        {
            var result = string.Empty;
            var deleteDepartmentCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteDepartmentArgs, EmptyBusinessCommandResult>>();
            var deleteDepartmentCommandResult = await deleteDepartmentCommand.Execute(new DeleteDepartmentArgs { Id = _department.Id });

            result = JsonConvert.SerializeObject(deleteDepartmentCommandResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly] //Получить данные о сотруднике
        public async Task<ActionResult> GetMember(int userId, int organisationId)
        {
            var result = string.Empty;
            User user = await _userService.GetCoreUser(userId);
            OrganisationUserEntry oue = await _organisationService.GetUserEntry(organisationId, userId);
            ourtDictionary = await _organisationService.GetOrganisationUserRoleTypes();
            dtoMember member = new dtoMember { };
            try
            {
                UserProfile up = oue.Profile;
                member = new dtoMember
                {
                    Id = userId,
                    Name = $"{up.SecondName} {up.FirstName} {up.PatronymicName} ({up.JobTitle})",
                    Salary = oue.Salary == null ? 0 : (double)oue.Salary,
                    IsEnabled = !oue.IsEnabled,
                    DisablingTime = oue.DisablingTime,
                    IsDeleted = oue.IsDeleted,
                    DeleteTime = oue.DeleteTime,
                    UserRoles = getUserRoles(oue.UserRoles)
                };
            }
            catch (Exception ex)
            {
                var aa = ex.Message;
                member = new dtoMember
                {
                    Id = userId,
                    Name = user.Invites.Count > 0 ? $"{user.Invites.First().Email} (профиль не заполнен)" : "Нет закрепленного профиля",
                    Salary = oue.Salary == null ? 0 : (double)oue.Salary,
                    IsEnabled = !oue.IsEnabled,
                    DisablingTime = oue.DisablingTime,
                    IsDeleted = oue.IsDeleted,
                    DeleteTime = oue.DeleteTime,
                    UserRoles = getUserRoles(oue.UserRoles)
                };
            }
            result = JsonConvert.SerializeObject(member);
            return Json(result);
        }

        [HttpPost, AjaxOnly] //Получить данные о сотруднике
        public async Task<ActionResult> GetDepartment(int departmentId, int organisationId)
        {
            var result = string.Empty;
            ourtDictionary = await _organisationService.GetOrganisationUserRoleTypes();
            IEnumerable<Department> deps = await _departmentService.GetAvailable(organisationId);
            Department department = await _departmentService.Get(departmentId);
            List<int> child = getChild(deps, department.Id);
            dtoDepartment dto = new dtoDepartment
            {
                Id = department.Id,
                ParentId = department.ParentId ?? 0,
                availableParents = deps.Where(p => child.Contains(p.Id) == false && p.Id != department.Id).Select(a => new dtoDepartment
                {
                    Id = a.Id,
                    Name = a.Name
                }).ToList(),
                Name = department.Name,
                Description = department.Description,
                Members = department.Members.Select(y =>
                {
                    OrganisationUserEntry oue = y.Organisations.Where(o => o.OrganisationId == organisationId).FirstOrDefault();
                    try
                    {
                        UserProfile up = y.Profiles.Where(p => p.Id == oue.ProfileId).FirstOrDefault();
                        return new dtoMember
                        {
                            Id = y.Id,
                            Name = $"{up.SecondName} {up.FirstName} {up.PatronymicName} ({up.JobTitle})",
                            Salary = oue.Salary ?? 0,
                            IsEnabled = !oue.IsEnabled,
                            DisablingTime = oue.DisablingTime,
                            IsDeleted = oue.IsDeleted,
                            DeleteTime = oue.DeleteTime,
                            UserRoles = getUserRoles(oue.UserRoles)
                        };
                    }
                    catch (Exception)
                    {
                        return new dtoMember
                        {
                            Id = y.Id,
                            Name = y.Invites.Count > 0 ? $"{y.Invites.First().Email} (профиль не заполнен)" : "Нет закрепленного профиля",
                            Salary = oue.Salary ?? 0,
                            IsEnabled = !oue.IsEnabled,
                            DisablingTime = oue.DisablingTime,
                            IsDeleted = oue.IsDeleted,
                            DeleteTime = oue.DeleteTime,
                            UserRoles = getUserRoles(oue.UserRoles)
                        };
                    }
                }).ToList(),
                Nodes = GetNodes(deps, department.Id, organisationId).ToList()
            };
            dto.Members = dto.Members.OrderBy(x => x.Name);
            result = JsonConvert.SerializeObject(dto);
            return Json(result);
        }

        [HttpPost, AjaxOnly] //Добавить пользователя в отдел
        public async Task<ActionResult> AddMember(int userId, int departmentId)
        {
            var result = string.Empty;
            if (userId != 0)
            {
                User user = new User { Id = userId };
                Department department = new Department { Id = departmentId };

                var addMemberCommand = DependencyResolver.Current.GetService<IBusinessCommand<AddMemberArgs, EmptyBusinessCommandResult>>();
                var addMemberResult = await addMemberCommand.Execute(new AddMemberArgs { UserId = user, DepartmentId = department });

                result = JsonConvert.SerializeObject(addMemberResult);
            }

            return Json(result);
        }

        [HttpPost, AjaxOnly] //Добавить пользователя в отдел
        public async Task<ActionResult> DeleteMember(int userId, int departmentId)
        {
            var result = string.Empty;
            if (userId != 0)
            {
                User user = new User { Id = userId };
                Department department = new Department { Id = departmentId };

                var deleteMemberCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteMemberArgs, EmptyBusinessCommandResult>>();
                var deleteMemberResult = await deleteMemberCommand.Execute(new DeleteMemberArgs { UserId = user, DepartmentId = department });

                result = JsonConvert.SerializeObject(deleteMemberResult);
            }

            return Json(result);
        }

        [HttpPost, AjaxOnly] //Добавить пользователя в отдел
        public async Task<ActionResult> EditMember(int organisationId, dtoMember member)
        {
            var result = string.Empty;

            OrganisationUserEntry oue = await _organisationService.GetUserEntry(organisationId, member.Id);
            if (oue == null) //Типа проверка
            {
                EmptyBusinessCommandResult bc = new EmptyBusinessCommandResult();
                bc.IsSuccess = false;
                result = JsonConvert.SerializeObject(bc);
            }
            else
            {
                //Если было включено, а теперь нет, то ставим даты и наоборот: убираем даты
                member.IsEnabled = !member.IsEnabled; //По форме галочка стоит, это значит что Сотрудник выключен
                oue.DisablingTime = member.DisablingTime;
                oue.DeleteTime = member.DeleteTime;
                if (oue.IsEnabled == true && member.IsEnabled == false) { oue.DisablingTime = DateTime.Now.AddMilliseconds(-DateTime.Now.Millisecond); }
                if (oue.IsEnabled == false && member.IsEnabled == true) { oue.DisablingTime = null; }
                if (oue.IsEnabled == true && member.IsEnabled == true) { oue.DisablingTime = null; }
                if (oue.IsDeleted == true && member.IsDeleted == false) { oue.DeleteTime = null; }
                if (oue.IsDeleted == false && member.IsDeleted == true) { oue.DeleteTime = DateTime.Now.AddMilliseconds(-DateTime.Now.Millisecond); }
                if (oue.IsDeleted == false && member.IsDeleted == false) { oue.DeleteTime = null; }
                oue.IsEnabled = member.IsEnabled;
                oue.IsDeleted = member.IsDeleted;

                oue.Salary = member.Salary;

                // Суть всей этой вакханалии проста:
                // Перебираем все роли что есть
                var ids = oue.UserRoles.Select(ur => ur.OrganisationUserRoleType.Id).ToList();
                foreach (dtoOrganisationUserRoleType dto in member.UserRoles)
                {
                    // Если эта роль притутствовала раньше, а теперь она выключена, то УДАЛИТЬ ЕЁ
                    if (ids.Contains(dto.Id))
                    {
                        if (dto.IsSet == false)
                        {
                            var urId = oue.UserRoles.Where(ur => ur.OrganisationUserRoleTypeId == dto.Id).First().Id;
                            var deleteUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteUserRoleArgs, EmptyBusinessCommandResult>>();
                            var deleteUserRoleCommandResult = await deleteUserRoleCommand.Execute(new DeleteUserRoleArgs { OrganisationUserEntryId = oue.Id, UserRoleId = urId });
                        }
                    }
                    // Если этой роли раньше небыло, а теперь она включена, то СОЗДАТЬ ЕЁ
                    else
                    {
                        if (dto.IsSet == true)
                        {
                            var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
                            var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = oue.Id, UserRoleTypeId = dto.Id });
                        }
                    }
                }

                var editOrganisationUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditOrganisationUserEntryArgs, EditOrganisationUserEntryResult>>();
                var editOrganisationUserEntryResult = await editOrganisationUserEntryCommand.Execute(new EditOrganisationUserEntryArgs { OrganisationUserEntry = oue });
                result = JsonConvert.SerializeObject(editOrganisationUserEntryResult);
            }

            return Json(result);
        }
    }
}