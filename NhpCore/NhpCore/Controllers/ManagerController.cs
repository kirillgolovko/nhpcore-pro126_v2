﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.DTOs;

using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NhpCore.Infrastructure.Data.Ef;

using Newtonsoft.Json;
using NhpCore.Attributes;
using System.Threading.Tasks;

namespace NhpCore.Controllers
{
    public class ManagerController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;

        public ManagerController(IUserService userService, IOrganisationService organisationService)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _userService = userService;
            _organisationService = organisationService;
        }

        public CustomUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private CustomUserManager _userManager;

        // GET: Manager
        public ActionResult Index()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            var userId = int.Parse(subClaim.Value);

            var customUser = UserManager.FindById(userId);
            ViewBag.UserId = userId;
            ViewBag.UserName = customUser.UserName;
            try
            {
                ViewBag.DisplayName = customUser.Claims.Where(x => x.ClaimType == "displayname").First().ClaimValue;
            }
            catch (Exception)
            {
                ViewBag.DisplayName = "Имя не задано";
            }
            return View();
        }
    }
}