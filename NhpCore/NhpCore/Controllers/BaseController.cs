﻿using NhpCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    [Auth]
    public class BaseController : Controller
    {
        protected virtual string GetPartialPath(string name)
        {
            return $"~/Views/Partial/{name}.cshtml";
        }
    }
}