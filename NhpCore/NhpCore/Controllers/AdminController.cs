﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.DTOs;

using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NhpCore.Infrastructure.Data.Ef;

using Newtonsoft.Json;
using NhpCore.Attributes;
using System.Threading.Tasks;

namespace NhpCore.Controllers
{
    public class AdminController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;

        public AdminController(IUserService userService, IOrganisationService organisationService)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _userService = userService;
            _organisationService = organisationService;
        }

        public CustomUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private CustomUserManager _userManager;


        // GET: Admin
        public ActionResult Index()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            var userId = int.Parse(subClaim.Value);

            var customUser = UserManager.FindById(userId);
            ViewBag.UserId = userId;
            ViewBag.UserName = customUser.UserName;
            try
            {
                ViewBag.DisplayName = customUser.Claims.Where(x => x.ClaimType == "displayname").First().ClaimValue;
            }
            catch (Exception)
            {
                ViewBag.DisplayName = "Имя не задано";
            }
            return View();
        }

        [HttpPost, AjaxOnly] //получить список организаций
        public async Task<ActionResult> GetAvailable()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            IEnumerable<Organisation> orgs = await _organisationService.GetOrganisations();

            var availableOrgsDto = orgs.Select(x =>
            {
                OrganisationDocumentsStorageAccess odsa = x.StorageAccess.LastOrDefault();
                if (odsa == null)
                {
                    odsa = new OrganisationDocumentsStorageAccess { OrganisationId = x.Id, AssignerId = userId };
                }
                OrganisationDetailCard odc = x.Details ?? new OrganisationDetailCard { };
                return new
                {
                    id = x.Id,
                    name = $"{x.Name} ({x.Id})",
                    StorageAccess = odsa == null ? null : new dtoOrganisationStorageAccess
                    {
                        Id = odsa.Id,
                        OrganisationId = odsa.OrganisationId,
                        CreationDateTime = odsa.CreationDateTime,
                        AccessName = odsa.AccessName,
                        StorageType = odsa.StorageType,
                        AccessType = odsa.AccessType,
                        AssignerId = odsa.AssignerId,
                        Token = odsa.Token,
                        RefreshToken = odsa.RefreshToken,
                        ExpiresIn = odsa.ExpiresIn,
                        Email = odsa.Email,
                        Password = odsa.Password,
                        ServerUri = odsa.ServerUri
                    },
                    Details = odc == null ? null : new dtoOrganisationDetailCard
                    {
                        Id = odc.Id,
                        FullName = odc.FullName,
                        Logo = odc.Logo,
                        ShortName = odc.ShortName,
                        OGRN = odc.OGRN,
                        INN = odc.INN,
                        KPP = odc.KPP,
                        OKPO = odc.OKPO,
                        OKATO = odc.OKATO,
                        LegalAddress = odc.LegalAddress,
                        PhysicalAddress = odc.PhysicalAddress,
                        PhoneNumber = odc.PhoneNumber,
                        PhoneNumberAdditional = odc.PhoneNumberAdditional,
                        Email = odc.Email,
                        DirectorString = odc.DirectorString,
                        DirectorId = odc.DirectorId,
                        ChiefAccountantString = odc.ChiefAccountantString,
                        ChiefAccountantId = odc.ChiefAccountantId,
                        BankCorporateAccount = odc.BankCorporateAccount,
                        BankName = odc.BankName,
                        BankAddress = odc.BankAddress,
                        BankOGRN = odc.BankOGRN,
                        BankOKPO = odc.BankOKPO,
                        BankBIK = odc.BankBIK,
                        BankCorrespondentAccount = odc.BankCorrespondentAccount
                    }
                };
            }).ToList();

            var result = JsonConvert.SerializeObject(availableOrgsDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}