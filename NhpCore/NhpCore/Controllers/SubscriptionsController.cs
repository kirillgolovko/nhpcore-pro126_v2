﻿using Common.Commands;
using Common.Mapping;
using Newtonsoft.Json;
using NhpCore.Common;
using NhpCore.Attributes;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using NhpCore.Business.Commands;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Entities.Enums;
using NhpCore.DTOs;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using NhpCore.Infrastructure.Data.Ef;
using System.Data;
using NhpCore.Infrastructure.Utils.MessageSender;

namespace NhpCore.Controllers
{
    public class SubscriptionsController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ISubscriptionService _subscriptionService;
        private readonly IOrganisationService _organisationService;
        private readonly IMapperUtility _mapper;
        private readonly IMessageSender _messageSender;
        private CustomUserManager _userManager;

        public SubscriptionsController(IUserService userService, ISubscriptionService subscriptionService, IOrganisationService organisationService, IMessageSender messageSender, IMapperUtility mapper)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (subscriptionService == null) { throw new ArgumentNullException(nameof(subscriptionService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (messageSender == null) throw new ArgumentNullException(nameof(messageSender));
            if (mapper == null) { throw new ArgumentNullException(nameof(mapper)); }

            _userService = userService;
            _subscriptionService = subscriptionService;
            _organisationService = organisationService;
            _messageSender = messageSender;
            _mapper = mapper;
        }

        public CustomUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Список доступных подписок
        /// </summary>
        [HttpPost, AjaxOnly]
        public async Task<ActionResult> GetAvailableSubscriptions()
        {
            var availableSubscriptions = await _subscriptionService.GetAvailable();

            List<dtoITProduct> dto = availableSubscriptions.Select(x =>
            {
                return new dtoITProduct
                {
                    Id = x.Id,
                    Name = x.Name,
                    FullName = x.FullName,
                    Subscriptions = x.Subscriptions.Where(s => s.IsActive == true).Select(s =>
                        {
                            TimePeriodType tpt = s.TimePeriodType;
                            var weTPT = "";
                            if (tpt == TimePeriodType.year) { weTPT = getWE(s.Duration, "год", "года", "лет"); }
                            if (tpt == TimePeriodType.month) { weTPT = getWE(s.Duration, "месяц", "месяца", "месяцев"); }
                            if (tpt == TimePeriodType.week) { weTPT = getWE(s.Duration, "неделя", "недели", "недель"); }
                            if (tpt == TimePeriodType.hour) { weTPT = getWE(s.Duration, "час", "часа", "часов"); }
                            if (tpt == TimePeriodType.day) { weTPT = getWE(s.Duration, "день", "дня", "дней"); }
                            return new dtoSubscription
                            {
                                Id = s.Id,
                                IsActive = s.IsActive,
                                ItProductId = s.ItProductId,
                                Duration = $"{s.Duration} {weTPT}",
                                Price = s.Price,
                                TimePeriodType = s.TimePeriodType
                            };
                        }).ToList()
                };
            }).ToList();

            var result = JsonConvert.SerializeObject(dto);
            return Json(result);
        }

        public string getWE(int Value, string One, string TwoFour, string FiveEtc)
        {
            switch (Value)
            {
                case 1:
                    return One;
                case 2:
                case 3:
                case 4:
                    return TwoFour;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                    return FiveEtc;
            }
            var endCharValue = Value.ToString();
            if (endCharValue.EndsWith("0")) { return FiveEtc; }
            endCharValue = endCharValue[-1].ToString();
            if (Value.ToString().Length > 2)
            {
                endCharValue = Value.ToString()[-2].ToString();
            }
            switch (endCharValue)
            {
                case "1":
                case "01":
                    return One;
                case "2":
                case "3":
                case "4":
                case "02":
                case "03":
                case "04":
                    return TwoFour;
                default:
                    return FiveEtc;
            }
        }

        /// <summary> получение юзеров с имеющимися у них подписками для списка с выбором юзеров при покупке </summary>
        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetUsersInfo(int organisationId, int subscriptionId)
        {
            //1. выбор юзеров
            var users = await _organisationService.GetUserEntries(organisationId);
            //2. прикрепить к ним информацию о уже купленных подписках
            var usersIds = users.Select(x => x.UserId).ToList();

            var subscriptionInfo = await _subscriptionService.GetUserEntries(subscriptionId, usersIds);

            // TODO: в идеале - нужно проверять наличие более расширеных подписок у пользователя.
            var subscriptionInfoDto = users.Select(x =>
            {
                var userSubscription = subscriptionInfo.Where(u => x.UserId == u.UserId).OrderBy(u => u.ExpirationDate).FirstOrDefault();


                var expirationDate = userSubscription == null ? // проверяем, есть ли подписка
                    string.Empty :// подписки нет - string.empty
                    userSubscription.ExpirationDate.HasValue == true ? // подписка есть - проверка наличия значения ExpirationDate
                            userSubscription.ExpirationDate.Value.ToString("dd.MM.yyyy") // значение есть
                            : string.Empty; // значения нет - string.empty

                string name = name = x.UserProfile.DisplayName ?? x.UserProfile.FirstName; ;

                return new
                {
                    userOrgEntryId = x.Id,
                    userId = x.UserId,
                    expirationDate = expirationDate,
                    name = name
                };
            }).ToList();

            var result = JsonConvert.SerializeObject(subscriptionInfoDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> GetLicenses(int organisationId)
        {
            var Licenses = await _subscriptionService.GetSubscriptionPayments(organisationId);

            List<dtoSubscriptionPayment> dtoSubscriptionPayment = Licenses.Select(x =>
            {
                return new dtoSubscriptionPayment
                {
                    Id = x.Id,
                    SubscriptionName = x.BoughtSubscription.ItProduct.Name,
                    Quantity = x.Quantity,
                    Price = x.Quantity * x.BoughtSubscription.Price,
                    BuyeerName = x.Buyeer.Name,
                    EndCustomerOrganisationName = x.EndCustomerOrganisation.Name,
                    InvalidationTime = x.InvalidationTime.HasValue ? x.InvalidationTime.GetValueOrDefault().ToShortDateString() : "",
                    LicenseCode = x.LicenseCode,
                    IsActivatedLicense = x.IsActivatedLicense,
                    DownloadLink = NhpConstants.DocumentsPath.License.Replace("{subPaymentId}", x.Id.ToString()).Replace("~", "")
                };
            }).ToList();

            var result = JsonConvert.SerializeObject(dtoSubscriptionPayment);
            return Json(result);
        }

        [HttpPost, AjaxOnly] // действие происходит при нажатии на кнопку "оплатить"
        public async Task<ActionResult> Pay(int organisationId, int subscriptionId, int duration, int[] users)
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost, AllowAnonymous] // обработка редиректа от платежной системы
        public async Task<ActionResult> SuccessfulPaid()
        {
            return null;
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> GetInvoices(int organisationId)
        {
            var Invoices = await _subscriptionService.GetSubscriptionPaymentBillList(organisationId);

            List<dtoSubscriptionPaymentBill> dtoSubscriptionPaymentBill = Invoices.OrderByDescending(x => x.Id).Select(x =>
              {
                  return new dtoSubscriptionPaymentBill
                  {
                      Id = x.Id,
                      IsPayed = x.IsPayed,
                      Name = $"НХП-{x.DateTime.Year.ToString().Substring(2)}.{x.DateTime.Month.ToString("##00")}-{x.Id}",
                      BuyeerName = string.Concat(x.Buyeer.Name, x.Buyeer.Details == null ? "" : $" ({x.Buyeer.Details.INN})"),
                      DateTime = x.DateTime.ToShortDateString(),
                      Total = x.Total,
                      DownloadLink = NhpConstants.DocumentsPath.Invoice.Replace("{invoiceId}", x.Id.ToString()).Replace("~", "")
                  };
              }).ToList();

            var result = JsonConvert.SerializeObject(dtoSubscriptionPaymentBill);
            return Json(result);
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> InvoiceCreate(int organisationId, int totalInvoice, List<dtoInvoiceItems> dtoSubscriptions)
        {

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            var userId = int.Parse(subClaim.Value);

            var customUser = await UserManager.FindByIdAsync(userId);
            User user = await _userService.GetCoreUser(userId);
            var result = string.Empty;
            double billTotal = totalInvoice;

            IEnumerable<Subscription> Subscriptions = await _subscriptionService.GetSubscriptions(dtoSubscriptions.Select(x => x.Id).ToArray());
            //foreach (Subscription Subscription in Subscriptions)
            //{
            //    if (subscriptionIds.Contains(Subscription.Id))
            //    {
            //        billTotal += Subscription.Price * Subscription.MaxAvailableUsers;
            //    }
            //}

            SubscriptionPaymentBill _bill = new SubscriptionPaymentBill { BuyeerId = organisationId, DateTime = DateTime.Now.Date, Total = billTotal, UserId = userId };
            var createInvoiceCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateSubscriptionPaymentBillArgs, CreateSubscriptionPaymentBillResult>>();
            var createInvoiceResult = await createInvoiceCommand.Execute(new CreateSubscriptionPaymentBillArgs { Bill = _bill });

            createInvoiceResult.Year = _bill.DateTime.Year;
            createInvoiceResult.Month = _bill.DateTime.Month;

            var billId = createInvoiceResult.ID;
            var pos = 0;
            foreach (Subscription Subscription in Subscriptions)
            {
                pos += 1;
                SubscriptionPaymentBillEntry _billEntry = new SubscriptionPaymentBillEntry { BillId = billId, Name = $"{Subscription.ItProduct.FullName} на {dtoSubscriptions.Where(x => x.Id == Subscription.Id).Select(x => x.Duration).FirstOrDefault()}", Price = Subscription.Price, Quantity = dtoSubscriptions.Where(x => x.Id == Subscription.Id).Select(x => x.Quantity).FirstOrDefault(), SubscriptionId = Subscription.Id, Position = pos };
                var createBillEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateSubscriptionPaymentBillEntryArgs, CreateSubscriptionPaymentBillEntryResult>>();
                var createBillEntryResult = await createBillEntryCommand.Execute(new CreateSubscriptionPaymentBillEntryArgs { BillEntry = _billEntry });
            }

            string invoiceUrl = await Invoice(billId);  //$"{Request.UrlReferrer.AbsoluteUri.Substring(0, Request.UrlReferrer.AbsoluteUri.Length - Request.UrlReferrer.AbsolutePath.Length)}/Subscriptions/Invoice/{billId}";
            invoiceUrl = invoiceUrl.Replace("~", $"{Request.UrlReferrer.AbsoluteUri.Substring(0, Request.UrlReferrer.AbsoluteUri.Length - Request.UrlReferrer.AbsolutePath.Length)}");

            var messageBody = $"Вами был сформирован счет, скачать счет <a href=\"{invoiceUrl}\">здесь</a><br><br>---<br>НХП Проект в облаке<br><a href=\"https://online.nhp-soft.ru\">https://online.nhp-soft.ru</a>";
#if !DEBUG
            var message = new Message($"info@nhp-soft.ru", $"Сформирован счет в сервисе НХП Онлайн", messageBody);
            await _messageSender.SendAsync(message);
#endif

            var messageUser = new Message($"{customUser.Email}", "Сформирован счет в сервисе НХП Онлайн", messageBody);
            await _messageSender.SendAsync(messageUser);

            //await UserManager.SendEmailAsync(userId, );

            result = JsonConvert.SerializeObject(createInvoiceResult);
            return Json(result);
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> InvoiceAccept(int invoiceId)
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            var userId = int.Parse(subClaim.Value);

            var customUser = await UserManager.FindByIdAsync(userId);
            User user = await _userService.GetCoreUser(userId);
            EmptyBusinessCommandResult resultCommand = new EmptyBusinessCommandResult() { IsSuccess = true };

            SubscriptionPaymentBill subscriptionPaymentBill = await _subscriptionService.GetSubscriptionPaymentBill(invoiceId);

            foreach (SubscriptionPaymentBillEntry billEntry in subscriptionPaymentBill.BillEntry)
            {
                // EndCustomerOrganisationId и EndCustomerUserId задаю сразу, т.к. стоит FOREIGN KEY и без них не создасться
                // Получается что сначала купил для себя, а если будет отдан другому, то при активации эти поля сменятся на активатора
                SubscriptionPayment SubscriptionPayment = new SubscriptionPayment { BillId = subscriptionPaymentBill.Id, BoughtSubscriptionId = billEntry.SubscriptionId, BuyeerId = subscriptionPaymentBill.BuyeerId, BuyeerUserId = subscriptionPaymentBill.UserId, DateOfSuccesfullyPaid = DateTime.Now, IsPayed = true, Quantity = billEntry.Quantity, LicenseCode = Guid.NewGuid(), EndCustomerOrganisationId = subscriptionPaymentBill.BuyeerId, EndCustomerUserId = subscriptionPaymentBill.UserId };
                //1.Создается SubscriptionPayment в котором указывается вся необходимая информация по приобретаемой лицензии

                var createSubscriptionPaymentCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateSubscriptionPaymentArgs, CreateSubscriptionPaymentResult>>();
                var createSubscriptionPaymentResult = await createSubscriptionPaymentCommand.Execute(new CreateSubscriptionPaymentArgs { SubscriptionPayment = SubscriptionPayment });
                if (createSubscriptionPaymentResult.IsSuccess == false)
                {
                    resultCommand.IsSuccess = false;
                }
            }
            if (resultCommand.IsSuccess)
            {
                subscriptionPaymentBill.IsPayed = true;
                var editInvoiceCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditSubscriptionPaymentBillArgs, EditSubscriptionPaymentBillResult>>();
                var editInvoiceResult = await editInvoiceCommand.Execute(new EditSubscriptionPaymentBillArgs { Bill = subscriptionPaymentBill });
            }
            //2.Создается сущность BusinessContractDetails в которую вносятся актуальные реквизиты выставителя счета(наши) получаемые из OwnerDetails и реквизиты приобретателя

            //3.Создается сущность OperationDocument_SubscriptionPaymentBill  представляющая собой счет, ей в свойстве BillDetails присваивается сущность созданная на этапе 2.

            //4.Создается сущность OperationDocument_SubscriptionPaymentContract представляющая собой договор, ей в свойстве ContractDetails присваивается сущность созданная на этапе 2.

            //5.Создается сущность Operation_SubscriptionPayment ей присваивается в свойстве Payment присваивается сущность SubscriptionPayment созаднная на этапе 1.

            //6.Созданной на 5 этапе сущности Operation_SubscriptionPayment к коллекции AttachedDocuments добавляются сущности созданные на 3 и 4 этапе.

            return Json(JsonConvert.SerializeObject(resultCommand));
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> ActivateCode(int organisationId, string Code)
        {
            EmptyBusinessCommandResult result = new EmptyBusinessCommandResult { IsSuccess = true };

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            var userId = int.Parse(subClaim.Value);

            var customUser = await UserManager.FindByIdAsync(userId);
            User user = await _userService.GetCoreUser(userId);

            SubscriptionPayment SubscriptionPayment = await _subscriptionService.GetSubscriptionPayment(Code);
            if (SubscriptionPayment == null)
            {
                result.IsSuccess = false;
                result.ErrorCode = 10;
                result.ErrorMessage = "Код не найден, проверьте правильность ввода";
                return Json(JsonConvert.SerializeObject(result));
            }
            if (SubscriptionPayment.IsActivatedLicense == true)
            {
                result.IsSuccess = false;
                result.ErrorCode = 20;
                result.ErrorMessage = "Лицензия уже активирована";
                return Json(JsonConvert.SerializeObject(result));
            }
            Organisation organisation = await _organisationService.GetOrganisation(organisationId);
            if (organisation == null)
            {
                result.IsSuccess = false;
                result.ErrorCode = 30;
                result.ErrorMessage = "Указанной организации не существует";
                return Json(JsonConvert.SerializeObject(result));
            }
            SubscriptionPayment.EndCustomerOrganisationId = organisation.Id;
            SubscriptionPayment.EndCustomerUserId = userId;
            SubscriptionPayment.IsActivatedLicense = true;
            DateTime InvalidationTime = DateTime.Now;
            switch (SubscriptionPayment.BoughtSubscription.TimePeriodType)
            {
                case TimePeriodType.year:
                    InvalidationTime = DateTime.Now.AddYears(SubscriptionPayment.BoughtSubscription.Duration);
                    break;
                case TimePeriodType.month:
                    InvalidationTime = DateTime.Now.AddMonths(SubscriptionPayment.BoughtSubscription.Duration);
                    break;
                case TimePeriodType.week:
                    InvalidationTime = DateTime.Now.AddDays(SubscriptionPayment.BoughtSubscription.Duration * 7);
                    break;
                case TimePeriodType.day:
                    InvalidationTime = DateTime.Now.AddDays(SubscriptionPayment.BoughtSubscription.Duration);
                    break;
                case TimePeriodType.hour:
                    InvalidationTime = DateTime.Now.AddHours(SubscriptionPayment.BoughtSubscription.Duration);
                    break;
                default:
                    InvalidationTime = DateTime.Now;
                    break;
            }
            InvalidationTime = DateTime.Parse(InvalidationTime.ToShortDateString());
            SubscriptionPayment.InvalidationTime = InvalidationTime;
            SubscriptionPayment.DateOfSuccesfullyActivation = DateTime.Now;

            var editSubscriptionPaymentCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditSubscriptionPaymentArgs, EditSubscriptionPaymentResult>>();
            var editSubscriptionPaymentResult = await editSubscriptionPaymentCommand.Execute(new EditSubscriptionPaymentArgs { SubscriptionPayment = SubscriptionPayment });

            if (editSubscriptionPaymentResult.IsSuccess)
            {
                string LicenseUrl = await License(SubscriptionPayment.Id);  //$"{Request.UrlReferrer.AbsoluteUri.Substring(0, Request.UrlReferrer.AbsoluteUri.Length - Request.UrlReferrer.AbsolutePath.Length)}/Subscriptions/Invoice/{billId}";
                LicenseUrl = LicenseUrl.Replace("~", $"{Request.UrlReferrer.AbsoluteUri.Substring(0, Request.UrlReferrer.AbsoluteUri.Length - Request.UrlReferrer.AbsolutePath.Length)}");

                var messageBody = $"Вами была активирована лицензия, скачать сертификат <a href=\"{LicenseUrl}\">здесь</a><br><br>---<br>НХП Проект в облаке<br><a href=\"https://online.nhp-soft.ru\">https://online.nhp-soft.ru</a>";
#if !DEBUG
            var message = new Message($"info@nhp-soft.ru", $"Активирована лицензия в сервисе НХП Онлайн", messageBody);
            await _messageSender.SendAsync(message);
#endif

                var messageUser = new Message($"{customUser.Email}", "Активирована лицензия в сервисе НХП Онлайн", messageBody);
                await _messageSender.SendAsync(messageUser);
            }
            else
            {
                result.IsSuccess = editSubscriptionPaymentResult.IsSuccess;
                result.ErrorCode = editSubscriptionPaymentResult.ErrorCode;
                result.ErrorMessage = editSubscriptionPaymentResult.ErrorMessage;
            }
            //1.Создается SubscriptionPayment в котором указывается вся необходимая информация по приобретаемой лицензии

            //2.Создается сущность BusinessContractDetails в которую вносятся актуальные реквизиты выставителя счета(наши) получаемые из OwnerDetails и реквизиты приобретателя

            //3.Создается сущность OperationDocument_SubscriptionPaymentBill  представляющая собой счет, ей в свойстве BillDetails присваивается сущность созданная на этапе 2.

            //4.Создается сущность OperationDocument_SubscriptionPaymentContract представляющая собой договор, ей в свойстве ContractDetails присваивается сущность созданная на этапе 2.

            //5.Создается сущность Operation_SubscriptionPayment ей присваивается в свойстве Payment присваивается сущность SubscriptionPayment созаднная на этапе 1.

            //6.Созданной на 5 этапе сущности Operation_SubscriptionPayment к коллекции AttachedDocuments добавляются сущности созданные на 3 и 4 этапе.



            return Json(JsonConvert.SerializeObject(result));
        }

        public async Task<string> Act(int invoiceId)
        {
            return NhpConstants.DocumentsPath.Act.Replace("{invoiceId}", invoiceId.ToString());
        }

        public async Task<string> Invoice(int invoiceId)
        {
            OwnerDetails od = await _organisationService.GetOwnerDetails();
            //
            string ttf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
            var baseFont = BaseFont.CreateFont(ttf, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            var font = new Font(baseFont, 10, iTextSharp.text.Font.NORMAL);
            var fontBold = new Font(baseFont, 10, iTextSharp.text.Font.BOLD);
            var fontBoldUnderline = new Font(baseFont, 10, iTextSharp.text.Font.BOLD + iTextSharp.text.Font.UNDERLINE);
            var fontLargeBold = new Font(baseFont, 14, iTextSharp.text.Font.BOLD);
            var invoice = await _subscriptionService.GetSubscriptionPaymentBill(invoiceId);
            if (invoice == null)
            {
                return "";
            }
            var orgBuyer = invoice.Buyeer.Name;
            //Document doc = new Document(iTextSharp.text.PageSize.A4, 36, 36, 36, 36);
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Request.PhysicalApplicationPath + $"\\Content\\invoices\\{invoiceId}.pdf", FileMode.Create));
            doc.Open();
            doc.Add(new Paragraph(od.ShortName, fontBoldUnderline));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph(od.LegalAddress, fontBold));
            doc.Add(new Paragraph($" ", font));
            iTextSharp.text.Paragraph paragraph = new Paragraph($"Образец заполнения платежного поручения", fontBold);
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.SpacingAfter = 5;
            doc.Add(paragraph);

            PdfPTable table = new PdfPTable(4); table.WidthPercentage = 100; float[] widths = new float[] { 26.83f, 25.5f, 7.67f, 33f }; table.SetWidths(widths);
            PdfPCell cell = new PdfPCell(new Phrase($"ИНН {od.INN}", font)); cell.VerticalAlignment = Element.ALIGN_MIDDLE; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"КПП {od.KPP}", font)); cell.VerticalAlignment = Element.ALIGN_MIDDLE; table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Сч. №", font)); cell.Rowspan = 3; cell.HorizontalAlignment = Element.ALIGN_CENTER; cell.VerticalAlignment = Element.ALIGN_BOTTOM; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"{od.BankCorporateAccount}", font)); cell.VerticalAlignment = Element.ALIGN_BOTTOM; cell.Rowspan = 3; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"Получатель", font)); cell.VerticalAlignment = Element.ALIGN_MIDDLE; cell.Colspan = 2; cell.Border = 13; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"{od.ShortName}", font)); cell.Colspan = 2; cell.BorderColorTop = BaseColor.WHITE; cell.Border = 14; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"Банк получателя", font)); cell.VerticalAlignment = Element.ALIGN_MIDDLE; cell.Colspan = 2; cell.Border = 13; table.AddCell(cell);
            cell = new PdfPCell(new Phrase("БИК", font)); cell.VerticalAlignment = Element.ALIGN_MIDDLE; cell.HorizontalAlignment = Element.ALIGN_CENTER; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"{od.BankBIK}", font)); cell.Border = 13; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"{od.BankName}", font)); cell.Border = 14; cell.Colspan = 2; table.AddCell(cell);
            cell = new PdfPCell(new Phrase("Сч. №", font)); cell.VerticalAlignment = Element.ALIGN_BOTTOM; cell.HorizontalAlignment = Element.ALIGN_CENTER; table.AddCell(cell);
            cell = new PdfPCell(new Phrase($"{od.BankCorrespondentAccount}", font)); cell.VerticalAlignment = Element.ALIGN_BOTTOM; cell.Border = 14; table.AddCell(cell);
            doc.Add(table);
            paragraph = new Paragraph($"СЧЕТ № НХП-{invoice.DateTime.Year.ToString().Substring(2)}.{invoice.DateTime.Month.ToString("##00")}-{invoice.Id} от {invoice.DateTime.ToLongDateString()}", fontLargeBold); paragraph.Alignment = Element.ALIGN_CENTER; paragraph.SpacingBefore = 11.25f; doc.Add(paragraph);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($"Плательщик:      {orgBuyer}", font));
            doc.Add(new Paragraph($"Грузополучатель: {orgBuyer}", font));
            doc.Add(new Paragraph($" ", font));
            var positionCount = 0;
            double positionPrice = 0;
            double positionTotal = 0;
            var totalCount = 0;
            double totalSum = 0;
            PdfPTable tableInvoice = new PdfPTable(6); tableInvoice.WidthPercentage = 100; widths = new float[] { 3.5f, 43.66f, 8.67f, 7.67f, 16.5f, 16.5f }; tableInvoice.SetWidths(widths);
            PdfPCell cellI = new PdfPCell(new Phrase("№", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_MIDDLE; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase("Наименование товара", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_MIDDLE; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase("Единица изме-\r\nрения", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_MIDDLE; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase("Коли-\r\nчество", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_MIDDLE; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase("Цена", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_MIDDLE; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase("Сумма", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_MIDDLE; tableInvoice.AddCell(cellI);

            foreach (SubscriptionPaymentBillEntry Entry in invoice.BillEntry)
            {
                positionCount = Entry.Quantity;
                positionPrice = Entry.Price;
                positionTotal = positionCount * positionPrice;
                totalCount += 1;
                cellI = new PdfPCell(new Phrase(totalCount.ToString("N0"), font)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_TOP; tableInvoice.AddCell(cellI);
                cellI = new PdfPCell(new Phrase(Entry.Name, font)); cellI.VerticalAlignment = Element.ALIGN_TOP; tableInvoice.AddCell(cellI);
                cellI = new PdfPCell(new Phrase("шт", font)); cellI.HorizontalAlignment = Element.ALIGN_CENTER; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);
                cellI = new PdfPCell(new Phrase(positionCount.ToString("N0"), font)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);
                cellI = new PdfPCell(new Phrase(positionPrice.ToString("N2"), font)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);
                cellI = new PdfPCell(new Phrase(positionTotal.ToString("N2"), font)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);
                totalSum += positionTotal;
            }

            // Итого
            cellI = new PdfPCell(new Phrase("Итого:", fontBold)); cellI.Border = Rectangle.NO_BORDER; cellI.Colspan = 5; cellI.HorizontalAlignment = Element.ALIGN_RIGHT; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase($"{totalSum.ToString("N2")}", fontBold)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);
            // НДС
            cellI = new PdfPCell(new Phrase("Без налога (НДС).", fontBold)); cellI.Border = Rectangle.NO_BORDER; cellI.Colspan = 5; cellI.HorizontalAlignment = Element.ALIGN_RIGHT; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase("-", fontBold)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);
            // Всего к оплате
            cellI = new PdfPCell(new Phrase("Всего к оплате:", fontBold)); cellI.Border = Rectangle.NO_BORDER; cellI.Colspan = 5; cellI.HorizontalAlignment = Element.ALIGN_RIGHT; tableInvoice.AddCell(cellI);
            cellI = new PdfPCell(new Phrase($"{totalSum.ToString("N2")}", fontBold)); cellI.HorizontalAlignment = Element.ALIGN_RIGHT; cellI.VerticalAlignment = Element.ALIGN_BOTTOM; tableInvoice.AddCell(cellI);

            doc.Add(tableInvoice);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($"Всего наименований {totalCount}, на сумму {totalSum.ToString("N2")}", font));
            doc.Add(new Paragraph($"{ConverterDateAndMoney.CurrencyToTxt(totalSum, true)}", fontBold));
            doc.Add(new Paragraph($" ", font));
            FileStream fs = new FileStream(Request.PhysicalApplicationPath + "/Content/images/stamp.png", FileMode.Open);
            Image png = Image.GetInstance(System.Drawing.Image.FromStream(fs), System.Drawing.Imaging.ImageFormat.Png);
            png.Alignment = Image.UNDERLYING;
            png.ScalePercent(60);
            fs.Dispose();
            doc.Add(new Phrase(new Chunk(png, 126f, -90f)));
            var Director = od.DirectorString;
            var ChiefAccountant = od.ChiefAccountantString;
            try
            {
                doc.Add(new Paragraph($"Руководитель предприятия_____________________ ({Director.Split(new Char[] { ' ' })[0]} {Director.Split(new Char[] { ' ' })[1][0]}.{Director.Split(new Char[] { ' ' })[2][0]}.)", font));
            }
            catch (Exception)
            {
                doc.Add(new Paragraph($"Руководитель предприятия_____________________", font));
            }
            fs = new FileStream(Request.PhysicalApplicationPath + "/Content/images/sign.png", FileMode.Open);
            png = Image.GetInstance(System.Drawing.Image.FromStream(fs), System.Drawing.Imaging.ImageFormat.Png);
            png.ScalePercent(30);
            fs.Dispose();
            png.Alignment = Image.UNDERLYING;
            doc.Add(new Phrase(new Chunk(png, 150f, 8f)));
            doc.Add(new Paragraph($" ", font));
            try
            {
                doc.Add(new Paragraph($"Главный бухгалтер____________________________ ({ChiefAccountant.Split(new Char[] { ' ' })[0]} {ChiefAccountant.Split(new Char[] { ' ' })[1][0]}.{ChiefAccountant.Split(new Char[] { ' ' })[2][0]}.)", font));

            }
            catch (Exception)
            {
                doc.Add(new Paragraph($"Главный бухгалтер____________________________", font));
            }
            doc.Add(new Phrase(new Chunk(png, 126f, 8f)));
            doc.Add(new Paragraph($"", font));

            doc.Close();
            //return Redirect($"~/Content/invoices/{invoiceId}.pdf");
            return NhpConstants.DocumentsPath.Invoice.Replace("{invoiceId}", invoiceId.ToString());
        }

        public async Task<string> License(Guid subPaymentId)
        {
            OwnerDetails od = await _organisationService.GetOwnerDetails();
            SubscriptionPayment subscriptionPayment = await _subscriptionService.GetSubscriptionPayment(subPaymentId);
            SubscriptionPaymentBill bill = await _subscriptionService.GetSubscriptionPaymentBill(subscriptionPayment.BillId);
            //
            string ttf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
            var baseFont = BaseFont.CreateFont(ttf, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            var font = new Font(baseFont, 10, iTextSharp.text.Font.NORMAL);
            var font8 = new Font(baseFont, 8, iTextSharp.text.Font.NORMAL);
            var fontBold10 = new Font(baseFont, 10, iTextSharp.text.Font.BOLD);
            var font14 = new Font(baseFont, 14, iTextSharp.text.Font.NORMAL);
            var fontBold20 = new Font(baseFont, 20, iTextSharp.text.Font.BOLD);
            var fontBold28 = new Font(baseFont, 28, iTextSharp.text.Font.BOLD);
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Request.PhysicalApplicationPath + $"\\Content\\licenses\\{subPaymentId}.pdf", FileMode.Create));
            doc.Open();
            FileStream fs = new FileStream(Request.PhysicalApplicationPath + "/Content/images/license.png", FileMode.Open);
            Image png = Image.GetInstance(System.Drawing.Image.FromStream(fs), System.Drawing.Imaging.ImageFormat.Png);
            png.Alignment = Image.UNDERLYING;
            png.ScalePercent(90);
            fs.Dispose();
            png.SetAbsolutePosition(0f, 0f);
            doc.Add(png);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            iTextSharp.text.Paragraph paragraph = new Paragraph($"{od.FullName}", font8);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            paragraph = new Paragraph($"(г.Ставрополь, ИНН {od.INN}, ОГРН {od.OGRN})", font8);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            paragraph = new Paragraph($"ЛИЦЕНЗИЯ", fontBold28);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            paragraph = new Paragraph($"№НХП-{bill.DateTime.Year.ToString().Substring(2)}.{bill.DateTime.Month.ToString("##00")}-{bill.Id}", fontBold20);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            paragraph = new Paragraph($"действительно с {subscriptionPayment.DateOfSuccesfullyActivation.GetValueOrDefault().ToShortDateString()} по {subscriptionPayment.InvalidationTime.GetValueOrDefault().ToShortDateString()} г.", font14);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            paragraph = new Paragraph($"ВЫДАНА", font14);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            doc.Add(new Paragraph($" ", font));
            paragraph = new Paragraph($"{subscriptionPayment.EndCustomerOrganisation.Details.ShortName}", font14);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            paragraph = new Paragraph($"в том, что является Лицензиатом на ПО для ЭВМ", font14);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);

            TimePeriodType tpt = subscriptionPayment.BoughtSubscription.TimePeriodType;
            var weTPT = "";
            if (tpt == TimePeriodType.year) { weTPT = getWE(subscriptionPayment.BoughtSubscription.Duration, "год", "года", "лет"); }
            if (tpt == TimePeriodType.month) { weTPT = getWE(subscriptionPayment.BoughtSubscription.Duration, "месяц", "месяца", "месяцев"); }
            if (tpt == TimePeriodType.week) { weTPT = getWE(subscriptionPayment.BoughtSubscription.Duration, "неделя", "недели", "недель"); }
            if (tpt == TimePeriodType.hour) { weTPT = getWE(subscriptionPayment.BoughtSubscription.Duration, "час", "часа", "часов"); }
            if (tpt == TimePeriodType.day) { weTPT = getWE(subscriptionPayment.BoughtSubscription.Duration, "день", "дня", "дней"); }
            var Duration = $"{subscriptionPayment.BoughtSubscription.Duration} {weTPT}";
            var Users = $"{subscriptionPayment.Quantity} {getWE(subscriptionPayment.Quantity, "пользователь", "пользователя", "пользователей")}";
            paragraph = new Paragraph($"«{subscriptionPayment.BoughtSubscription.ItProduct.Name}, {Duration} использования, {Users}»", font14);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            fs = new FileStream(Request.PhysicalApplicationPath + "/Content/images/stamp.png", FileMode.Open);
            png = Image.GetInstance(System.Drawing.Image.FromStream(fs), System.Drawing.Imaging.ImageFormat.Png);
            png.Alignment = Image.UNDERLYING;
            png.ScalePercent(60);
            fs.Dispose();
            doc.Add(new Phrase(new Chunk(png, 226f, -90f)));
            var Director = od.DirectorString;
            var ChiefAccountant = od.ChiefAccountantString;
            try
            {
                paragraph = new Paragraph($"Генеральный директор_____________________ {Director.Split(new Char[] { ' ' })[1][0]}.{Director.Split(new Char[] { ' ' })[2][0]}. {Director.Split(new Char[] { ' ' })[0]}", font14);
                paragraph.IndentationLeft = 50;
                doc.Add(paragraph);
            }
            catch (Exception)
            {
                doc.Add(new Paragraph($"Генеральный директор_____________________", font14));
                
            }
            paragraph = new Paragraph($"{od.ShortName}", font14);
            paragraph.IndentationLeft = 50;
            doc.Add(paragraph);
            fs = new FileStream(Request.PhysicalApplicationPath + "/Content/images/sign.png", FileMode.Open);
            png = Image.GetInstance(System.Drawing.Image.FromStream(fs), System.Drawing.Imaging.ImageFormat.Png);
            png.ScalePercent(30);
            fs.Dispose();
            png.Alignment = Image.UNDERLYING;
            doc.Add(new Phrase(new Chunk(png, 250f, 32f)));
            doc.Add(new Paragraph($" ", font));
            doc.Add(new Paragraph($" ", font));
            paragraph = new Paragraph($"г.Ставрополь", font);
            paragraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(paragraph);

            doc.Close();
            return NhpConstants.DocumentsPath.License.Replace("{subPaymentId}", subPaymentId.ToString());
        }
    }
}