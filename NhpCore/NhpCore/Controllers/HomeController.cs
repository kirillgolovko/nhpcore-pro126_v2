﻿using Common.Commands;
using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NhpCore.Attributes;
using NhpCore.Common;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NhpCore.Business.Commands;
using NhpCore.Infrastructure.Utils.MessageSender;
using System.Text.RegularExpressions;

namespace NhpCore.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        private ApplicationSignInManager _appSignInManager;
        private readonly ILoggerUtility _logger;
        private readonly IUserService _userService;
        private readonly IMessageSender _messageSender;

        public HomeController(ApplicationSignInManager appSignInMngr)
        {
            if (appSignInMngr == null) throw new ArgumentNullException(nameof(appSignInMngr));

            _appSignInManager = appSignInMngr;
        }
        public HomeController(IUserService userService, IMessageSender messageSender, ILoggerUtility logger, ApplicationSignInManager appSignInMngr)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (appSignInMngr == null) throw new ArgumentNullException(nameof(appSignInMngr));
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (messageSender == null) throw new ArgumentNullException(nameof(messageSender));
            _userService = userService;
            _appSignInManager = appSignInMngr;
            _messageSender = messageSender;
            _logger = logger;
        }
        // GET: Home
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                //var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
                //if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

                //var userId = int.Parse(subClaim.Value);

                //User user = await _userService.GetCoreUser(userId);
                //if (user.IsAdmin)
                //{
                //    return RedirectToAction("Index", "Admin");
                //}
                //if (user.IsManager)
                //{
                //    return RedirectToAction("Index", "Manager");
                //}
                return RedirectToAction("Index", "Dashboard");
            }

            return View();
        }

        public ActionResult Rates()
        {
            return Redirect("http://nhp-soft.ru/rates/");
        }

        public ActionResult Help()
        {
            return Redirect("http://nhp-soft.ru/help/");
        }

        [Auth, HttpGet]
        public async Task<ActionResult> Unsubscribe(string email)
        {
            // Действия для снятия подписки на новости
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            User user = await _userService.GetCoreUser(userId);

            UserProfile userProfile = user.Profiles.Where(x => x.Email == email).FirstOrDefault();
            if (userProfile == null)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                userProfile.IsEmailExpired = false;
                userProfile.IsEmailTask = false;
                var editUserProfileCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditUserProfileArgs, EditUserProfileResult>>();
                var editUserProfileCommandResult = await editUserProfileCommand.Execute(new EditUserProfileArgs { Profile = userProfile });
                return View();
            }
        }

        [HttpGet]
        public async Task<ActionResult> NotMailSender(string idprofile)
        {
            Guid guid = Guid.Empty;
            try
            {
                guid = Guid.Parse(idprofile);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
            // Действия для снятия подписки на новости
            var profiles = await _userService.GetProfiles();

            UserProfile userProfile = profiles.Where(x => x.IdProfile == guid).FirstOrDefault();
            if (userProfile == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                userProfile.IsEmailExpired = false;
                userProfile.IsEmailTask = false;
                var editUserProfileCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditUserProfileArgs, EditUserProfileResult>>();
                var editUserProfileCommandResult = await editUserProfileCommand.Execute(new EditUserProfileArgs { Profile = userProfile });
                return View();
            }
        }

        [Auth]
        public ActionResult Test()
        {
            return View();
        }

        [Auth, HttpGet]
        public async Task<ActionResult> TestApi()
        {
            return View();
        }

        class systemDto
        {
            public string Name { get; set; }
            public string Link { get; set; }
            public string Icon { get; set; }
        }

        [HttpPost]
        public async Task<ActionResult> TestApi(int? id = null)
        {
            var token = (User as ClaimsPrincipal).FindFirst("access_token").Value;

            var client = new HttpClient();
            client.SetBearerToken(token);

            var i = id ?? 1;
            var result = await client.GetStringAsync($"http://nhpserverapi.azurewebsites.net/api/values/{i}");
            var res = JValue.Parse(result).ToString(Formatting.Indented);

            return View(new TestApi { Result = res });
        }

        public string fromHtmlSpecialChars(string text)
        {
            /*
             '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;' 
             */
            text = text.Replace("&#039;", "'");
            text = text.Replace("&quot;", "\"");
            text = text.Replace("&gt;", ">");
            text = text.Replace("&lt;", "<");
            text = text.Replace("&amp;", "&");
            return text;
        }

        [HttpPost, AjaxOnly] //получить список организаций
        public async Task<ActionResult> SendEmail(string To, string Theme, string messageBody)
        {
            EmptyBusinessCommandResult comResult = new EmptyBusinessCommandResult { IsSuccess = true };
            var message = new Message(To, Theme, fromHtmlSpecialChars(messageBody));
            try
            {
                await _messageSender.SendAsync(message);
            }
            catch (Exception ex)
            {
                comResult.IsSuccess = false;
                comResult.ErrorMessage = ex.Message;
            }
            var result = JsonConvert.SerializeObject(comResult);
            return Json(result);
        }

        [HttpPost, AjaxOnly] //получить список организаций
        public async Task<ActionResult> SendEmailMass(string To, string Theme, string messageBody)
        {
            EmptyBusinessCommandResult comResult = new EmptyBusinessCommandResult { IsSuccess = true };
            var Emails = new List<string>();
            var validEmails = new List<string>();
            //Добавляем в список из поля "Кому"
            To = To.Replace(" ", "");
            Emails.AddRange(To.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries));
            //Добавляем в список из учетных записей
            IEnumerable<CustomUser> coreUsers = await _userService.GetCoreUsers();
            string[] dtoCoreUsers = coreUsers.Where(w => w.EmailConfirmed == true).Select(x => x.Email).ToArray();
            Emails.AddRange(dtoCoreUsers);
            //Добавляем в список из профилей
            IEnumerable<UserProfile> userProfile = await _userService.GetProfiles();
            string[] dtoUserProfile = userProfile.Where(w => w.Email!=null).Select(x => x.Email).ToArray();
            Emails.AddRange(dtoUserProfile);
            //Получаем список исключений
            string emailPattern = @"^\w+([-.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
            Regex regex = new Regex(emailPattern, RegexOptions.IgnoreCase);
            IEnumerable<UnsubscribedEmails> UnsubscribedEmails = await _userService.GetUnsubscribedEmails();
            string[] dtoUnsubscribedEmails = UnsubscribedEmails.Where(w => w.Email != null).Select(x => x.Email).ToArray();
            //Валидация списка для рассылки
            foreach (string email in Emails) {
                if (regex.IsMatch(email) && !dtoUnsubscribedEmails.Contains(email) && !validEmails.Contains(email)) {
                    validEmails.Add(email);
                }
            }
            //Собсно рассылка
            var taskList = new List<Task>();
            validEmails.ForEach(email =>
            {
                var message = new Message
                {
                    Destination = email,
                    Subject = Theme,
                    Body = messageBody,

                };
                taskList.Add(_messageSender.SendAsync(message));
            });
            Task.WhenAll(taskList).GetAwaiter();
            var result = JsonConvert.SerializeObject(comResult);
            return Json(result);
        }

        [HttpPost, AjaxOnly] //получить список организаций
        public async Task<ActionResult> GetSystem()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            User user = await _userService.GetCoreUser(userId);

            List<systemDto> systemDto = new List<systemDto> { };
            systemDto.Add(new systemDto { Name = "Кабинет", Link = "/Dashboard", Icon = "fa-address-card-o" });
            if (user.IsManager) { systemDto.Add(new systemDto { Name = "Менеджер", Link = "/Manager", Icon = "fa-user-circle" }); }
            if (user.IsAdmin) { systemDto.Add(new systemDto { Name = "Админ", Link = "/Admin", Icon = "fa-gear" }); }
            var result = JsonConvert.SerializeObject(systemDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Header()
        {
            var model = new object();
            return PartialView(this.GetPartialPath(NhpConstants.Partials.Header), model);
        }

        public PartialViewResult NavigationMenu()
        {
            var model = new object();

            return PartialView(this.GetPartialPath(NhpConstants.Partials.NavigationMenu), model);
        }
    }
}