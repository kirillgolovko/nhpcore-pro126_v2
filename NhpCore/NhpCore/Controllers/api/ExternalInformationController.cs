﻿using Common.Logging;
using Common.Mapping;
using NhpCore.Attributes;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.DTOs;
using NhpCore.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NhpCore.Controllers.api
{

    public class ExternalInformationController : ApiController
    {
        private NhpCoreDbContext _db = new NhpCoreDbContext();
        private readonly ILoggerUtility _logger;
        private readonly IOrganisationService _organisationService;
        private readonly IMapperUtility _mapper;

        public ExternalInformationController(IOrganisationService organisationService, IMapperUtility mapper, ILoggerUtility logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (organisationService == null) throw new ArgumentNullException(nameof(organisationService));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));

            _organisationService = organisationService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        [Route("api/ExternalInformation/GetWebDavToken")]
        public async Task<IHttpActionResult> GetWebDavToken(int orgId)
        {
            if (orgId <= 0) return BadRequest("Id организации должен быть больше 0");


            try
            {
                var storageAccesses = await _organisationService.GetStorageAccess(orgId);

                var dbAccesses = _mapper.MapEnumerable<OrganisationDocumentsStorageAccess, DbOrganisationDocumentsStorageAccess>(storageAccesses);

                return Ok(dbAccesses);

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet, Route("api/ExternalInformation/GetSubscriptionInfo")]
        public async Task<IHttpActionResult> GetSubscriptionInfo(string clientId, int userId)
        {
            if (clientId != null && userId > 0)
            {
                try
                {
                    return Ok("success");
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
            return BadRequest();
        }
    }
}
