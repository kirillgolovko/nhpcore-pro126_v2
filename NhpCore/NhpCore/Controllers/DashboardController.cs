﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NhpCore.Infrastructure.Data.Ef;

namespace NhpCore.Controllers
{
    public class DashboardController : BaseController
    {


        public CustomUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private CustomUserManager _userManager;

        // GET: Dashboard
        public ActionResult Index()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            var userId = int.Parse(subClaim.Value);

            var customUser = UserManager.FindById(userId);
            ViewBag.UserId = userId;
            ViewBag.UserName = customUser.UserName;
            try
            {
                ViewBag.DisplayName = customUser.Claims.Where(x=>x.ClaimType=="displayname").First().ClaimValue;
            }
            catch (Exception)
            {
                ViewBag.DisplayName = "Имя не задано";
            }
            //try
            //{
            //    ViewBag.firstTab = Request.QueryString.GetValues("tab").First();
            //}
            //catch (Exception)
            //{
            //    ViewBag.firstTab = "";
            //}
            return View();
        }
    }
}