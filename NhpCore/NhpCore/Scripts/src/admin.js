﻿Vue.directive('mask', VueMask.VueMaskDirective);

$(document).ready(function () {
    var parts = document.location.href.split("#");
    getSystems();
    getOrganisations();
    $.cookie('selectedSystem', 'Admin', { expires: 30, path: '/' });
});

var vueAppState = {
    hasSystem: false,
    isOrganisationLoading: true,
    organisations: [],
    profiles: [],
    selectedOrganisation: {},
    selectedProfile: {},
    systems: []
};

var vueApp = new Vue({
    el: "#app",
    data: vueAppState,
    methods: {
        selectOrganisation: function (org) {
            this.selectedOrganisation = org;
            $.cookie('selectedOrganisation', JSON.stringify(this.selectedOrganisation), { expires: 30, path: '/' });
        }
    }
});

function getSystems() {
    $.ajax({
        url: "../Home/GetSystem",
        type: "POST",
        beforeSend: function () {
            vueAppState.systems = [];
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            for (var i = 0; i < parsedResponse.length; i++) {
                var system = parsedResponse[i];
                vueAppState.systems.push(system);
            }
            vueAppState.hasSystem = (vueAppState.systems.length > 1);
        },
        complete: function () {
        }
    });
};

function getOrganisations() {
    $.ajax({
        url: "../Admin/GetAvailable",
        type: "POST",
        beforeSend: function () {
            vueApp.organisations = [];
            vueApp.isOrganisationLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            var selectedOrganisation = $.cookie("selectedOrganisation");
            var hasSelected = false;
            if (typeof selectedOrganisation != 'undefined') {
                hasSelected = true;
                selectedOrganisation = JSON.parse(selectedOrganisation);
            }
            for (var i = 0; i < parsedResponse.length; i++) {
                var organisation = parsedResponse[i];
                vueApp.organisations.push(organisation);
                if (hasSelected && organisation.id == selectedOrganisation.id) {
                    vueApp.selectOrganisation(organisation);
                }
            }
            if (typeof vueApp.selectedOrganisation.id == 'undefined') {
                vueApp.selectOrganisation(parsedResponse[0]);
            };
        },
        complete: function () {
            vueApp.isOrganisationLoading = false;
        }
    });
};