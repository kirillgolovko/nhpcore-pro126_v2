﻿if (!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, "includes", {
        enumerable: false,
        value: function (obj) {
            var newArr = this.filter(function (el) {
                return el == obj;
            });
            return newArr.length > 0;
        }
    });
}

Vue.directive('mask', VueMask.VueMaskDirective);

var test_obj = {};

var periods = [{ duration: 12, title: "1 год" }, { duration: 24, title: "2 год" }];
var modelMembers = [];
var nodeSelected = {};
var currentTab = "";
var tabs = [
    {
        tabid: '#personalData',
        tabOrder: 1,
        tabname: 'Личные данные (профили)',
        class: 'section-personalData',
        isAccess: true,
        isVisible: true,
        isActive: false
    },
    {
        tabid: '#organisationManagement',
        tabOrder: 2,
        tabname: 'Карта организации',
        class: 'section-organisationManagement',
        isAccess: false,
        isVisible: true,
        isActive: false
    },
    {
        tabid: '#subscriptions',
        tabOrder: 4,
        tabname: 'Лицензии',
        class: 'section-subscriptions',
        isAccess: false,
        isVisible: true,
        isActive: false
    },
    {
        tabid: '#employees',
        tabOrder: 5,
        tabname: 'Пригласить сотрудников',
        class: 'section-employees',
        isAccess: false,
        isVisible: true,
        isActive: false
    },
    {
        tabid: '#organisationStructure',
        tabOrder: 6,
        tabname: 'Структура компании',
        class: 'section-organisationStructure',
        isAccess: false,
        isVisible: true,
        isActive: false
    }
];

var altTabs = $.extend(true, [], tabs);;

$(document).ready(function () {
    var parts = document.location.href.split("#");
    currentTab = "#" + parts[1];
    getSystems();
    getAvailableOrgs();
    getGeneralUserInformation();
    $.cookie('selectedSystem', 'Dashboard', { expires: 30, path: '/' });
    $('#availableInvitedUsers').on('hidden.bs.modal', function (e) {
        loadDepartment();
    });
    $('#buyLicense').on('hidden.bs.modal', function (e) {
        $('#invoiceLicense').modal('show');
    });

});

$(window).resize(function () { vueTabs.tabVisible() });


Vue.component('node', {
    template: '#node-template',
    props: {
        model: Object
    },
    data: function () {
        return {
            open: true
        }
    },
    computed: {
        isFolder: function () {
            return this.model.Nodes && this.model.Nodes.length
        }
    },
    methods: {
        toggle: function (e) {
            if (this.isFolder) {
                this.open = !this.open
            } else {
                this.selectNode(e);
            }
        },
        selectNode: function (e) {
            if (vueOrganisationStructureTabState.isDepartmentUpdating == true) { return false }
            vueOrganisationStructureTabState.selectedMember = {};
            vueOrganisationStructureTabState.selectedDepartment = this.model;
            nodeSelected = this.model;
            vueOrganisationStructureTabState.isDepartmentEditMode = true;
            vueOrganisationStructureTabState.isMemberEditMode = false;
            $('#organisationStructureTab').find('.treeView__content').find('.active').removeClass('active');
            $(e.target).addClass('active');
            modelMembers = this.model.Members;
            getAvailableInvitedUsers();
            var field = $('.departmentCardDetail');
            field.show("200", function () {
                var offset = field.offset();
                var offset_top = offset.top;
                if (offset_top < 200) { offset_top = 0 }
                $('html,body').animate({ scrollTop: offset_top }, 1000);
            });
        },
        addNode: function () {
            if (vueOrganisationStructureTabState.isDepartmentUpdating == true) { return false }
            if (this.model.Id === 0) {
                bootbox.alert(this.model.Name);
                return false
            }
            vueOrganisationStructureTabState.selectedDepartment = this.model;
            var node = this;
            var model = jQuery.extend(true, {}, node.model);
            var modelId = model.Id;
            var availableParents = jQuery.extend(true, [], model.availableParents);
            if (modelId != vueHeaderState.selectedOrganisation.id) {
                availableParents.push({
                    Id: model.Id,
                    ParentId: model.ParentId,
                    Name: model.Name,
                });
            } else {
                modelId = 0;
            }
            bootbox.prompt("Введите название подразделения", function (result) {
                if (result !== null) {
                    $.ajax({
                        url: '../Department/Create',
                        type: 'POST',
                        data: {
                            ParentId: model.Id,
                            Name: result,
                            OrganisationId: vueHeaderState.selectedOrganisation.id
                        },
                        beforeSend: function () {
                            vueOrganisationStructureTabState.isLoading = true;
                        },
                        success: function (response) {
                            var parsedResponse = JSON.parse(response);
                            //alert(JSON.stringify(parsedResponse));
                            if (parsedResponse.IsSuccess) {
                                node.open = true;
                                if (!node.isFolder) {
                                    Vue.set(node.model, 'Nodes', [])
                                }
                                node.model.Nodes.push({
                                    Id: parsedResponse.Id,
                                    availableParents: availableParents,
                                    ParentId: modelId,
                                    Name: result,
                                    Members: [],
                                    Description: ''
                                })
                            }
                        },
                        error: function () {
                            vueMessage.DangerMessage = "Не удалось добавить подразделение";
                        },
                        complete: function () {
                            vueOrganisationStructureTabState.isLoading = false;
                        }
                    });
                }
            });
        },
        deleteNode: function () {
            if (vueOrganisationStructureTabState.isDepartmentUpdating == true) { return false }
            if (this.model.Id === 0) {
                bootbox.alert(this.model.Name);
                return false
            }
            vueOrganisationStructureTabState.selectedDepartment = this.model;
            var modelId = this.model.Id;
            var node = this;
            bootbox.confirm({
                message: "Действительно удалить " + this.model.Name + "?",
                buttons: {
                    confirm: {
                        label: '<i class="fa fa-check"></i> Да',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '<i class="fa fa-times"></i> Нет',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: '../Department/Delete',
                            type: 'POST',
                            data: {
                                Id: modelId,
                                OrganisationId: vueHeaderState.selectedOrganisation.id
                            },
                            beforeSend: function () {
                                vueOrganisationStructureTabState.isLoading = true;
                            },
                            success: function (response) {
                                var parsedResponse = JSON.parse(response);
                                if (parsedResponse.IsSuccess) {
                                    //TODO: Пока не нашел более лучшего решения удаления элемента из массива
                                    getDepartments();
                                }
                            },
                            complete: function () {
                                vueOrganisationStructureTabState.isLoading = false;
                            }
                        });
                    }
                }
            });
        },
        selectMember: function (member) {
            $('#organisationStructureTab').find('.treeView__content').find('.active').removeClass('active');
            $(event.target).addClass('active');
            vueOrganisationStructureTabState.isDepartmentEditMode = false
            vueOrganisationStructureTab.selectMember(member);
        }
    }
})

//Вкладки
var vueTabState = {
    tabs: tabs,
    selectedTab: {},
    isAltMenuVisible: false,
    altTabs: altTabs
};

//Заголовок
var vueHeaderState = {

    isOrganisationLoading: true,
    organisations: [],
    selectedOrganisation: {},
    selectedProfile: {},
    systems: []
};

//Приглашения
var vueInviteTabState = {
    isVisible: true,
    users: [],
    selectedUser: {},
    emails: "",
    userLoading: false,
    isInviting: false
};

//Подписки
var vueSubscriptionTabState = {
    isVisible: true,
    selectedOrganisation: {},
    subscriptionLoading: false,
    invoiceLoading: false,
    Licenses: [],
    availableLicenses: [],
    Invoices: [],
    ActivationCode: "",
    isLicenseActivating: false,
    totalInvoiceAnimated: 0
};

//создание новых организаций, редактирование реквизитов
var vueOrganisationManagementTabState = {
    isVisible: true,
    selectedOrganisation: {},
    invitedUsers: [],
    isDetailLoading: true,
    isDetailCardPrimary: true,
    isDetailCardCodes: true,
    isDetailCardContacts: true,
    isDetailCardHead: true,
    isHeadDirectorString: true,
    isChiefAccountantString: true,
    isDetailCardBank: true,
    isDetailCardTechival: true,
    isDetailCardTechivalVisible: false,
    yandexStorageLink: '',
    isYandexStorageConnected: false
};

//создание структуры организаций, редактирование подразделений
var vueOrganisationStructureTabState = {
    isVisible: true,
    depLoading: false,
    selectedOrganisation: {},
    selectedDepartment: {},
    selectedMember: {},
    departments: {
        Id: 0,
        ParentId: null,
        Name: 'Выберите организацию',
        Description: '',
        Members: [],
        Nodes: []
    },
    invitedUsers: [],
    availableInvitedUsers: [],
    isDepartmentEditMode: false,
    isDepartmentUpdating: false,
    isMemberEditMode: false,
    isMemberUpdating: false
};

// редактирование личных данных
var vueProfileDataTabState = {
    isVisible: true,
    organisations: [],
    profiles: [],
    selectedProfile: {},
    isProfileLoading: false,
    isProfileCreateMode: false,
    isProfileEditMode: false,
    isProfileList: true,
    isProfileOrgList: true,
    isProfileCardPrimary: true,
    isProfileCardPassport: true,
    isProfileCardBuh: true,
    isProfileCardContacts: true,
    isProfileCardAdditional: true,
    isProfileCardNotice: true
};

var vueHeader = new Vue({
    el: "#header",
    data: vueHeaderState,
    methods: {
        createNewOrganisation: function () {
            bootbox.prompt("Введите название организации", function (result) {
                if (result !== null) {
                    $.ajax({
                        url: '../Organisation/CreateOrganisation',
                        type: 'POST',
                        data: {
                            name: result
                        },
                        beforeSend: function () {
                            vueHeaderState.isOrganisationLoading = true;
                        },
                        success: function (response) {
                            getAvailableOrgs();
                        },
                        error: function () {
                            vueHeaderState.isOrganisationLoading = false;
                            vueMessage.DangerMessage = "Не удалось создать организацию";
                        },
                        complete: function () {
                        }
                    });
                }
            });
        },
        showAccount: function () {
            var tabs = vueTabState.tabs;
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i].tabid == "#personalData") {
                    vueTabs.tabSwitch(tabs[i]);
                    break;
                }
            }
        },
        selectOrganisation: function (org) {
            this.selectedOrganisation = org;
            vueOrganisationStructureTabState.selectedOrganisation = org;
            vueOrganisationManagementTabState.selectedOrganisation = org;
            vueSubscriptionTabState.selectedOrganisation = org;
            $.cookie('selectedOrganisation', JSON.stringify(this.selectedOrganisation), { expires: 30, path: '/' });
            getTabAccess(); // Устанавливаем доступы к вкладкам согласно роли юзера
            loadInvitetedUsersInfo(); // Грузим, если вкладка Пользователей доступна
            getOrganisationInfo();
            getDepartments(); // Грузим, если вкладка Структуры подразделений доступна
            getLicenses(); // Грузим, если вкладка Лицензий доступна
            getAvailableLicenses(); // Грузим, если вкладка Лицензий доступна
            getInvoices();
        }
    },
    watch: {
        selectedOrganisation: function (selectedOrg) {
            vueOrganisationManagementTabState.yandexStorageLink = 'https://oauth.yandex.ru/authorize?response_type=code&client_id=845248035b2642b6ad1b12c14e2f90bb&state=';
            vueOrganisationManagementTabState.yandexStorageLink += uid + 'o' + selectedOrg.id;
        }
    },
    computed: {

    }
});

var vueTabs = new Vue({
    el: "#myCompanyTabs",
    data: vueTabState,
    created: function () {
        for (var i = 0; i < this.tabs.length; i++) {
            this.tabs[i].isAccess = false;
            //this.tabs[i].isVisible = false;
        }
    },
    methods: {
        tabVisible: function () {
            var panelWidth = $('#myCompanyTabs').width() - 28;
            var tabs = $('#myCompanyTabs').find('.tab-content-nav-item');
            var leftWidth = 0;
            this.isAltMenuVisible = false;
            for (var i = 0; i < tabs.length; i++) {
                if ((leftWidth + $(tabs[i]).width() + 28) < panelWidth) {
                    this.tabs[i].isVisible = true;
                } else {
                    this.tabs[i].isVisible = false;
                    this.isAltMenuVisible = true;

                }
                vueTabState.altTabs[i].isAccess = this.tabs[i].isAccess;
                vueTabState.altTabs[i].isVisible = !this.tabs[i].isVisible;
                leftWidth += $(tabs[i]).width() + 28;
            }
        },
        tabSwitch: function (tab) {
            if (this.selectedTab.class != tab.class) {
                this.selectedTab = tab;
                for (var i = 0; i < this.tabs.length; i++) {
                    if (this.tabs[i].class == this.selectedTab.class) {
                        $('.' + this.tabs[i].class).addClass('display');
                        $('.' + this.tabs[i].class).addClass('active');
                        this.tabs[i].isActive = true;
                        currentTab = this.tabs[i].tabid;
                    } else {
                        $('.' + this.tabs[i].class).removeClass('active');
                        $('.' + this.tabs[i].class).removeClass('display');
                        this.tabs[i].isActive = false;
                    }
                }
            }
        }
    },
    computed: {
        sortedTab() {
            return this.tabs.sort(function (d1, d2) { return d1.tabOrder > d2.tabOrder });
        },
        sortedAltTab() {
            return this.altTabs.sort(function (d1, d2) { return d1.tabOrder > d2.tabOrder });
        }
    }
});

var vueMessage = new Vue({
    el: "#message",
    data: {
        SuccessMessage: "",
        DangerMessage: "",
        WarningMessage: "",
        InfoMessage: ""
    },
    methods: {
        closeSuccess: function () {
            this.SuccessMessage = "";
        },
        closeDanger: function () {
            this.DangerMessage = "";
        },
        closeWarning: function () {
            this.WarningMessage = "";
        },
        closeInfo: function () {
            this.InfoMessage = "";
        }
    },
    computed: {
        isSuccessMessage: function () {
            return this.SuccessMessage.length > 0;
        },
        isDangerMessage: function () {
            return this.DangerMessage.length > 0;
        },
        isWarningMessage: function () {
            return this.WarningMessage.length > 0;
        },
        isInfoMessage: function () {
            return this.InfoMessage.length > 0;
        }
    },
    watch: {
        SuccessMessage: function (newSuccessMessage) {
            if (newSuccessMessage.length > 0) {
                setTimeout(function () {
                    vueMessage.closeSuccess();
                }, 3000);
            }
        }
    }
});

var vueInviteTab = new Vue({
    el: "#inviteTab",
    data: vueInviteTabState,
    methods: {
        selectUser: function (user) {
            this.selectedUser = user;
        },
        invite: function () {

            // null если одна строка
            // [] если есть разрывы
            var isMultiline = /\r|\n/.exec(this.emails);

            var splitted = this.emails.split(/(\r\n|\n|\r)/gm);

            var filtered = splitted.filter(function (value) {
                var filterResult = value !== '\n' && value !== '\r' && value !== '\r\n';
                return filterResult;
            });

            var emailsArray = isMultiline === null ? [this.emails] : filtered;

            $.ajax({
                url: '../Organisation/InviteUsers',
                type: 'POST',
                dataType: 'json',
                traditional: true,
                timeout: 10000,
                data: {
                    organisationId: vueHeaderState.selectedOrganisation.id,
                    emails: emailsArray /*передать массив имейл адресов*/
                },
                beforeSend: function () { /*запустить анимацию*/
                    $('#inviteUsers').modal('hide');
                    vueInviteTabState.isInviting = true;
                },
                success: function (response) {
                    /* уведомление об успешном добавлении*/
                    var parsedResponse = JSON.parse(response);
                    var invalidEmails = parsedResponse.invalidEmails;
                    var failedEmails = parsedResponse.inviteResult.FailedEmails;
                    //TODO: show window with failed and invalid emails
                },
                error: function (x, t, e) {

                },
                complete: function () {
                    loadInvitetedUsersInfo();
                    vueInviteTabState.isInviting = false;
                }
            });
        },
        validateEmails: function () {
            // regexp validation
            /* something like that = .{0,}@\w{0,}\.\w{0,} = */
        },
        remove: function (user) {
            user.isDeleting = true;

            $.ajax({
                url: '../Organisation/DeleteInvitedUser',
                type: 'POST',
                data: {
                    id: user.id
                },
                beforeSend: function () { /*запустить  анимацию об удалении*/ },
                success: function (response) {
                    var parsedResponse = JSON.parse(response);

                    // удалить из приглашений
                    var findedUser = vueInviteTabState.users.filter(function (user) {
                        var expression = user.id === parsedResponse.DeletedId;
                        return expression;
                    });
                    if (findedUser.length === 1) {
                        var delUser = findedUser[0];

                        var index = vueInviteTabState.users.indexOf(delUser);
                        vueInviteTabState.users.splice(index, 1);

                        if (delUser.status === true) {
                            // remove from subscribe tab (удалить из стейта подписок, обновить суммарную цену)
                            var findedSubscrUser = vueSubscriptionTabState.users.filter(function (el) {
                                return el.id === user.userId;
                            });

                            if (findedSubscrUser.length === 1) {
                                var subUser = findedSubscrUser[0];
                                var indexInSubscr = vueSubscriptionTabState.users.indexOf(subUser);
                                vueSubscriptionTabState.splice(indexInSubscr, 1);
                            }
                        }
                    }
                },
                error: function () {

                },
                complete: function () {
                    user.isDeleting = false;
                }
            });
        }
    },
    watch: {
        selectedOrganisation: function (selectedOrg) {
            loadInvitetedUsersInfo(selectedOrg.id);
        }
    }
});

var vueSubscriptionTab = new Vue({
    el: "#subscriptionTab",
    data: vueSubscriptionTabState,
    methods: {
        getPayDocument: function () {
            $('#buyLicense').modal('hide');
        },
        setInvoice: function () {
            vueOrganisationManagementTab.saveOrgDetailCard();
            $('#invoiceLicense').modal('hide');
            var subscriptions = [];
            for (var i = 0; i < this.availableLicenses.length; i++) {
                var item = this.availableLicenses[i];
                if (item.isSelected) {
                    var subscriptionsItem = {
                        Id: item.SelectedSubscription.Id,
                        Price: item.Price,
                        Quantity: item.Quantity,
                        Duration: item.SelectedSubscription.Duration
                    };
                    subscriptions.push(subscriptionsItem);
                }
            }
            $.ajax({
                url: '../Subscriptions/InvoiceCreate',
                type: 'POST',
                data: {
                    organisationId: this.selectedOrganisation.id,
                    totalInvoice: this.totalInvoice,
                    dtoSubscriptions: subscriptions,
                },
                beforeSend: function () { },
                success: function () {
                    getInvoices();
                }
            });
        },
        activateLicense: function () {
            if (vueSubscriptionTabState.ActivationCode.length == 0) { return false }
            $.ajax({
                url: '../Subscriptions/ActivateCode',
                type: 'POST',
                data: {
                    organisationId: vueHeaderState.selectedOrganisation.id,
                    code: vueSubscriptionTabState.ActivationCode
                },
                beforeSend: function () {
                    vueSubscriptionTabState.isLicenseActivating = true;
                },
                success: function (response) {
                    $('#activateLicense').modal('hide');
                    var parsedResponse = JSON.parse(response);
                    if (parsedResponse.IsSuccess == true) {
                        vueMessage.SuccessMessage = "Лицензия успешно активирована";
                        getLicenses();
                    }
                    else {
                        vueMessage.DangerMessage = parsedResponse.ErrorMessage;
                    }
                },
                error: function () {
                    vueMessage.DangerMessage = "Не удалось активировать лицензию"
                },
                complete: function () {
                    $('#activateLicense').modal('hide');
                    vueSubscriptionTabState.isLicenseActivating = false;
                }
            });
        },
        pay: function (event) {
            var userIds = [];
            for (var i = 0; i < this.users.length; i++) {
                if (this.users[i].isSelected) {
                    userIds.push(this.users[i].userId);
                }
            }
            $.ajax({
                url: '../Subscriptions/Pay',
                type: 'POST',
                data: {
                    organisationId: this.selectedOrganisation.id,
                    subscriptionId: this.selectedSubscription.id,
                    duration: this.selectedSubscriptionPeriod.duration,
                    users: userIds
                },
                beforeSend: function () { },
                success: function () {
                }
            });
        }

    },
    computed: {
        subscrRetailPrice: function () {
            var result = "";

            // расчет стоимости 1 подписки на выбранный период
            var price = this.selectedSubscriptionPeriod.duration * this.selectedSubscription.price;
            var isNan = Number.isNaN(price);

            result = isNan === true ? "" : Math.ceil(price);

            return result;
        },
        totalInvoice: function () {
            var result = 0;

            for (var i = 0; i < this.availableLicenses.length; i++) {
                var license = this.availableLicenses[i];
                if (license.isSelected) {
                    if (license.Quantity < 5) { license.Quantity = 2 } else { license.Quantity = Math.ceil(license.Quantity / 5) * 5 }
                    license.Total = license.SelectedSubscription.Price * license.Quantity;
                    result += license.Total;
                }
            }
            oldTotalInvoice = result;
            return result;
        },
        isOrganisationLoading: function () {
            return vueHeaderState.isOrganisationLoading;
        }
    },
    watch: {
        totalInvoice: function (newTotal, oldTotal) {
            var vm = this;
            function animate() {
                if (TWEEN.update()) {
                    requestAnimationFrame(animate)
                }
            };

            new TWEEN.Tween({ tweeningNumber: oldTotal })
                .easing(TWEEN.Easing.Quadratic.Out)
                .to({ tweeningNumber: newTotal }, 500)
                .onUpdate(function () {
                    vm.totalInvoiceAnimated = this.tweeningNumber.toFixed(0)
                })
                .start();

            animate();

            //$(".spincrement").spincrement({
            //    from: oldTotal,                // Стартовое число
            //    to: newTotal,              // Итоговое число. Если false, то число будет браться из элемента с классом spincrement, также сюда можно напрямую прописать число. При этом оно может быть, как целым, так и с плавающей запятой
            //    decimalPlaces: 0,       // Сколько знаков оставлять после запятой
            //    decimalPoint: ".",      // Разделитель десятичной части числа
            //    thousandSeparator: " ", // Разделитель тыcячных
            //    duration: 1000          // Продолжительность анимации в миллисекундах
            //});
        }
    }
});

var vueOrganisationManagementTab = new Vue({
    el: '#organisationManagementTab',
    data: vueOrganisationManagementTabState,
    methods: {
        saveOrgDetailCard: function () {
            var valid = validForm(this.selectedOrganisation.Details, 'orgDetailCard', 'right');
            if (valid.isSuccess == false) { return false; }
            $.ajax({
                url: '../Organisation/EditOrganisationDetails',
                type: 'POST',
                data: {
                    model: vueOrganisationManagementTabState.selectedOrganisation.Details
                },
                beforeSend: function () {
                    vueOrganisationManagementTabState.isDetailLoading = true;
                },
                success: function (response) {
                    vueMessage.SuccessMessage = "Данные организации успешно обновлены";
                },
                error: function () {
                    vueMessage.DangerMessage = "Не удалось обновить данные организации"
                },
                complete: function () {
                    vueOrganisationManagementTabState.isDetailLoading = false;
                }
            });
        },
        saveOrgStorageAccessCard: function () {
            $.ajax({
                url: '../Organisation/EditOrganisationStorageAccess',
                type: 'POST',
                data: {
                    dto: vueOrganisationManagementTabState.selectedOrganisation.StorageAccess
                },
                beforeSend: function () {
                    vueOrganisationManagementTabState.isStorageAccessLoading = true;
                },
                success: function (response) {
                    parsedResponse = JSON.parse(response);
                    vueOrganisationManagementTabState.selectedOrganisation.StorageAccess.Id = parsedResponse.Id;
                    vueMessage.SuccessMessage = "Сервер WEBDav организации успешно сохранены";
                },
                error: function () {
                    vueMessage.DangerMessage = "Не удалось сохранить сервер WEBDav организации"
                },
                complete: function () {
                    vueOrganisationManagementTabState.isStorageAccessLoading = false;
                }
            });
        },
        changeDirector: function (e) {
            this.selectedOrganisation.Details.DirectorString = e.target[e.target.selectedIndex].text;
        },
        changeChiefAccountant: function (e) {
            this.selectedOrganisation.Details.ChiefAccountantString = e.target[e.target.selectedIndex].text
        },
    }
});

var vueOrganisationStructureTab = new Vue({
    el: '#organisationStructureTab',
    data: vueOrganisationStructureTabState,
    methods: {
        getAvailable: function () {
            getDepartments();
        },
        saveDepartment: function () {
            var department = vueOrganisationStructureTabState.selectedDepartment;
            $.ajax({
                url: '../Department/Edit',
                type: 'POST',
                data: department,
                beforeSend: function () {
                    vueOrganisationStructureTabState.depLoading = true;
                    vueOrganisationStructureTabState.isDepartmentEditMode = false;
                    vueOrganisationStructureTabState.isMemberEditMode = false;
                },
                success: function (response) {
                    var parsedResponse = JSON.parse(response);
                    if (parsedResponse.IsSuccess) {
                        vueOrganisationStructureTabState.isDepartmentEditMode = false;
                        getDepartments();
                    } else {
                        vueOrganisationStructureTabState.isDepartmentEditMode = true;
                    }

                },
                complete: function () {
                    vueOrganisationStructureTabState.depLoading = false;
                }
            });
        },
        selectMember: function (member) {
            vueOrganisationStructureTabState.selectedMember = member;
            if (vueOrganisationStructureTabState.isDepartmentEditMode) {

            } else {
                vueOrganisationStructureTabState.selectedDepartment = {};
                vueOrganisationStructureTabState.isDepartmentEditMode = false;
                vueOrganisationStructureTabState.isMemberEditMode = true;
                var field = $('.memberCardDetail');
                field.show("200", function () {
                    var offset = field.offset();
                    var offset_top = offset.top;
                    if (offset_top < 200) { offset_top = 0 }
                    $('html,body').animate({ scrollTop: offset_top }, 1000);
                });
            }
        },
        addMember: function (member) {
            var userId = member.userId == null ? 0 : member.userId;
            $.ajax({
                url: '../Department/AddMember',
                type: 'POST',
                data: {
                    userId: userId,
                    departmentId: vueOrganisationStructureTabState.selectedDepartment.Id
                },
                beforeSend: function () {
                    member.isDeleting = true;
                },
                success: function (response) {
                    //$('#availableInvitedUsers').modal('hide');
                    var findedUser = vueOrganisationStructureTabState.availableInvitedUsers.filter(function (user) {
                        var expression = user.userId === userId;
                        return expression;
                    });
                    if (findedUser.length === 1) {
                        var delUser = findedUser[0];

                        var index = vueOrganisationStructureTabState.availableInvitedUsers.indexOf(delUser);
                        vueOrganisationStructureTabState.availableInvitedUsers.splice(index, 1);
                    }
                    //getDepartments();
                },
                error: function () {
                    vueMessage.DangerMessage = "Не удалось добавить сотрудника";
                },
                complete: function () {
                }
            });
        },
        deleteMember: function () {
            if (Object.keys(vueOrganisationStructureTabState.selectedMember).length == 0) { return false }
            bootbox.confirm({
                message: "Действительно убрать " + vueOrganisationStructureTabState.selectedMember.Name + "?",
                buttons: {
                    confirm: {
                        label: '<i class="fa fa-check"></i> Да',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '<i class="fa fa-times"></i> Нет',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var selectedMember = vueOrganisationStructureTabState.selectedMember;
                        $.ajax({
                            url: '../Department/DeleteMember',
                            type: 'POST',
                            data: {
                                UserId: vueOrganisationStructureTabState.selectedMember.Id,
                                DepartmentId: vueOrganisationStructureTabState.selectedDepartment.Id
                            },
                            beforeSend: function () {
                                vueOrganisationStructureTabState.isDepartmentUpdating = true;
                                selectedMember.isDeleting = true;
                            },
                            success: function (response) {
                                var findedMember = vueOrganisationStructureTabState.selectedDepartment.Members.filter(function (member) {
                                    var expression = member.Id === selectedMember.Id;
                                    return expression;
                                });
                                if (findedMember.length === 1) {
                                    var delMember = findedMember[0];
                                    var index = vueOrganisationStructureTabState.selectedDepartment.Members.indexOf(delMember);
                                    vueOrganisationStructureTabState.selectedDepartment.Members.splice(index, 1);
                                    vueOrganisationStructureTabState.selectedMember = {};
                                }
                                getAvailableInvitedUsers();
                            },
                            error: function () {
                                vueMessage.DangerMessage = "Не удалось убрать сотрудника";
                            },
                            complete: function () {
                                vueOrganisationStructureTabState.isDepartmentUpdating = false;
                            }
                        });
                    }
                }
            });

        },
        saveMember: function () {
            if (Object.keys(vueOrganisationStructureTabState.selectedMember).length == 0) { return false }
            var member = this.selectedMember;
            member.Salary = member.Salary + '';
            member.Salary = member.Salary.replace(".", ",");
            //TODO: Нужна форма выбора приглашенных сотрудников
            $.ajax({
                url: '../Department/EditMember',
                type: 'POST',
                data: {
                    organisationId: vueHeaderState.selectedOrganisation.id,
                    member: member
                },
                beforeSend: function () {
                    vueOrganisationStructureTabState.isMemberUpdating = true;
                },
                success: function (response) {
                    vueMessage.SuccessMessage = "Сотрудник успешно сохранен";
                    loadMember();
                },
                error: function () {
                    vueMessage.DangerMessage = "Ошибка сохранения";
                },
                complete: function () {
                    member.Salary = member.Salary.replace(",", ".");
                    vueOrganisationStructureTabState.isMemberUpdating = false;
                }
            });
        },
    }
});

var vuePersonalDataTab = new Vue({
    el: '#personalDataTab',
    data: vueProfileDataTabState,
    methods: {
        selectProfile: function (profile) {
            var copiedProfile = jQuery.extend(true, {}, profile);
            this.selectedProfile = copiedProfile;
            this.isProfileEditMode = true;
            this.isProfileCreateMode = false;
            $('.profileCard').show("200");
        },
        createProfile: function () {
            this.selectedProfile = {};
            this.isProfileCreateMode = true;
            this.isProfileEditMode = false;
        },
        deleteProfile: function (e) {
            //Удаление профиля с проверкой
            if (this.isProfileEditMode == false) { return false }
            var copiedProfile = $.extend(true, {}, this.selectedProfile);
            bootbox.confirm({
                message: "Вы действительно хотите удалить профиль?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Нет',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Да',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: '../UserProfile/DeleteProfile',
                            type: 'POST',
                            data: copiedProfile,
                            beforeSend: function () {
                                this.isGeneralLoading = true;
                            },
                            success: function (response) {
                                var parsedResponse = JSON.parse(response);
                                if (parsedResponse.IsSuccess == true) {
                                    getGeneralUserInformation();
                                    vueMessage.SuccessMessage = "Профиль удален";
                                } else {
                                    vueMessage.DangerMessage = "Не удалось удалить профиль, возможно он связан с одной из организаций";
                                };
                            },
                            complete: function () {
                                this.isGeneralLoading = false;
                            }
                        });
                    };
                }
            });
        },
        saveProfile: function () {
            this.selectedProfile.SNP = this.selectedProfile.SecondName + ' ' + this.selectedProfile.FirstName + ' ' + this.selectedProfile.PatronymicName;
            var valid = validForm(this.selectedProfile, 'profileCard', 'left');
            if (valid.isSuccess == false) { return false }
            var copiedProfile = $.extend(true, {}, this.selectedProfile);
            if (this.isProfileCreateMode) {
                //Создание профиля в базе
                $.ajax({
                    url: '../UserProfile/CreateProfile',
                    type: 'POST',
                    data: copiedProfile,
                    beforeSend: function () {
                        this.isGeneralLoading = true;
                    },
                    success: function (response) {
                        var parsedResponse = JSON.parse(response);
                        if (parsedResponse.IsSuccess == true) {
                            getGeneralUserInformation();
                            vueMessage.SuccessMessage = "Профиль создан";
                        } else {
                            vueMessage.DangerMessage = "Не удалось создать профиль";
                        };
                    },
                    complete: function () {
                        this.isGeneralLoading = false;
                    }
                });

                this.isProfileCreateMode = false;
            }
            if (this.isProfileEditMode) {
                //Редактирование профиля в базе
                $.ajax({
                    url: '../UserProfile/EditProfile',
                    type: 'POST',
                    data: copiedProfile,
                    beforeSend: function () {
                        this.isGeneralLoading = true;
                    },
                    success: function (response) {
                        var parsedResponse = JSON.parse(response);
                        if (parsedResponse.IsSuccess == true) {
                            getGeneralUserInformation();
                            vueMessage.SuccessMessage = "Профиль сохранен";
                        } else {
                            vueMessage.DangerMessage = "Не удалось сохранить профиль";
                        };
                    },
                    complete: function () {
                        this.isGeneralLoading = false;
                    }
                });
                this.isProfileEditMode = false;
            }
        },
        changeOrgProfile: function (org) {
            $.ajax({
                url: '../UserProfile/ChangeProfileId',
                type: 'POST',
                data: { 'Org': org.id, 'Profile': org.Profile.Id },
                beforeSend: function () {
                    this.isGeneralLoading = true;
                },
                success: function (response) {
                    vueMessage.SuccessMessage = "Профиль компании успешно сменен";
                    getGeneralUserInformation();
                },
                error: function () {
                    vueMessage.DangerMessage = "Не удалось задать профиль к компании";
                },
                complete: function () {
                    this.isGeneralLoading = false;
                }
            });
        },
        generalInfoClick: function () {

        },
        selectOrganisation: function (organisation) {

        }
    }
});

function loadSubscriptionUserInfo(organisationId, subscriptionId) {
    if (organisationId !== '' && organisationId !== null && subscriptionId !== '' && subscriptionId !== null) {
        $.ajax({
            url: '../Subscriptions/GetUsersInfo',
            type: 'GET',
            data: {
                organisationId: organisationId,
                subscriptionId: subscriptionId
            },
            beforeSend: function () {
                vueSubscriptionTabState.subscrUsersLoading = true;
            },
            success: function (response) {
                var parsedResponse = JSON.parse(response);
                vueSubscriptionTabState.users = [];

                for (var i = 0; i < parsedResponse.length; i++) {
                    // push objects in collection
                    //  vueSubscriptionTabState.users.push(parsedResponse[i]);
                    var user = parsedResponse[i];
                    user.isSelected = false;
                    vueSubscriptionTabState.users.push(user);
                }
            },
            complete: function () {
                vueSubscriptionTabState.subscrUsersLoading = false;
            }
        });
    }
};

function loadInvitetedUsersInfo() {
    if (vueTabState.tabs[1].isAccess == true) {
        $.ajax({
            url: '../Organisation/GetInvitedUsers',
            type: 'GET',
            data: {
                organisationId: vueHeaderState.selectedOrganisation.id
            },
            beforeSend: function () {
                vueInviteTabState.userLoading = true;
            },
            success: function (response) {
                // проверить реализацию
                var parsedResponse = JSON.parse(response);
                // чистим массив приглашенных пользователей
                vueInviteTabState.users = [];
                vueOrganisationStructureTabState.invitedUsers = [];
                vueOrganisationManagementTabState.invitedUsers = [];
                for (var i = 0; i < parsedResponse.length; i++) {
                    var user = parsedResponse[i];
                    user.isDeleting = false;
                    vueInviteTabState.users.push(user);
                    //заполняем приглашенных
                    //user.status = true; // всех принимаем
                    if (user.status == true) {
                        vueOrganisationStructureTabState.invitedUsers.push(user);
                        vueOrganisationManagementTabState.invitedUsers.push(user);
                    }

                }
            },
            complete: function () {
                vueInviteTabState.userLoading = false;
            }
        });
    }
};

function getSystems() {
    $.ajax({
        url: "../Home/GetSystem",
        type: "POST",
        beforeSend: function () {
            vueHeaderState.systems = [];
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            for (var i = 0; i < parsedResponse.length; i++) {
                var system = parsedResponse[i];
                vueHeaderState.systems.push(system);
            }
            vueHeaderState.hasSystem = (vueHeaderState.systems.length > 1);
        },
        complete: function () {
        }
    });
};

function getAvailableOrgs() {
    $.ajax({
        url: "../Organisation/GetAvailable",
        type: "GET",
        beforeSend: function () {
            vueHeaderState.organisations = [];
            vueHeaderState.isOrganisationLoading = true;
            vueOrganisationManagementTabState.isDetailLoading = true;
            vueProfileDataTabState.organisations = [];
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            var selectedOrganisation = $.cookie("selectedOrganisation");
            var hasSelected = false;
            if (typeof selectedOrganisation != 'undefined') {
                hasSelected = true;
                selectedOrganisation = JSON.parse(selectedOrganisation);
            }
            for (var i = 0; i < parsedResponse.length; i++) {
                var organisation = parsedResponse[i];
                vueHeaderState.organisations.push(organisation);
                if (hasSelected && organisation.id == selectedOrganisation.id) {
                    vueHeader.selectOrganisation(organisation);
                }
                vueProfileDataTabState.organisations.push(organisation);
            }
            if (typeof vueHeaderState.selectedOrganisation.id == 'undefined') {
                vueHeader.selectOrganisation(parsedResponse[0]);
            };
        },
        complete: function () {
            vueHeaderState.isOrganisationLoading = false;
            vueOrganisationManagementTabState.isDetailLoading = false;
        }
    });
};

function getSubscriptions() {
    $.ajax({
        url: "../Subscriptions/GetAvailableSubscriptions",
        type: "GET",
        beforeSend: function () {
            vueSubscriptionTabState.subscrLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);

            for (var i = 0; i < parsedResponse.length; i++) {
                // push objects in collection
                vueSubscriptionTabState.subscriptions.push(parsedResponse[i]);
            }

        },
        complete: function () {
            vueSubscriptionTabState.subscrLoading = false;
        }
    });
};

function getGeneralUserInformation() {
    // Получаем список доступных профилей пользователя
    $.ajax({
        url: "../UserProfile/GetAvailableProfiles",
        type: "GET",
        beforeSend: function () {
            vueProfileDataTabState.profiles = [];
            vueProfileDataTabState.selectedProfile = {};
            vueProfileDataTabState.isGeneralLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            for (var i = 0; i < parsedResponse.length; i++) {
                var userProfile = parsedResponse[i];
                if (userProfile.DisplayName.length == 0) {
                    userProfile.DisplayName = 'Текущий-' + userProfile.Id
                }
                vueProfileDataTabState.profiles.push(userProfile);
            }
        },
        complete: function () {
            vueProfileDataTabState.isGeneralLoading = false;
        }
    });

};

function getTabAccess() {
    var tabs = vueTabState.tabs;
    //var userRoles = vueHeaderState.selectedOrganisation.userRoles;
    //var userRoleId = vueHeaderState.selectedOrganisation.userRoles[0].Id;
    //for (var i = 0; i < userRoles.length; i++) {
    //    userRoleId = vueHeaderState.selectedOrganisation.userRoles[i].Id < userRoleId ? vueHeaderState.selectedOrganisation.userRoles[i] : userRoleId;
    //}
    tabs[0].isAccess = hasUserRole([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
    tabs[1].isAccess = hasUserRole([1, 2, 5, 10, 11]);
    vueOrganisationManagementTabState.isDetailCardTechivalVisible = hasUserRole([1]);
    tabs[2].isAccess = hasUserRole([1, 2, 5, 10, 11]);
    tabs[3].isAccess = hasUserRole([1, 2, 5, 10]);
    tabs[4].isAccess = hasUserRole([1, 2, 5, 10, 11]);
    if (currentTab !== '#undefined') {
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i].tabid === currentTab) {
                vueTabs.tabSwitch(tabs[i]);
            }
        }
    } else {
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i].isAccess == true) {
                vueTabs.tabSwitch(tabs[i]);
                break;
            }
        }
    }
};

function getDepartments() {
    if (vueTabState.tabs[4].isAccess == true) {
        $.ajax({
            url: '../Department/GetAvailable',
            type: 'GET',
            data: {
                organisationId: vueHeaderState.selectedOrganisation.id
            },
            beforeSend: function () {
                vueOrganisationStructureTabState.depLoading = true;
                vueOrganisationStructureTabState.isDepartmentEditMode = false;
                vueOrganisationStructureTabState.isMemberEditMode = false;
            },
            success: function (response) {
                var parsedResponse = JSON.parse(response);
                vueOrganisationStructureTabState.departments = parsedResponse;
            },
            complete: function () {
                vueOrganisationStructureTabState.depLoading = false;
                vueOrganisationStructureTabState.selectedDepartment = {};
                vueOrganisationStructureTabState.selectedMember = {};
            }
        });
    }
};

function hasUserRole(ids) {
    var userRoles = vueHeaderState.selectedOrganisation.userRoles;
    for (var i = 0; i < userRoles.length; i++) {
        if (ids.includes(userRoles[i].Id)) {
            return true;
        }
    }
    return false;
};

function validForm(object, type, placement) {
    var result = {
        isSuccess: true,
        invalid: []
    };
    if (type == 'orgDetailCard') {
        if (object.ShortName == null || object.ShortName.length == 0) {
            result.isSuccess = false;
            result.invalid.push('ShortName');
        }
        if (object.INN == null || object.INN.length == 0) {
            result.isSuccess = false;
            result.invalid.push('INN');
        }
        if (object.LegalAddress == null || object.LegalAddress.length == 0) {
            result.isSuccess = false;
            result.invalid.push('LegalAddress');
        }
        if (object.PhoneNumber == null || object.PhoneNumber.length == 0) {
            result.isSuccess = false;
            result.invalid.push('PhoneNumber');
        }
        if (object.Email == null || object.Email.length == 0) {
            result.isSuccess = false;
            result.invalid.push('Email');
        }
        if (object.DirectorString == null || object.DirectorString.length == 0) {
            result.isSuccess = false;
            result.invalid.push('DirectorString');
        }
        if (object.ChiefAccountantString == null || object.ChiefAccountantString.length == 0) {
            result.isSuccess = false;
            result.invalid.push('ChiefAccountantString');
        }
        if (object.BankCorporateAccount == null || object.BankCorporateAccount.length == 0) {
            result.isSuccess = false;
            result.invalid.push('BankCorporateAccount');
        }
        if (object.BankName == null || object.BankName.length == 0) {
            result.isSuccess = false;
            result.invalid.push('BankName');
        }
        if (object.BankBIK == null || object.BankBIK.length == 0) {
            result.isSuccess = false;
            result.invalid.push('BankBIK');
        }
        if (object.BankCorrespondentAccount == null || object.BankCorrespondentAccount.length == 0) {
            result.isSuccess = false;
            result.invalid.push('BankCorrespondentAccount');
        }
    }
    if (type == 'profileCard') {
        if (object.SecondName == null || object.SecondName.length == 0) {
            result.isSuccess = false;
            result.invalid.push('SecondName');
        }
        if (object.FirstName == null || object.FirstName.length == 0) {
            result.isSuccess = false;
            result.invalid.push('FirstName');
        }
        if (object.PatronymicName == null || object.PatronymicName.length == 0) {
            result.isSuccess = false;
            result.invalid.push('PatronymicName');
        }
        if (object.DisplayName == null || object.DisplayName.length == 0) {
            result.isSuccess = false;
            result.invalid.push('DisplayName');
        }
        if (object.MobilePhone1 == null || object.MobilePhone1.length == 0) {
            result.isSuccess = false;
            result.invalid.push('MobilePhone1');
        }
        if (object.Email == null || object.Email.length == 0) {
            result.isSuccess = false;
            result.invalid.push('Email');
        }
    }
    if (result.isSuccess == false) {
        var field = $('.' + type).find('#' + result.invalid[0]);
        var offset = field.offset();
        var offset_top = offset.top;
        $('html,body').animate({ scrollTop: offset_top - field.height() }, 1000);
        field.popover({
            trigger: 'focus',
            placement: placement,
            content: 'Обязательное поле'
        });
        field.find('input').focus();
        field.find('select').focus();
    }
    return result;
}

function loadDepartment() {
    var department = vueOrganisationStructureTabState.selectedDepartment;

    $.ajax({
        url: '../Department/GetDepartment',
        type: 'POST',
        data: {
            departmentId: department.Id,
            organisationId: vueHeaderState.selectedOrganisation.id,
        },
        beforeSend: function () {
            vueOrganisationStructureTabState.isDepartmentUpdating = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            vueOrganisationStructureTabState.selectedDepartment.Members = parsedResponse.Members;
        },
        error: function () {
            vueMessage.DangerMessage = "Ошибка обновления данных подразделения"
        },
        complete: function () {
            vueOrganisationStructureTabState.isDepartmentUpdating = false;
        }
    });
}

function loadMember() {
    var member = vueOrganisationStructureTabState.selectedMember;
    $.ajax({
        url: '../Department/GetMember',
        type: 'POST',
        data: {
            userId: member.Id,
            organisationId: vueHeaderState.selectedOrganisation.id,
        },
        beforeSend: function () {
            vueOrganisationStructureTabState.isMemberUpdating = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            vueOrganisationStructureTabState.selectedMember = parsedResponse;
        },
        error: function () {
            vueMessage.DangerMessage = "Ошибка обновления данных сотрудника"
        },
        complete: function () {
            vueOrganisationStructureTabState.isMemberUpdating = false;
        }
    });
}

function getOrganisationInfo() {

}

function getAvailableInvitedUsers() {
    vueOrganisationStructureTabState.availableInvitedUsers = [];
    for (var i = 0; i < vueOrganisationStructureTabState.invitedUsers.length; i++) {
        var user = jQuery.extend(true, {}, vueOrganisationStructureTabState.invitedUsers[i]);
        user.isDeleting = false;
        var isFind = false;
        for (var j = 0; j < modelMembers.length; j++) {
            if (user.userId == modelMembers[j].Id) {
                isFind = true;
            }
        }
        if (isFind == false) {
            vueOrganisationStructureTabState.availableInvitedUsers.push(user);
        }
    }
}

function getLicenses() {
    if (vueTabState.tabs[2].isAccess == true) {
        $.ajax({
            url: '../Subscriptions/GetLicenses',
            type: 'POST',
            data: {
                organisationId: vueHeaderState.selectedOrganisation.id,
            },
            beforeSend: function () {
                vueSubscriptionTabState.subscriptionLoading = true;
                vueSubscriptionTabState.Licenses = [];
            },
            success: function (response) {
                var parsedResponse = JSON.parse(response);
                for (var i = 0; i < parsedResponse.length; i++) {
                    var license = parsedResponse[i];
                    vueSubscriptionTabState.Licenses.push(license);
                }
            },
            error: function () {
                vueMessage.DangerMessage = "Не удалось получить список купленных лицензий"
            },
            complete: function () {
                vueSubscriptionTabState.subscriptionLoading = false;
            }
        });
    }
}

function getAvailableLicenses() {
    if (vueTabState.tabs[2].isAccess == true) {
        $.ajax({
            url: '../Subscriptions/GetAvailableSubscriptions',
            type: 'POST',
            beforeSend: function () {
                vueSubscriptionTabState.availableLicenses = [];
            },
            success: function (response) {
                var parsedResponse = JSON.parse(response);
                for (var i = 0; i < parsedResponse.length; i++) {
                    var license = parsedResponse[i];
                    if (license.Subscriptions.length > 0) {
                        var subscription = license.Subscriptions[0];
                        license.isSelected = false;
                        license.Quantity = 2;
                        license.SelectedSubscription = subscription;
                        license.isCalculation = false;
                        license.Total = license.SelectedSubscription.Price * license.Quantity;
                        vueSubscriptionTabState.availableLicenses.push(license);
                    }
                }
            },
            error: function () {
                vueMessage.DangerMessage = "Не удалось получить список доступных лицензий"
            },
            complete: function () {
            }
        });
    }
}

function getInvoices() {
    if (vueTabState.tabs[2].isAccess == true) {
        $.ajax({
            url: '../Subscriptions/GetInvoices',
            type: 'POST',
            data: {
                organisationId: vueHeaderState.selectedOrganisation.id,
            },
            beforeSend: function () {
                vueSubscriptionTabState.invoiceLoading = true;
                vueSubscriptionTabState.Invoices = [];
            },
            success: function (response) {
                var parsedResponse = JSON.parse(response);
                for (var i = 0; i < parsedResponse.length; i++) {
                    var invoice = parsedResponse[i];
                    vueSubscriptionTabState.Invoices.push(invoice);
                }
            },
            error: function () {
                vueMessage.DangerMessage = "Не удалось получить список счетов"
            },
            complete: function () {
                vueSubscriptionTabState.invoiceLoading = false;
            }
        });
    }
}