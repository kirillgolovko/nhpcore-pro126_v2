﻿Vue.directive('mask', VueMask.VueMaskDirective);

$(document).ready(function () {
    var parts = document.location.href.split("#");
    getSystems();
    getOrganisations();
    getInvoices();
    $.cookie('selectedSystem', 'Manager', { expires: 30, path: '/' });
    CKEDITOR.replace('editor1', {
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'About',
        //removeButtons: 'Subscript,Superscript,Anchor,Styles,Specialchar',
        height: 300, width: '100%'
    });

    $(".acidjs-css3-treeview").delegate("label input:checkbox", "change", function () {
        var
            checkbox = $(this),
            nestedList = checkbox.parent().next().next(),
            selectNestedListCheckbox = nestedList.find("label:not([for]) input:checkbox");

        if (checkbox.is(":checked")) {
            return selectNestedListCheckbox.prop("checked", true);
        }
        selectNestedListCheckbox.prop("checked", false);
    });
});

var vueAppState = {
    hasSystem: false,
    isOrganisationLoading: true,
    isDepartmentLoading: true,
    organisations: [],
    profiles: [],
    selectedOrganisation: {},
    selectedProfile: {},
    systems: [],
    SuccessMessage: "",
    DangerMessage: "",
    WarningMessage: "",
    InfoMessage: "",
    invoiceLoading: true,
    Invoices: [],
    isFilterInvoices: true,
    FilterInvoices: {
        DateTime: '',
        Name: '',
        BuyeerName: '',
        Total: ''
    },
    isMailSending: false,
    isInvoiceAccepting: false,
    departments: []
};

var vueApp = new Vue({
    el: "#app",
    data: vueAppState,
    methods: {
        selectOrganisation: function (org) {
            this.selectedOrganisation = org;
            $.cookie('selectedOrganisation', JSON.stringify(this.selectedOrganisation), { expires: 30, path: '/' });
            getDepartments();
        },
        closeSuccess: function () {
            this.SuccessMessage = "";
        },
        closeDanger: function () {
            this.DangerMessage = "";
        },
        closeWarning: function () {
            this.WarningMessage = "";
        },
        closeInfo: function () {
            this.InfoMessage = "";
        },
        acceptIncoice: function (invoice) {
            var answer = confirm("Принять оплату по счету №" + invoice.Name +"?");
            if (answer != true) { return false; }
            $.ajax({
                url: "../Subscriptions/InvoiceAccept",
                type: "POST",
                data: {
                    invoiceId: invoice.Id
                },
                beforeSend: function () {
                    vueAppState.isInvoiceAccepting=true;
                },
                success: function (response) {
                    var parsedResponse = JSON.parse(response);
                    if (parsedResponse.IsSuccess == true) {
                        vueAppState.SuccessMessage = "Принятие оплаты прошло успешно";
                        invoice.IsPayed = true;
                    } else {
                        vueAppState.DangerMessage = parsedResponse.ErrorMessage;
                    }
                },
                error: function (a, b, c) {
                    vueAppState.DangerMessage = "Ошибка принятия оплаты";
                },
                complete: function () {
                    vueAppState.isInvoiceAccepting = false;
                }
            });
        },
        sendEmail: function () {
            if (vueAppState.isMailSending == false) {
                var to = $('.compose-header-to').find('input').val();
                var theme = $('.compose-header-theme').find('input').val();
                var editor = CKEDITOR.instances.editor1;
                var body = escapeHtml(editor.getData());

                $.ajax({
                    url: "../Home/SendEmailMass",
                    type: "POST",
                    data: {
                        To: to,
                        Theme: theme,
                        messageBody: body,
                    },
                    beforeSend: function () {
                        vueAppState.isMailSending = true;
                    },
                    success: function (response) {
                        var parsedResponse = JSON.parse(response);
                        if (parsedResponse.IsSuccess == true) {
                            vueAppState.SuccessMessage = "Почтовое сообщение успешно отправлено";
                        } else {
                            vueAppState.DangerMessage = parsedResponse.ErrorMessage;
                        }
                    },
                    error: function (a, b, c) {
                        vueAppState.DangerMessage = "Ошибка отправки почтового сообщения";
                    },
                    complete: function () {
                        vueAppState.isMailSending = false;
                    }
                });
            }
        },
        showAdressBook: function () {
            $('#adressbook').modal('show');
        }
    },
    computed: {
        isSuccessMessage: function () {
            return this.SuccessMessage.length > 0;
        },
        isDangerMessage: function () {
            return this.DangerMessage.length > 0;
        },
        isWarningMessage: function () {
            return this.WarningMessage.length > 0;
        },
        isInfoMessage: function () {
            return this.InfoMessage.length > 0;
        },
        filteredInvoices: function () {
            return this.Invoices.filter(function (invoice) {
                var result = true;
                if (vueAppState.FilterInvoices.DateTime.length > 0 && result === true) {
                    result = invoice.DateTime.toLowerCase().indexOf(vueAppState.FilterInvoices.DateTime.toLowerCase()) !== -1;
                }
                if (vueAppState.FilterInvoices.Name.length > 0 && result === true) {
                    result = invoice.Name.toLowerCase().indexOf(vueAppState.FilterInvoices.Name.toLowerCase()) !== -1;
                }
                if (vueAppState.FilterInvoices.BuyeerName.length > 0 && result === true) {
                    result = invoice.BuyeerName.toLowerCase().indexOf(vueAppState.FilterInvoices.BuyeerName.toLowerCase()) !== -1;
                }
                if (vueAppState.FilterInvoices.Total.toString().length > 0 && result === true) {
                    result = invoice.Total.toString().toLowerCase().indexOf(vueAppState.FilterInvoices.Total.toString().toLowerCase()) !== -1;
                }
                return result;
            })
        }
    },
    watch: {
        SuccessMessage: function (newSuccessMessage) {
            if (newSuccessMessage.length > 0) {
                setTimeout(function () {
                    vueApp.closeSuccess();
                }, 3000);
            }
        }
    }
});

function getSystems() {
    $.ajax({
        url: "../Home/GetSystem",
        type: "POST",
        beforeSend: function () {
            vueAppState.systems = [];
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            for (var i = 0; i < parsedResponse.length; i++) {
                var system = parsedResponse[i];
                vueAppState.systems.push(system);
            }
            vueAppState.hasSystem = (vueAppState.systems.length > 1);
        },
        complete: function () {
        }
    });
};

function getOrganisations() {
    $.ajax({
        url: "../Admin/GetAvailable",
        type: "POST",
        beforeSend: function () {
            vueApp.organisations = [];
            vueApp.isOrganisationLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            var selectedOrganisation = $.cookie("selectedOrganisation");
            var hasSelected = false;
            if (typeof selectedOrganisation !== 'undefined') {
                hasSelected = true;
                selectedOrganisation = JSON.parse(selectedOrganisation);
            }
            for (var i = 0; i < parsedResponse.length; i++) {
                var organisation = parsedResponse[i];
                vueApp.organisations.push(organisation);
                if (hasSelected && organisation.id === selectedOrganisation.id) {
                    vueApp.selectOrganisation(organisation);
                }
            }
            if (typeof vueApp.selectedOrganisation.id === 'undefined') {
                vueApp.selectOrganisation(parsedResponse[0]);
            };
        },
        complete: function () {
            vueApp.isOrganisationLoading = false;
            
        }
    });
};

function getInvoices() {
    $.ajax({
        url: '../Subscriptions/GetInvoices',
        type: 'POST',
        data: {
            organisationId: 0,
        },
        beforeSend: function () {
            vueAppState.invoiceLoading = true;
            vueAppState.Invoices = [];
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            for (var i = 0; i < parsedResponse.length; i++) {
                var invoice = parsedResponse[i];
                vueAppState.Invoices.push(invoice);
            }
        },
        error: function () {
            vueAppState.DangerMessage = "Не удалось получить список счетов"
        },
        complete: function () {
            vueAppState.invoiceLoading = false;
        }
    });
}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}

function getDepartments() {
    $.ajax({
        url: '../Department/GetAvailable',
        type: 'GET',
        data: {
            organisationId: vueAppState.selectedOrganisation.id
        },
        beforeSend: function () {
            vueAppState.isDepartmentLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            vueAppState.departments = parsedResponse;
        },
        complete: function () {
            vueAppState.isDepartmentLoading = false;
        }
    });
};

