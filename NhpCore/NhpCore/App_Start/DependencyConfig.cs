﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NhpCore.Infrastructure.DependencyResolution;
using NhpCore.Infrastructure.Data.Ef;
using Microsoft.AspNet.Identity;
using NhpCore.CoreLayer.Entities;
using Microsoft.Owin.Security;
using NhpCore.Infrastructure.DependencyResolution.AutofacModules;
using NhpCore.Infrastructure.Utils.Mapping;
using Common.Mapping;

namespace NhpCore
{
    public class DependencyConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new BusinessServicesModule());
            builder.RegisterModule(new InfrastructureDataModule());
            builder.RegisterModule(new InfrastructureUtilsModule());
            builder.RegisterModule(new IdentityManagerModule());

            builder.Register(x => HttpContext.Current.GetOwinContext().Authentication).As<IAuthenticationManager>();

            var mapper = new AutoMapperUtilityImpl();
            mapper.Initialize((MappingConfig.Configure()));
            builder.RegisterInstance(mapper).As<IMapperUtility>();


            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}