﻿using AutoMapper;
using NhpCore.CoreLayer.Entities;
using NhpCore.DTOs;
using NhpCore.Infrastructure.Utils.Mapping.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NhpCore
{
    public class MappingConfig
    {
        public static MapperConfiguration Configure()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OrganisationDocumentsStorageAccess, DbOrganisationDocumentsStorageAccess>()
                .ForMember(dest => dest.StorageType, opt => opt.MapFrom(src => src.StorageType));
            });

            return configuration;
        }
    }

}