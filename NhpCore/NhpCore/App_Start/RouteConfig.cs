﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NhpCore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //notmailsender?idprofile=guid
            routes.MapRoute(
                name: "NotMailSender",
                url: "notmailsender",
                defaults: new { controller = "Home", action = "NotMailSender" }
            );

            routes.MapRoute(
                name: "Unsubscribe",
                url: "newsletter/unsubscribe",
                defaults: new { controller = "Home", action = "Unsubscribe" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            
        }
    }
}
