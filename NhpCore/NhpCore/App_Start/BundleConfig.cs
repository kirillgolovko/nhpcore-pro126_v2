﻿using System.Web.Optimization;

namespace NhpCore
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/jquerycookie").Include(
                        "~/Scripts/jquery.cookie.js"));
            bundles.Add(new ScriptBundle("~/bundles/vue").Include(
                "~/Scripts/vue.js", "~/Scripts/v-mask.min.js"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootbox").Include(
                      "~/Scripts/bootbox.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Lib/bootstrap.css",
                      "~/Content/Styles/site.css"));

            bundles.Add(new StyleBundle("~/bundles/vue.css").Include(
                "~/Content/Styles/vue.css"));

            bundles.Add(new StyleBundle("~/bundles/fa").Include(
                "~/Content/Lib/font-awesome.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/jHTML").Include(
                "~/Scripts/jHtmlArea-0.7.0.min-vsdoc.js", "~/Scripts/jHtmlArea-0.7.0.min.js", "~/Scripts/jHtmlArea.ColorPickerMenu-0.7.0.min.js", "~/Scripts/jquery-ui-1.7.2.custom.min.js"));
        }
    }
}
