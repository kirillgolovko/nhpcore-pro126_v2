﻿using Common.Logging;
using Dapper;
using NhpCore.Infrastructure.Utils.MessageSender;
using SyncDto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Utils.ServiceSynchronization
{
    public class ServiceSynchronization : IServiceSynchronization
    {
        private string[] connectionStrings = new[] {
            "data source=nhpreports.westeurope.cloudapp.azure.com;Initial Catalog=NhpProjectDbNew;Persist Security Info=True;User ID=vmreport;Password=Rehbnmjnxtns24xfcf;MultipleActiveResultSets=True"
        };

        private readonly IMessageSender _messageSender;
        private readonly ILoggerUtility _logger;

        public ServiceSynchronization(IMessageSender messageSender, ILoggerUtility logger)
        {
            if (messageSender == null) throw new ArgumentNullException(nameof(messageSender));
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            _messageSender = messageSender;
            _logger = logger;
        }

        #region Insert Organisation

        public Task AddOrganisation(int id, string name, DateTime registerDate)
        {

            return Task.Run(() => InsertOrganisation(id, name, registerDate, connectionStrings[0]));
        }

        private void InserOrganisationToServices(int id, string name, DateTime registerDate)
        {
            foreach (var connStr in connectionStrings)
            {
                Task.Run(() => InsertOrganisation(id, name, registerDate, connStr));
            }
        }

        private async Task InsertOrganisation(int id, string name, DateTime registerDate, string connStr)
        {
            //проверить есть ли пользователь
            // если есть - уведомить о наличии, если нет - добавить
            IDbConnection db = new SqlConnection(connStr);
            var gettedOrganisation = db.Query<GetOrganisationSync>("SELECT Id, Name FROM NHP_Organisations where Id=@orgId", new { orgId = id }).FirstOrDefault();

            if (gettedOrganisation == null)
            {
                try
                {
                    var query = @"INSERT INTO NHP_Organisations (
Id,
Name,
RegisterDate,
CoreId,
IsDeleted) values (@OrganisationId, @Name, @RegistrationDate, @OrganisationId, 0)";

                    await db.ExecuteAsync(query, new { OrganisationId = id, Name = name, RegistrationDate = registerDate });
                    db.Dispose();
                }
                catch (Exception ex)
                {
#if !DEBUG

                    // TODO: отправить имейл о возникшей ошибке
                    var catalog = connStr.Split(';')[1];
                    var body = $"Проблема при вставке организации {id}-{name}; БД {catalog}";

                    try
                    {
                        // TODO: отправить имейл о возникшей ошибке
                        await _messageSender.SendAsync(new Message("info@nhp-soft.ru", "WARNING: Ошибка при синхронизации сервисов.", body));
                    }
                    catch (Exception e)
                    {
                        _logger.Warn(body, e);
                    }
#endif

                }
            }
        }

        #endregion Insert Organisation

        #region AddUser

        public Task AddUser(int id, string email, string displayName)
        {
            return Task.Run(() => InsertUser(id, email, displayName, connectionStrings[0]));
        }


        private async Task InsertUser(int id, string email, string displayName, string connStr)
        {
            //проверить есть ли пользователь
            // если есть - уведомить о наличии, если нет - добавить
            IDbConnection db = new SqlConnection(connStr);
            try
            {
                var gettedUser = db.Query<GetUserSync>("SELECT UserID, Email FROM NHP_Users where UserID=@userId", new { userId = id }).FirstOrDefault();

                if (gettedUser == null)
                {
                    try
                    {
                        var query = @"INSERT INTO NHP_Users (
UserID,
Email,
Password,
DisplayName,
IsDepartmentChief,
AccessRole_F,
IsMailSended,
IsDeleted,
IsBlocked,
CoreId,
IdentityId,
TelegramChatId) values (@Id, @Email, '', @DisplayName, 0, 300, 1, 0, 0, @Id, @Id, 0)";

                        await db.ExecuteAsync(query, new { Id = id, Email = email, DisplayName = displayName });
                        db.Dispose();
                    }
                    catch (Exception ex)
                    {
#if !DEBUG

                    // TODO: отправить имейл о возникшей ошибке
                    var catalog = connStr.Split(';')[1];
                    var body = $"Проблема при вставке организации {id}-{email}; БД {catalog}";

                    try
                    {
                        await _messageSender.SendAsync(new Message("info@nhp-soft.ru", "WARNING: Ошибка при синхронизации сервисов.", body));
                    }
                    catch (Exception e)
                    {
                        _logger.Warn(body, e);
                    }
#endif

                    }
                }
            }
            catch (Exception ex)
            {


            }
        }

        #endregion AddUser

        #region AddUserToOrganisation

        public Task AddUserToOrganisation(int userId, int organisationId)
        {
            return Task.Run(() => AddOrganisationUser(userId, organisationId, connectionStrings[0]));
        }

        private void AddOrganisationUserToServices(int userId, int organisationId)
        {
            foreach (var connStr in connectionStrings)
            {
                Task.Run(() => AddOrganisationUser(userId, organisationId, connStr));
            }
        }

        private async Task AddOrganisationUser(int userId, int organisationId, string connStr)
        {
            //проверить есть ли пользователь
            // если есть - уведомить о наличии, если нет - добавить
            IDbConnection db = new SqlConnection(connStr);
            var gettedUser = db.Query<GetOrganisationUserSync>("SELECT Organisation_Id, User_Id FROM OrganisationUsers where Organisation_Id=@orgId and User_Id=@uId", new { orgId = organisationId, uId = userId }).FirstOrDefault();

            if (gettedUser == null)
            {
                try
                {
                    var query = @"INSERT INTO OrganisationUsers (
Organisation_Id,
User_Id
) values (@OrganisationId, @UserId)";

                    await db.ExecuteAsync(query, new { OrganisationId = organisationId, UserId = userId });
                    db.Dispose();
                }
                catch (Exception ex)
                {
#if !DEBUG
                    var catalog = connStr.Split(';')[1];
                    var body = $"Проблема при добавлении пользователя {userId} к организации {organisationId} - БД {catalog}";

                    try
                    {
                        // TODO: отправить имейл о возникшей ошибке
                        await _messageSender.SendAsync(new Message("info@nhp-soft.ru", "WARNING: Ошибка при синхронизации сервисов.", body));
                    }
                    catch (Exception e)
                    {
                        _logger.Warn(body, e);
                    }
#endif
                }
            }
        }

        #endregion AddUserToOrganisation
    }
}