namespace NhpProject.Infrastructure.Data.Ef
{
    using Core.Entities;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public class NhpProjectDbContext : DbContext
    {
        #region Constructors

        public NhpProjectDbContext()
            : base(nameof(NhpProjectDbContext))
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<NhpProjectDbContext>());
        }
        public NhpProjectDbContext(string name)
            : base(name)
        {

        }
        #endregion Constructors

        #region Properties

        public virtual DbSet<AccessRole> AccessRoles { get; set; }

        public virtual DbSet<AdvancedDescription> AdvancedDescriptions { get; set; }

        public virtual DbSet<AllowToNotification> AllowToNotification { get; set; }

        public virtual DbSet<Department> Departments { get; set; }

        public virtual DbSet<License> Licenses { get; set; }

        public virtual DbSet<Organisation> Organisations { get; set; }

        public virtual DbSet<Structure> Structure { get; set; }

        public virtual DbSet<TaskAllert> TaskAllerts { get; set; }

        public virtual DbSet<TaskNote> TaskNotes { get; set; }

        public virtual DbSet<TaskEntity> Tasks { get; set; }

        public virtual DbSet<TaskStateEntity> TaskStates { get; set; }

        public virtual DbSet<TaskUserState> TaskUserStates { get; set; }

        public virtual DbSet<UserRole> UserRoles { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<AssignedTask> AssignedTasks { get; set; }

        public virtual DbSet<ProgramVersion> ProgramVersions { get; set; }

        public virtual DbSet<UserNotificationStatus> UserNotificationStatuses { get; set; }

        public virtual DbSet<UserNotificationPreferences> UserNotificationPreferences { get; set; }
        #endregion Properties

        #region Methods

        public static NhpProjectDbContext Create()
        {
            return new NhpProjectDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region MigrationHistory
            //modelBuilder.Entity<__MigrationHistory>()
            //    .Property(e => e.MigrationId)
            //    .IsUnicode(false);

            //modelBuilder.Entity<__MigrationHistory>()
            //    .Property(e => e.ContextKey)
            //    .IsUnicode(false);

            //modelBuilder.Entity<__MigrationHistory>()
            //    .Property(e => e.ProductVersion)
            //    .IsUnicode(false);
            #endregion

            #region AccessRole
            modelBuilder.Entity<AccessRole>()
                .Property(e => e.RoleDescription)
                .IsUnicode(false);

            modelBuilder.Entity<AccessRole>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.AccessRole)
                .HasForeignKey(e => e.AccessRoleId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Department
            modelBuilder.Entity<Department>()
                .Property(e => e.Name)
                .IsUnicode(true);

            modelBuilder.Entity<Department>()
                .Property(e => e.Description)
                .IsUnicode(true);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Department)
                .HasForeignKey(e => e.DepartmentId);
            #endregion

            #region Organisation
            modelBuilder.Entity<Organisation>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<Organisation>()
                .Property(e => e.Name)
                .IsUnicode(true);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.AllowToNotifications)
                .WithRequired(e => e.Organisation)
                .HasForeignKey(e => e.OrganisationId);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.Departments)
                .WithRequired(e => e.Organisation)
                .HasForeignKey(e => e.OrganisationId);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.Licenses)
                .WithRequired(e => e.Organisation)
                .HasForeignKey(e => e.OrganisationId);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.Users)
                .WithOptional(e => e.Organisation)
                .HasForeignKey(e => e.OrganisationId);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.ManyUsers)
                .WithMany(e => e.Organisations);

            #endregion

            #region TaskAllert
            modelBuilder.Entity<TaskAllert>()
               
                .Property(e => e.AllertText)
                .IsUnicode(true);
            #endregion

            #region TaskNote
            modelBuilder.Entity<TaskNote>()
                .Property(e => e.Text)
                .IsUnicode(true);
            #endregion

            #region TaskEntity
            modelBuilder.Entity<TaskEntity>()
                .Property(e => e.Name)
                .IsUnicode(true);

            modelBuilder.Entity<TaskEntity>()
                .Property(e => e.Description)
                .IsUnicode(true);
            #endregion

            #region TaskStateEntity
            modelBuilder.Entity<TaskStateEntity>()
                .Property(e => e.Name)
                .IsUnicode(true);

            modelBuilder.Entity<TaskStateEntity>()
                .HasMany(e => e.Tasks)
                .WithOptional(e => e.TaskState)
                .HasForeignKey(e => e.TaskStateId);
            #endregion

            #region TaskUserState
            modelBuilder.Entity<TaskUserState>()
                .Property(e => e.Name)
                .IsUnicode(true);

            modelBuilder.Entity<TaskUserState>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.TaskUserState)
                .HasForeignKey(e => e.TaskUserStateId);
            #endregion

            #region User
            modelBuilder.Entity<User>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(true);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(true);

            modelBuilder.Entity<User>()
                .Property(e => e.DisplayName)
                .IsUnicode(true);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(true);

            modelBuilder.Entity<User>()
                .Property(e => e.LastName)
                .IsUnicode(true);

            modelBuilder.Entity<User>()
                .Property(e => e.PatronymicName)
                .IsUnicode(true);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tasks)
                .WithOptional(e => e.Creator)
                .HasForeignKey(e => e.CreatorId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Organisations)
                .WithMany(e => e.ManyUsers);
            #endregion
        }

        #endregion Methods
    }
}