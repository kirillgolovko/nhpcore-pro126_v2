﻿using NhpProject.Core.Entities;
using System;
using System.Data.Entity.Migrations;

namespace NhpProject.Infrastructure.Data.Ef
{
    internal sealed class NhpProjectEfConfiguration : DbMigrationsConfiguration<NhpProjectDbContext>
    {
        public NhpProjectEfConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(NhpProjectDbContext context)
        {
        }
    }

}
