﻿using NhpProject.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DefaultOrganisationToOrganisationUser
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new NhpProjectDbContext())
            {
                var users =  db.Users.ToList();
                var organisations = db.Organisations.ToList();

                users.ForEach(x =>
                {
                    var org = organisations.FirstOrDefault(o => o.Id == x.OrganisationId);

                    if (org!=null)
                    {
                        x.Organisations.Add(org);
                    }
                });

                db.SaveChanges();
            }

        }
    }
}
