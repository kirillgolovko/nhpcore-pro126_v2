namespace NhpProject.Core.Entities
{
    using Common.EntityFramework;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NHP_Users")]
    public partial class User : IEntity
    {
        #region Constructors

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            TaskAllerts = new HashSet<TaskAllert>();
            Tasks = new HashSet<TaskEntity>();

            Organisations = new HashSet<Organisation>();
        }

        #endregion Constructors

        #region Properties

        [Key]
        [Column("UserID")]
        public int Id { get; set; }

        public int? OrganisationId { get; set; }

        [Required]
        [StringLength(45)]
        public string Email { get; set; }

        [Required]
        [StringLength(45)]
        public string Password { get; set; }

        [StringLength(45)]
        public string DisplayName { get; set; }

        [StringLength(45)]
        public string FirstName { get; set; }

        [StringLength(45)]
        public string LastName { get; set; }

        [StringLength(45)]
        public string PatronymicName { get; set; }

        public int? Salary { get; set; }

        [Column("DepartmentID_F")]
        public int? DepartmentId { get; set; }

        public bool IsDepartmentChief { get; set; }

        [Column("AccessRole_F")]
        public int AccessRoleId { get; set; }

        public bool IsMailSended { get; set; }

        public virtual AccessRole AccessRole { get; set; }

        public virtual Department Department { get; set; }

        public virtual Organisation Organisation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskAllert> TaskAllerts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskEntity> Tasks { get; set; }

        public virtual UserNotificationStatus UserNotificationStatus { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(45)]
        public string TelegramUserName { get; set; }
        public long TelegramChatId { get; set; }

        public virtual UserNotificationPreferences UserNotificationPreferences { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsBlocked { get; set; }

        public int IdentityId { get; set; }

        public int CoreId { get; set; }


        public ICollection<Organisation> Organisations { get; set; }
        /* 
ALTER TABLE `NHP_Users` 
ADD COLUMN `PhoneNumber` VARCHAR(20) NULL AFTER `IsMailSended`,
ADD COLUMN `TelegramUserName` VARCHAR(45) NULL AFTER `PhoneNumber`,
ADD COLUMN `TelegramChatId` BIGINT NULL AFTER `TelegramUserName`;
         */
        #endregion Properties
    }
}