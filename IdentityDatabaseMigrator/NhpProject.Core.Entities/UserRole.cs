namespace NhpProject.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Entities;
    using Common.EntityFramework;

    [Table("NHP_UserRoles")]
    public partial class UserRole : IEntity
    {
        #region Properties

        public int Id { get; set; }

        public int OrganisationId { get; set; }
        public virtual Organisation Organisation { get; set; }

        public int TaskId { get; set; }

        public int UserId { get; set; }

        public int TaskUserStateId { get; set; }

        public virtual TaskEntity Task { get; set; }

        public virtual User User { get; set; }

        public virtual TaskUserState TaskUserState { get; set; }

        #endregion Properties
    }
}