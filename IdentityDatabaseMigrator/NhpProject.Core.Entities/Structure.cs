namespace NhpProject.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Entities;
    using Common.EntityFramework;

    [Table("NHP_Structure")]
    public partial class Structure : IEntity
    {
        #region Properties

        public int OrganisationId { get; set; }

        public int ID { get; set; }

        public int? ParentID { get; set; }

        public bool? ParentType { get; set; }

        public int CurrentID { get; set; }

        public bool CurrentType { get; set; }

        #endregion Properties
    }
}