﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Common.EntityFramework;

namespace NhpProject.Core.Entities
{
    /* 
CREATE TABLE `NHP_UserNotificationPreferences` (
  `UserId` int(11) NOT NULL,
  `IsEmail` tinyint(1) NOT NULL DEFAULT '1',
  `IsTelegram` tinyint(1) NOT NULL DEFAULT '1',
  KEY `PROMARY` (`UserId`),
  CONSTRAINT `FK_NHP_UserNotificationPreferences_NHP_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `NHP_Users` (`UserID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
     */
    [Table("NHP_UserNotificationPreferences")]
    public class UserNotificationPreferences : IEntity
    {
        #region Properties
        [Key]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [ForeignKey("User")]
        public int UserId { get; set; }

        public bool IsEmail { get; set; }

        public bool IsTelegram { get; set; }

        public virtual User User { get; set; }
        #endregion
    }
}
