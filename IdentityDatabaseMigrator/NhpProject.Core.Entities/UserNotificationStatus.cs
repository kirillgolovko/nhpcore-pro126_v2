﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EntityFramework;

namespace NhpProject.Core.Entities
{
    public class UserNotificationStatus : IEntity
    {
        [Key]
        [ForeignKey("User")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int Id { get; set; }
        public DateTime? LastDateOfDailyStartingTaskSended { get; set; }

        public virtual User User { get; set; }
    }
}
