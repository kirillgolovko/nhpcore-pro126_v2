namespace NhpProject.Core.Entities
{
    using Common.EntityFramework;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NHP_TaskAllerts")]
    public partial class TaskAllert : IEntity
    {
        #region Properties

        public int Id { get; set; }

        public int TaskId { get; set; }

        public int UserId { get; set; }

        public DateTime AllertDate { get; set; }

        public int OrganisationId { get; set; }

        [MaxLength(1000)]
        public string AllertText { get; set; }

        public bool? IsSended { get; set; }

        public virtual TaskEntity Task { get; set; }

        public virtual User User { get; set; }

        #endregion Properties
    }
}