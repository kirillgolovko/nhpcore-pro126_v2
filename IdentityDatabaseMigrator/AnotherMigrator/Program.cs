﻿using Microsoft.AspNet.Identity;

using NhpCore.Infrastructure.Data.Ef;
using NhpProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace AnotherMigrator
{
    class Program
    {
        static void Main(string[] args)
        {
            //  var projUsers = Migration.Start();
            //   NextStep(projUsers);
            string nonParsed = string.Empty;
            using (StreamReader sr = new StreamReader("D:\\projectUsers.txt"))
            {
                nonParsed = sr.ReadToEnd();
            }
            string[] parsed = nonParsed.Split(';');
            var parsedList = parsed.ToList();

            using (NhpIdentityDbContext db = new NhpIdentityDbContext("data source=nhp-soft.database.windows.net;initial catalog=NhpCore-Identity;persist security info=True;user id=autumninthecity240490;password=Helgard240490;MultipleActiveResultSets=True;App=EntityFramework"))
            {
                var s = db.Users.ToList();

                s.ForEach(x => db.Users.Remove(x));

                db.SaveChanges();
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT('AspNetUsers', RESEED, 0)");

                if (parsedList != null && parsedList.Count > 0)
                {
                    Next(parsedList);
                }
            }

            Console.WriteLine("finished");
            Console.ReadLine();
        }
        class UserMock
        {
            public int Id { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }
        private async static void Next(List<string> parsedList)
        {
            parsedList.Remove("");
            var ids = new List<int>();
            var manager = new CustomUserManager(new CustomUserStore(new NhpIdentityDbContext("data source=nhp-soft.database.windows.net;initial catalog=NhpCore-Identity;persist security info=True;user id=autumninthecity240490;password=Helgard240490;MultipleActiveResultSets=True;App=EntityFramework")));
            manager.UserValidator = new UserValidator<CustomUser, int>(manager) { RequireUniqueEmail = false, AllowOnlyAlphanumericUserNames = false };
            manager.PasswordValidator = new PasswordValidator() { RequiredLength = 3 };
            foreach (var item in parsedList)
            {

                string stringId = item.Split(':').First();
                int value = Convert.ToInt32(stringId);
                ids.Add(value);

            }

            var projUsers = parsedList.Select(x => new
            {

                Id = Convert.ToInt32(x.Split(':')[0]),
                Email = x.Split(':')[1],
                Password = x.Split(':')[2]
            }).ToList();


            var maxId = ids.Max();
            for (int i = 1; i <= maxId; i++)
            {
                var user = projUsers.Find(x => x.Id == i);

                CustomUser userForAdd = null;
                //string password = "emptyPsw";
                string password = "empty";
                if (user != null)
                {
                    userForAdd = new CustomUser
                    {
                        Email = user.Email,
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        UserName = user.Email.Split('@').First()
                    };

                    password = user.Password;
                    //if (password.Length < 6)
                    //{
                    //    password += "123456";
                    //}
                }
                else
                {
                    userForAdd = new CustomUser
                    {
                        Email = "test@test",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        UserName = $"test"
                    };
                }

                if (userForAdd.UserName == "test")
                {
                    var str = Guid.NewGuid().ToString().Split('-').First().Substring(0, 3);
                    userForAdd.UserName += str;
                }

                var result = await manager.CreateAsync(userForAdd, password);
                if (result.Succeeded)
                {
                    Console.WriteLine($"{userForAdd.Email} saved");
                }
                if (!result.Succeeded)
                {
                    Console.WriteLine($"{userForAdd.Email} doesn't saved");
                }
            }

            using (NhpIdentityDbContext db = new NhpIdentityDbContext("data source=nhp-soft.database.windows.net;initial catalog=NhpCore-Identity;persist security info=True;user id=autumninthecity240490;password=Helgard240490;MultipleActiveResultSets=True;App=EntityFramework"))
            {
                var list = db.Users.Where(x => x.Email == "test@test").ToList();

                list.ForEach(x => db.Users.Remove(x));

                db.SaveChanges();
            }
        }
    }
}
