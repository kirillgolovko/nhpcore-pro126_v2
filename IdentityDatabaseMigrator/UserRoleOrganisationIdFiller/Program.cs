﻿using NhpProject.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRoleOrganisationIdFiller
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = "";
            using (var db = new NhpProjectDbContext(path))
            {
                var users = db.Users.ToList();

                // key - userId ; value - OrganisationId
                var userOrganisationIds = new Dictionary<int, int>();
                users.ForEach(x =>
                {

                    var isContains = userOrganisationIds.ContainsKey(x.Id);
                    var isValue = x.OrganisationId.HasValue;
                    if (!isContains && isValue)
                    {
                        userOrganisationIds.Add(x.Id, x.OrganisationId.Value);
                    }
                });

                var roles = db.UserRoles.ToList();

                roles.ForEach(x =>
                {
                    var isContains = userOrganisationIds.ContainsKey(x.UserId);

                    if (isContains)
                    {
                        x.OrganisationId = userOrganisationIds[x.UserId];
                    }

                });

                db.SaveChanges();
            }


            Console.WriteLine("finished");

            Console.Read();
        }
    }
}
